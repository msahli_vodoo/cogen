# -*- coding: utf-8 -*-

import ast
import base64
import csv
import functools
import glob
import itertools
import jinja2
import logging
import operator
import datetime
import hashlib
import os
import re
import simplejson
import sys
import time
import urllib2
import zlib
from xml.etree import ElementTree
from cStringIO import StringIO

import babel.messages.pofile
import werkzeug.utils
import werkzeug.wrappers
try:
    import xlwt
except ImportError:
    xlwt = None

import openerp
import openerp.modules.registry
from openerp.addons.base.ir.ir_qweb import AssetsBundle, QWebTemplateNotFound
from openerp.modules import get_module_resource
from openerp.tools import topological_sort
from openerp.tools.translate import _
from openerp import http

from openerp.http import request, serialize_exception as _serialize_exception

import openerp.addons.web.controllers.main

_logger = logging.getLogger(__name__)

import logging
logger= logging.getLogger('_______Debug________')

#----------------------------------------------------------
# OpenERP Web web Controllers
#----------------------------------------------------------
class Home_extend(openerp.addons.web.controllers.main.Home):

   
    @http.route(['/web/login/check_login_password'], type='json', auth="public", methods=['POST'], website=True)
    def check_login_password(self,login,password,**post):
        uid =request.uid
        logger.warn("_______Login: %s  Password: %s",login,password)
        
        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(request.session.db, login, password)
            if not (uid is not False):
                request.uid = old_uid
                return {'error': _('Wrong login/password'),'title': _('Change login/password')}
            
        return {}

# vim:expandtab:tabstop=4:softtabstop=4:shiftwidth=4:
