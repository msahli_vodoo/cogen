$('document').ready(function(){
    
    //polyfill placeholders
    $("[placeholder]").not('[data-sticky-ignore]').stickyPlaceholders({placeholderOpacity: 1});
    
    if($('.resp-box.map').length>0){
        $(window).on('resize',function(){
            if($(window).width()>767){
                $('.resp-box.map').css('height',$('.resp-box.map').parent().siblings().outerHeight());
            }else{
                $('.resp-box.map').css('height','');
            }
        }).resize();
    }
    
    $('a[href*="#"]:not([href="#"]):not([href*="#home-slider"])').not('.nav-tabs a[href*="#"]').click(function (e) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').stop(true, true).animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    
    if ($.fn.selectpicker) {
        $('select').selectpicker();
    }
    
    if($('.essences').length>0){
        $('.essences label').on('click',function(){
            $(this).addClass('active').children('input').prop('checked',true).parent().parent().siblings().children('label').removeClass('active');
        })
        $('.essences label:eq(0)').click();
    }
    
    if($('.doors').length>0){
        $('.doors label').on('click',function(){
            $(this).addClass('active').children('input').prop('checked',true).parent().parent().siblings().children('label').removeClass('active');
        })
        $('.doors label:eq(0)').click();
    }
    
    if($('.thickness-list').length>0){
        $('.thickness-list label').on('click',function(){
            $(this).addClass('active').children('input').prop('checked',true).parent().parent().siblings().children('label').removeClass('active');
            $(this).parents('.thickness-list').siblings('.visible-xs').children('select').val($(this).children('input').val()).change();
        })
        $('.thickness-list').siblings('.visible-xs').children('select').on('change',function(){
            console.log($(this).parents('.visible-xs').siblings('.thickness-list').find('input[value="'+$(this).val()+'"]'));
            $(this).parents('.visible-xs').siblings('.thickness-list').find('input[value="'+$(this).val()+'"]').prop('checked',true).parent().addClass('active').parent().siblings().children('label').removeClass('active');
        })
        $('.thickness-list label:eq(0)').click();
    }
    
    if($('.edge-box')){
        $('select[name="attrib_top"]').on('change',function(){
            if($(this).val()!='1470-1-0'){
                $('.edge-box').addClass('full-top');
            }else{
                $('.edge-box').removeClass('full-top');
            }
        }).change();
        $('select[name="attrib_bottom"]').on('change',function(){
            if($(this).val()!='1470-1-0'){
                $('.edge-box').addClass('full-bottom');
            }else{
                $('.edge-box').removeClass('full-bottom');
            }
        }).change();
        $('select[name="attrib_left"]').on('change',function(){
            if($(this).val()!='1470-1-0'){
                $('.edge-box').addClass('full-left');
            }else{
                $('.edge-box').removeClass('full-left');
            }
        }).change();
        $('select[name="attrib_right"]').on('change',function(){
            if($(this).val()!='1470-1-0'){
                $('.edge-box').addClass('full-right');
            }else{
                $('.edge-box').removeClass('full-right');
            }
        }).change();
    }
    
    if($('.spinner').length>0){
        /*var spinner = $( ".spinner" ).spinner({
            min:0
        });*/
        $(".spinner").TouchSpin({
            min:0
        });
    }
    if($('input[type="checkbox"]').length>0){
        $('input[type="checkbox"]').wrap('<div class="checkbox"></div>');
        
        $('.checkbox input').on('change',function(){
            if($(this).prop('checked')==true){
                $(this).parent().addClass('checked');
            }else{
                $(this).parent().removeClass('checked');
            }
        }).change();
        
        $('.checkbox').on('click',function(){
            if($(this).parents('label').length==0){
                $(this).children('input').prop('checked',!$(this).children('input').prop('checked'));
            }
        })
    }
    
    if($('.other-adress-form').length>0){
        $('select[name="shipping"]').on('change',function(){
            if($(this).val()=='other-adress'){
                $('.other-adress-form').slideDown(200);
            }else if($('.other-adress-form').css('display')=='block'){
                $('.other-adress-form').slideUp(200);
            }
            if($(this).val()=='pick-up'){
                $('.shipping-adress h2').toggleClass('hidden');
            }else if($('.shipping-adress h2:eq(0)').hasClass('hidden')){
                $('.shipping-adress h2').toggleClass('hidden');
            }
        }).change();
    }
    
    if($('.btn.inactive').length>0){
        $('.btn.inactive').on('click',function(e){
            e.preventDefault();
        })
    }
})