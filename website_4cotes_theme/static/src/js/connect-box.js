$(document).ready(function () {
	$('.oe_website_sale').each(function () {
    	var oe_website_sale = $(this);
	$(oe_website_sale).on('submit','form.login', function (e) {
		e.preventDefault();
        var $form = $(this);
        var login = $form.find('.login_input').val();
        var password = $form.find('.password_input').val();
        
        $('.js_check_login_password').addClass("hidden");
        $('.login_input').css("border","border: 2px solid #333");
		$('.password_input').css("border","border: 2px solid #333");
        
        openerp.jsonRpc("/web/login/check_login_password", 'call', {'login':login,'password':password}).then(function (data) {
        	if ('error' in data){
        		$('.js_check_login_password').removeClass("hidden");
        		$('.login_input').css("border","2px solid red");
        		$('.password_input').css("border","2px solid red");
        		
        	}
        	else{
        		console.log("Success:  js_login-modal check_login_password");
            	window.location.href=$form.attr("action");
        	}
        }).done(function() {
            console.log("done:  "); 
        }).fail(function() {
        	console.log("fail: ___________***_______");
        });
	});

	
	});
});