
$(document).ready(function () {
    var show_nb=5;
    var  count_model_div=0;
    var count_type_div=0;
    var count_epaisseur_div=0;
    var count_couleur_div=0;
    var nb_cbox_type = $('.cbox_type').children().length;
    var nb_cbox_model = $('.cbox_model').children().length;
    var nb_cbox_epaisseur = $('.cbox_epaisseur').children().length;
    var nb_cbox_couleur = $('.cbox_couleur').children().length;
    var showChar = 30;
    var ellipsestext = "...";
     var searches = [];
     var search_typ = [];
    
    $("input[name=group_checkbox_all_c]").on('change', function() {
        
        if( $(this).is(':checked') ){
            $(this).parent().children().first().addClass("fa-check");
            $('.cbox_couleur').children().last().children().each(function () {
             
               $(this).children().last().addClass("active");
                    $(this).children().last().children().first().prop('checked', true);
                    searches[searches.length]=$(this).children().last().children().first().val();
                    $('.js_search-selec').val(searches);
              });          
            
            } else {
                     $('.cbox_couleur').children().last().children().each(function () {
                        $(this).children().last().removeClass("active");
                        $(this).children().last().children().first().removeAttr('checked');
                        var index = searches.indexOf( $(this).children().last().children().first().val());
                        
                        if (index > -1) {
                                searches.splice(index, 1);
                             }
                             $('.js_search-selec').val(searches);
                  
                    
              });
                $(this).parent().children().first().removeClass("fa-check");
                
                    }
       
        return false;
   
    });
    
    $("input[name=group_checkbox_color]").on('change', function() {
        
        if( $(this).is(':checked') ){ 
            $(this).parent().addClass("active");
            searches[searches.length]=$(this).val();
             $('.js_search-selec').val(searches);
            
            } else {
                
                $(this).parent().removeClass("active");
                var index = searches.indexOf($(this).val());
                if (index > -1) {
                                searches.splice(index, 1);
                $('.js_search-selec').val(searches);                  }
                  
                    }
       
        return false;
    });
    
     $("input[name= group_checkbox_typ]").on('change', function() {
        
        if(  $(this).is(':checked')){
        
            $(this).parent().children().first().addClass("fa-check");
            search_typ[search_typ.length]=$(this).val();
             $('.js_search-typ').val(search_typ);
            } else {
      
                $(this).parent().children().first().removeClass("fa-check");
                 var index = search_typ.indexOf($(this).val());
                if (index > -1) {
                                search_typ.splice(index, 1);
                $('.js_search-typ').val(search_typ);                  }
                    }
       
        return false;
    });
    
    $("input[name=group_checkbox]").on('change', function() {
        
        if( $(this).is(':checked') ){
            $(this).parent().children().first().addClass("fa-check");
            searches[searches.length]=$(this).val();
             $('.js_search-selec').val(searches);
            } else {
      
                $(this).parent().children().first().removeClass("fa-check");
                 var index = searches.indexOf($(this).val());
                if (index > -1) {
                                searches.splice(index, 1);
                $('.js_search-selec').val(searches);                  }
                    }
       
        return false;
    });
    
    $('.cbox_epaisseur').children().each(function () {
    count_epaisseur_div+=1;
    if(count_epaisseur_div>show_nb){
        $(this).addClass("hidden");
    }
     if(count_epaisseur_div==show_nb){
        $(this).parent().append("<div class='affiche_plus epaisseur_plus'><span>Afficher toutes les epaisseurs</span></div>");
    }
    });
    
    $('.cbox_couleur').children().last().children().each(function () {
    count_couleur_div+=1;
    if(count_couleur_div>show_nb){
        $(this).addClass("hidden");
    }
      if(count_couleur_div==show_nb){
        $(this).parent().append("<div class='affiche_plus couleur_plus'><span>Afficher toutes les couleurs</span></div>");
    }
    });
    
     $('.cbox_type').children().each(function () {
    count_type_div+=1;
    if(count_type_div>show_nb){
        $(this).addClass("hidden");
    }
    if(count_type_div==show_nb){
        $(this).parent().append("<div class='affiche_plus type_plus'><span>Afficher toutes les types</span></div>");
    }
    });
    
    $('.cbox_model').children().each(function () {
    count_model_div+=1;
    if(count_model_div>show_nb){
        $(this).addClass("hidden");
    }
     if(count_model_div==show_nb){
        $(this).parent().append("<div class='affiche_plus model_plus'><span>Afficher toutes les Models</span></div>");
    } 
    });
//     model
    $('.circle-down-model').click(function (event) {
       
        $('.circle-down-model').addClass("hidden");
        $('.circle-up-model').removeClass("hidden");
        $('.cbox_model').children().each(function () {
           if($(this).hasClass("hidden")){
            $(this).removeClass("hidden");
        }  
        else { 
        }
         });
        
        return false;
    });

    $('.circle-up-model').click(function (event) {
        
        $('.circle-up-model').addClass("hidden");
        $('.circle-down-model').removeClass("hidden");
        $('.cbox_model').children().each(function () {
           if($(this).hasClass("hidden")){
        }  
        else { 
         $(this).addClass("hidden");
        }
         });
        
        
        return false;
    });
//     epaisseur
     $('.circle-down-epaisseur').click(function (event) {
       
        $('.circle-down-epaisseur').addClass("hidden");
        $('.circle-up-epaisseur').removeClass("hidden");
        $('.cbox_epaisseur').children().each(function () {
           if($(this).hasClass("hidden")){
            $(this).removeClass("hidden");
        }  
        else { 
        }
         });
        
        return false;
    });

    $('.circle-up-epaisseur').click(function (event) {
        
        $('.circle-up-epaisseur').addClass("hidden");
        $('.circle-down-epaisseur').removeClass("hidden");
        $('.cbox_epaisseur').children().each(function () {
           if($(this).hasClass("hidden")){
        }  
        else { 
         $(this).addClass("hidden");
        }
         });
        
        
        return false;
    });
//      type
    $('.circle-down-type').click(function (event) {
        $('.type_plus').addClass("hidden");
        $('.circle-down-type').addClass("hidden");
        $('.circle-up-type').removeClass("hidden");
        $('.cbox_type').children().each(function () {
           if($(this).hasClass("hidden")){
            $(this).removeClass("hidden");
        }  
        else { 
        }
         });
        
        return false;
    });

    $('.circle-up-type').click(function (event) {
        
        $('.circle-up-type').addClass("hidden");
        $('.circle-down-type').removeClass("hidden");
        $('.cbox_type').children().each(function () {
           if($(this).hasClass("hidden")){
        }  
        else { 
         $(this).addClass("hidden");
        }
         });
        
        
        return false;
    });
//     couleur
    $('.circle-down-color').click(function (event) {
       
        $('.circle-down-color').addClass("hidden");
        $('.circle-up-color').removeClass("hidden");
        $('.couleur_plus').addClass("hidden");
        $('.cbox_couleur').children().each(function () {
           if($(this).hasClass("hidden")){
            $(this).removeClass("hidden");
        }  
        else { 
        }
         });
        
        return false;
    });

    $('.circle-up-color').click(function (event) {
        
        $('.circle-up-color').addClass("hidden");
        $('.circle-down-color').removeClass("hidden");
        $('.cbox_couleur').children().each(function () {
           if($(this).hasClass("hidden")){
        }  
        else { 
         $(this).addClass("hidden");
        }
         });
        
        
        return false;
    });
    $('.couleur_plus,.type_plus,.model_plus,.epaisseur_plus').click(function (event) { 
        
        $(this).parent().children().removeClass("hidden");
         $(this).addClass("hidden");
 return false;
    });
    
    $('.more2').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
        var html = show_content + '<span class="moreelipses">' + ellipsestext;        
        $(this).html(html);
        
    }
});
//     $('.model_plus').click(function (event) { 
//         
//         $(this).parent().children().removeClass("hidden");
//          $(this).addClass("hidden");
//  return false;
//     });
});
 