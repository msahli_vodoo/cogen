{
    'name': '4cotes Theme',
    'description': '4cotes Theme',
    'category': 'Website',
    'version': '1.0',
    'author': 'HauteVoltige Consultants',
    'depends': ['website','website_sale','website_crm','base_geolocalize'],
    'data': [
             'data/data.xml',
        'views/theme.xml',
        'views/layout.xml',
        'views/sale.xml',
#         'views/checkout.xml',
        'views/res_config.xml',
    ],
    'application': True,
}
