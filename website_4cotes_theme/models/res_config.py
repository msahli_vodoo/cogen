
from openerp.osv import fields, osv

class website_config_settings(osv.osv_memory):

    _inherit = 'website.config.settings'

    _columns = {
         
        'social_instagram': fields.related('website_id', 'social_instagram', type="char", string='Instagram Account'),
    }
