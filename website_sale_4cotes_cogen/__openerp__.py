# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants (Hafedh RAHMANI) (http://hautevoltige.net/).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Website Sales 4Cotes Cogen',
    'category': 'Website',
    'summary': 'Website Sales 4Cotes Cogen',
    'website': 'http://hautevoltige.net',
    'version': '8.0.1',
    'description': """
Add some functionality to Odoo e-commerce to cut pieces of wood with requested dimension by customer
==================

        """,
    'author': 'Haute-Voltige Consultants',
    'depends': ['website', 'sale', 'payment','base','website_sale','product','mrp','mrp_operations','website_crm','sale_mrp'],
    'data': [
        'views/templates.xml',
        'wizard/generete_variant_view.xml',
        'wizard/generete_price_view.xml', 
        'wizard/consume_product_view.xml',
        'wizard/scraps_view.xml',
        'views/product_view.xml',
        'data/uom_categ_data.xml',
        'data/hvc_mrp_stage_data.xml',
        'data/4cotes_template_category_datas.xml',
        'data/pricelist_data.xml',
        'data/4cotes_attribute_value_datas.xml',
        'data/4cotes_type_product_datas.xml',
        'data/data_menu.xml',
        'data/4cotes_attribute_family_data.xml',
        'data/mrp_data.xml',
        'data/product_precision_data.xml',
        'data/res.country.state.csv',
#         'data/ir_cron_data.xml',
        'views/sale_order.xml',
        'views/mrp_view.xml',
        'views/mrp_workcenter_view.xml',
        'views/company_view.xml',
        'views/attribute_value_galerie_images_view.xml',
        'security/ir.model.access.csv',
        'security/website_sale_4cotes_cogen.xml',
#         'views/mrp_report.xml',
        'views/report_production_lot.xml', 
        'views/snippets.xml',
        'views/template_materiaux.xml',
        'views/template_portes_et_tiroires.xml',
        'views/template_shop.xml',
        'views/template_contactus.xml',
        'views/template_home.xml',
        'views/template_how_it_work.xml',
        'views/template_payment.xml',
        'views/templates_project.xml',
        'views/templates_checkout.xml',
        'views/template_product_detail.xml',
        'views/template_cart_step1.xml',
        'views/web_client_template.xml'
    ],
    'qweb': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
