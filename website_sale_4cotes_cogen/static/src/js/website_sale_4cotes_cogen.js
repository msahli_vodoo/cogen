
$(document).ready(function () {

   

	$('.js_addr_payment').wrapAll('<p class="big"></p>');
	 $('.js_street').attr("disabled", true);
	    $('.js_street').replaceWith(function () {
	    return $('<div/>', {
	        html: $(this).val()
	    }).addClass('js_street');
	    });
	    $('p > br').remove();
	 $('.js_street').attr("style", "background-color: white; border-style:none; text-align: center; height: auto;");
	 $('.js_code_postal').attr("disabled", true);            
	    $('.js_code_postal').attr("style", "background-color: white; border-style:none; text-align: center;");
	    $('.js_ville').attr("disabled", true);
	    $('.js_ville').attr("style", "background-color: white; border-style:none; text-align: center;");
	    
	    $('.js_code_postalLivraison').attr("disabled", true);
	    $('.js_code_postalLivraison').attr("style", "background-color: white; border-style:none; text-align: center;");
	    $('.js_streetLivraison').attr("disabled", true);
	    $('.js_streetLivraison').attr("style", "background-color: white; border-style:none; text-align: center; resize: none; overflow: hidden;");
	    $('.js_villeLivraison ').attr("disabled", true);
	    $('.js_villeLivraison ').attr("style", "background-color: white; border-style:none;text-align: center;");
	 $(":Button:contains('Reinitialiser le mot de passe')").css("margin-left", "40px");
	 
	  
	      $('.js_streetLivraison').replaceWith(function () {
	      return $('<div/>', {
	          html: $(this).val()
	      }).addClass('js_streetLivraison');
	      });
	      $('p > br').remove();
	 var clicks = 0;
	 $(".navbar-header Button.navbar-toggle:first-child").click(function(){
	         if (window.matchMedia("(min-width: 768px)").matches) {
	            if(clicks == 0){
	             console.log('____Test akrem button toggle bootstrap_____');
	             $("#oe_editzone").attr('style', 'display: inline-block !important; width: 300px !important');
	             $("#oe_systray > *").css('display','inline-block');
	             clicks++;
	         }else{
	             console.log('____Test2 akrem button toggle bootstrap_____');
	             $("#oe_editzone").attr('style', 'display: none !important');
	                $("#oe_systray > *").css('display','none');
	                clicks--;
	         }
	         }
	 });
	
    var $payment = $("#payment_method");
    var $payment_wiretransfer = $("#payment_wiretransfert");
    var $payment_paypal = $("#payment_paypal");
      $("span.fa-long-arrow-right",$payment).addClass("hidden");
      $("span",$payment_paypal).addClass("paypal");
      $("span",$payment_wiretransfer).addClass("wiretransfer");
    $payment.on("click", 'button[type="submit"],button[name="submit"]', function (ev) {
      ev.preventDefault();
      ev.stopPropagation();
      var $form = $(ev.currentTarget).parents('form');
      var acquirer_id = $(ev.currentTarget).parents('div.oe_sale_acquirer_button').first().data('id');
      if (! acquirer_id) {
        return false;
      }
      if (acquirer_id==2) {
      openerp.jsonRpc('/shop/payment/transaction/' + acquirer_id, 'call', {}).then(function (data) {
        $form.submit();
      });
      };
   });
	
	
//abir    
	
	$('.oe_website_sale').each(function () {
        var oe_website_sale = this;     
        $(oe_website_sale).on("click", "#clear_cart_button", function () {
//            var line_id2=$('#product_line_id').val();
        	var line_id2=($("input[name='product_line_id']", oe_website_sale)).val();
            openerp.jsonRpc("/shop/clear_cart", "call", {'line_id':line_id2}).then(function(){
            
                location.reload();
             });
             return false;
        });
        
        var $shippingDifferent = $("select[name='shipping_id']", oe_website_sale);
        $shippingDifferent.change(function (event) {
        var $snipping = $(".js_shipping", oe_website_sale);
        
        if ($shippingDifferent.val()==1){
             $snipping.addClass("hidden");
            
        }
        else{
             $snipping.removeClass("hidden");
            
        }

      
        });
        
     });    
	
	
	
	
	function empty(){
	    $(".js_final_length").val('');
	    $(".js_final_width").val('');
	    $(".js_final_cost").val('0');
	    $(".js_final_price_display").val('0.00');
	    $('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
	}
	function init_rec(){
	    var $input_length = $(".js_length");
	    var $input_width = $(".js_width");
	    var $max_length = $(".js_max_length").val();
	    var $width_decimal = $("#width_decimal_select").val();
	    var $length_decimal = $("#length_decimal_select").val();
	    var $max_width = $(".js_max_width").val();
	    

	    if ($("input[name='measurement_system']:checked").val() == 'metric'){
	        $max_length = $max_length * 25.4;
	        $max_width = $max_width * 25.4;
	    }
	    var final_surface;
	    if ($("input[name='measurement_system']:checked").val()=='imperial'){
	        final_surface = Math.round(10000/144*[parseInt($input_length.val())+parseFloat($length_decimal)]*[parseInt($input_width.val())+parseFloat($width_decimal)])/10000;
	        $(".js_final_surface").val(final_surface); 
	        $(".js_final_width").val(parseInt($input_width.val())+parseFloat($width_decimal));
	        $(".js_final_length").val(parseInt($input_length.val())+parseFloat($length_decimal));
	    }else{
	        final_surface =Math.round(10000*(parseInt($input_length.val())*parseInt($input_width.val())/(645.16*144)))/10000;
	        $(".js_final_surface").val(final_surface); 
	        $(".js_final_width").val(Math.round(parseInt($input_width.val())/25.4*10000)/10000);
	        $(".js_final_length").val(Math.round(parseInt($input_length.val())/25.4*10000)/10000);
	    }
	    
	}

	function reset_interface() {
	    if ($("input[name='measurement_system']:checked").val()=='imperial') {
	        $("#width_decimal_select, #length_decimal_select").css('display','block');
	        $("#mxwidth").text($("input[name='max_width']").val());
	        $("#mxlength").text($("input[name='max_length']").val());  
	        $(".js_width").val(parseInt($(".js_min_width").val()|| 1));
	        $(".js_length").val(parseInt($(".js_min_length").val()|| 1));        
	    }else {
	        $("#width_decimal_select, #length_decimal_select").css('display','none');
	        $("#mxwidth").text(Math.round($("input[name='max_width']").val()*25.4));
	        $("#mxlength").text(Math.round($("input[name='max_length']").val()*25.4));
	        $(".js_width").val(Math.round(25.4 * parseInt($(".js_min_width").val() || 1)));
	        $(".js_length").val(Math.round(25.4 * parseInt($(".js_min_length").val() || 1)));
	        parseInt($('input[name="template_id"]').val() || 0);
	    }  
	    $("#width_decimal_select").val('0');
	    $("#length_decimal_select").val('0');
	    $(".js_final_cost").val('0');
	    $(".js_price").val('0');
	    $('#valid_width, #valid_length, #integer_width, #integer_length').hide();
	    $(".js_length, .js_width").css("border","2px solid WhiteSmoke");
	}

	$('.oe_website_sale').each(function () {
        var oe_website_sale = this;
        reset_interface();
        init_interface();
        var result;
        Array.prototype.next = function() {
       	 	return this[++this.current];
        };
        Array.prototype.prev = function() {
        	return this[--this.current];
        };
        Array.prototype.current = 0;
        function reset_part() {
            if ($("input[name='measurement_system']:checked").val()=='imperial') {
                $("#mxwidth").text($("input[name='max_width']").val());
                $("#mxlength").text($("input[name='max_length']").val());  
            }else {
                $("#mxwidth").text($("input[name='max_width']").val()*25.4);
                $("#mxlength").text($("input[name='max_length']").val()*25.4);
            }   
        }
        
   
        var $form = $("form[action='/shop/cart/update']");
        
        var p = $(".js_final_price_display").val();
        var p2 = $(".js_final_price_display").text();
        if ((parseFloat(p) == 0) || (p2=='undefined') ||(!$(".js_final_price_display").val() ) ){
        	$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
        }
        
        
        
        // ************************* 4 Cotes: Nouveau configurateur des produits sur mesure  ********************//
        $(oe_website_sale).on('click',".nav-tabs-imperial-metric > li", function () {
        	$(".nav-tabs-imperial-metric > li").find('input[type="radio"]').removeAttr('checked');
            $(this).find('input[type="radio"]').attr('checked','checked').change();
        });
        
        var category_measurement=$('input[name="category_measurement"]').val();  
        if (category_measurement == 'normal' || category_measurement==  'quincaillerie'){
        	$('#add_to_cart, .js_add_to_cart').removeAttr('disabled');
        }
 
   
    function update_width(){
    	$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
    	$('#integer_width, #valid_width').hide();
    	var f_width_decimal = $("#width_decimal_select").find("option:selected").text();
    	
        var is_ok_width=check_width();
        if (is_ok_width == -1 || is_ok_width == -2){
            if ($("input[name='measurement_system']:checked").val() == 'metric')
                $(".measurePreviewWidth").html("-- mm");
            else
                $(".measurePreviewWidth").html(" --\" ");
        }
        if (is_ok_width == -1 ){
            $('#integer_width').show();
            empty();
            return false;
        }
        else if (is_ok_width == -2 ){
            $('#valid_width').show();
            empty();
            return false;
        }
        else{
        	$('.js_width').css("border","2px solid WhiteSmoke");
            var f_width_mm = $(".js_width").val();
            if ($("input[name='measurement_system']:checked").val() == 'metric')
            	$(".measurePreviewWidth").html(f_width_mm+" mm");
            else{
            	$(".measurePreviewWidth").html(f_width_mm+" \" "+f_width_decimal);
                var $dess = $(".js_new_width");
                $dess.html("<span >"+ $('#width_decimal_select').find("option:selected").text()+ "</span>");
            }
            var $desc = $(".js_new_value_width");
            $desc.html("<span>"+ $(".js_width" ).val()+ "</span>");
            return false;
        }
    	
    }
    
    function update_length(){
    	$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
    	$('#integer_length, #valid_length').hide();
    	var f_length_decimal = $("#length_decimal_select").find("option:selected").text();
        var is_ok_length=check_length();
        if (is_ok_length == -1 || is_ok_length == -2 ){
            if ($("input[name='measurement_system']:checked").val() == 'metric')
                $(".measurePreviewLength").html("-- mm");
            else
                $(".measurePreviewLength").html(" --\" ");
        }
        if (is_ok_length == -1){
            $('#integer_length').show();
            empty();
            return false;
        }
        else if (is_ok_length == -2){
            $('#valid_length').show();
            empty();
            return false;
        }
        else{
        	$(".js_length").css("border","2px solid WhiteSmoke");
            var f_length_mm = $(".js_length").val();
            if ($("input[name='measurement_system']:checked").val() == 'metric')
            	$(".measurePreviewLength").html(f_length_mm+" mm");
            else{
            	$(".measurePreviewLength").html(f_length_mm+" \" "+f_length_decimal);
            }
           
            return false;
        }
    }


    function reset_part_interface() {
        if ($("input[name='measurement_system']:checked").val()=='imperial') {
            $("#width_decimal_select, #length_decimal_select").css('display','block');
            $("#mxwidth").text($("input[name='max_width']").val());
            $("#mxlength").text($("input[name='max_length']").val());        
        }else {
            $("#width_decimal_select, #length_decimal_select").css('display','none');
            $("#mxwidth").text(Math.round($("input[name='max_width']").val()*25.4)); 
            $("#mxlength").text(Math.round($("input[name='max_length']").val()*25.4));
            parseInt($('input[name="template_id"]').val() || 0);
        }  
        $("#width_decimal_select").val('0');
        $("#length_decimal_select").val('0');
        $(".js_final_cost").val('0'); //? rev
        $(".js_price").val('0'); //? rev
        $('#valid_width, #valid_length, #integer_width, #integer_length').hide();
        $(".js_length, .js_width").css("border","2px solid WhiteSmoke");
    }

     $(oe_website_sale).on('change',".js_measurement_system", function () {
        reset_part_interface();
        $('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
        var f_length_mm = $(".js_length").val();
        var f_width_mm = $(".js_width").val();
        if ($("input[name='measurement_system']:checked").val() == 'metric'){
            $(".js_width").val(Math.round(25.4 * f_width_mm));
            $(".js_length").val(Math.round(25.4 * f_length_mm));
            update_width();
            update_length();
            $(".js_measurement_system_metric").addClass("selected_radio");
            $(".js_measurement_system_imperial").removeClass("selected_radio");
            $(".td_fa-quote").html("<span class='fa-quote-right_mm'>mm</span>"); 
            $(".sg_b_title_unite").html("en mm");
        }else{
            $(".js_width").val(Math.round(1/25.4 * f_width_mm));
            $(".js_length").val(Math.round(1/25.4 * f_length_mm));
        	update_width();
            update_length();
            
        	parseInt($('input[name="template_id"]').val() || 0);
            $(".js_measurement_system_metric").removeClass("selected_radio");
            $(".js_measurement_system_imperial").addClass("selected_radio");
            $(".td_fa-quote").html("<span class='fa-quote-right_mm' >po</span>");
            $(".sg_b_title_unite").html("en pouce");
        }
        set_product_description();
        return false;
    });

    $(oe_website_sale).on('change','.js_top_side, .js_right_side, .js_bottom_side , .js_left_side', function (ev) {
    	$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
    	set_slides_ids();
    	
        return false;
    });
    
    $(oe_website_sale).on('change','.js_final_sides', function (ev) {
    	set_product_description();
        return false;
    });

    


    $(oe_website_sale).on('change','.js_top_side', function () {
        set_slides_ids();
        if (this.options[this.selectedIndex] != undefined){
        color=this.options[this.selectedIndex].getAttribute('name');
        var top_side = $(".js_top_side").find("option:selected").text();
        var $cote = $('#bandeChant_top');
        var $parent = $cote.parent();
        var direction = "top";
        var borderClass = "showBorder_" + direction;
        if($parent.hasClass(borderClass)){ 
            if(color == "False"){  $("#bandeDeChant.showBorder_top .borderOverBg").css({"border-top-color":''});
               
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else {
                $("#bandeDeChant.showBorder_top .borderOverBg").css({"border-top-color":color});
            }
        } else {  
            if(color == "False"){
                $("#bandeDeChant.showBorder_top .borderOverBg").css({"border-top-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else {    
                $cote.addClass("selected");
                $parent.addClass(borderClass);
                $("#bandeDeChant.showBorder_top .borderOverBg").css({"border-top-color":color});
            }
        }
        
        }
        return false;
    });
    
    $(oe_website_sale).on('change','.js_right_side', function () {
        set_slides_ids();
        if (this.options[this.selectedIndex] != undefined){
        color=this.options[this.selectedIndex].getAttribute('name');
        var right_side = $(".js_right_side").find("option:selected").text();
        var $cote = $('#bandeChant_right');
        var $parent = $cote.parent();
        var direction = "right";
        var borderClass = "showBorder_" + direction;
        if($parent.hasClass(borderClass)){ 
            if(color == "False"){  
                $("#bandeDeChant.showBorder_right .borderOverBg").css({"border-right-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else {    
                $("#bandeDeChant.showBorder_right .borderOverBg").css({"border-right-color":color});
            }
        } else {  
            if(color == "False"){     
                $("#bandeDeChant.showBorder_right .borderOverBg").css({"border-right-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else { $cote.addClass("selected");
                $parent.addClass(borderClass);
                $("#bandeDeChant.showBorder_right .borderOverBg").css({"border-right-color":color});
            }
        }
        }
        return false;
    });
    
    $(oe_website_sale).on('change','.js_bottom_side', function () {
        set_slides_ids();
        if (this.options[this.selectedIndex] != undefined){
        color=this.options[this.selectedIndex].getAttribute('name');
        var bottom_side = $(".js_bottom_side").find("option:selected").text();
        var $cote = $('#bandeChant_bottom');
        var $parent = $cote.parent();
        var direction = "bottom";
        var borderClass = "showBorder_" + direction;
        if($parent.hasClass(borderClass)){ 
            if(color == "False"){  
                $("#bandeDeChant.showBorder_bottom .borderOverBg").css({"border-bottom-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else {    
                $("#bandeDeChant.showBorder_bottom .borderOverBg").css({"border-bottom-color":color});
            }
        }else {  
            if(color == "False"){  
                $("#bandeDeChant.showBorder_bottom .borderOverBg").css({"border-bottom-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            }else{    
                $cote.addClass("selected");
                $parent.addClass(borderClass);
                $("#bandeDeChant.showBorder_bottom .borderOverBg").css({"border-bottom-color":color});
            }
        }
        }
        return false;
    });
    
    
    $(oe_website_sale).on('change','.js_left_side', function () {
        set_slides_ids();
        if (this.options[this.selectedIndex] != undefined){
        color=this.options[this.selectedIndex].getAttribute('name');
        var left_side = $(".js_left_side").find("option:selected").text();
        var $cote = $('#bandeChant_right');
        var $cote = $('#bandeChant_left');
        var $parent = $cote.parent();
        var direction = "left";
        var borderClass = "showBorder_" + direction;
        if($parent.hasClass(borderClass)){ 
            if(color == "False"){  
                $("#bandeDeChant.showBorder_left .borderOverBg").css({"border-left-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else { 
                $("#bandeDeChant.showBorder_left .borderOverBg").css({"border-left-color":color});
            }
        } else {  
            if(color == "False"){ $("#bandeDeChant.showBorder_left .borderOverBg").css({"border-left-color":''});
                $cote.removeClass("selected");
                $parent.removeClass(borderClass);
            } else {
                $cote.addClass("selected");
                $parent.addClass(borderClass);
                $("#bandeDeChant.showBorder_left .borderOverBg").css({"border-left-color":color});
            }
        }
        }
        
        return false;
    });
    
    $(oe_website_sale).on('change','.js_all_sides', function (ev) {
    	$(".js_top_side, .js_right_side, .js_bottom_side, .js_left_side").val($(this).val());
   	 	$('.js_top_side, .js_bottom_side, .js_left_side, .js_right_side').change();
   	 	set_slides_ids();
   	 	return false;
    });
    
	function init_interface() {
        if ($("input[name='measurement_system']:checked").val()=='imperial') {
            $("#width_decimal_select, #length_decimal_select").css('display','block');
          
            
            $("#mxwidth").text($("input[name='max_width']").val());
            $("#mxlength").text($("input[name='max_length']").val());           
        }else {
            $("#width_decimal_select, #length_decimal_select").css('display','none');
          
            
            $("#mxwidth").text($("input[name='max_width']").val()*25.4);
            $("#mxlength").text($("input[name='max_length']").val()*25.4);
        }
		
		$('#js_thickness_select option:nth-child(1)').prop('selected', true);
		 
		 
				
    }
	function set_slides_ids() {
		if ($(".js_top_side").val()!=undefined){
			var top_side = $(".js_top_side").val().split('-')[2];
			var right_side = $(".js_right_side").val().split('-')[2];
			var bottom_side = $(".js_bottom_side").val().split('-')[2];
			var left_side = $(".js_left_side").val().split('-')[2];
			var sides=top_side+bottom_side+left_side+right_side;
			$(".js_final_sides").val(sides).trigger('change');
		}
	}
	
    var template_id=parseInt($('input[name="template_id"]').val() || 0);
    var category_measurement=$('input[name="category_measurement"]').val();
    if (category_measurement == 'normal' || category_measurement==  'quincaillerie'){
        openerp.jsonRpc("/shop/product/get_attributes_lines_price", 'call', {'template_id':template_id ,'values':[],'final_surface':0.0}).then(function (data) {
        	$(".js_final_cost").val(data['cost']);
        });
    }

    
   
    function hiding(){
        $('#valid_length, #valid_width, #integer_length, #integer_width').hide();  
    }
    
    function get_edge_length(){
    	var edge_length=0.0;
    	final_sides=$(".js_final_sides").val();
    	final_length=parseFloat($(".js_final_length").val() || 0.0);
    	final_width=parseFloat($(".js_final_width").val() || 0.0);
    	if (final_sides.charAt(0)=='1'){
    		edge_length+=final_length;
    	}
    	
    	if (final_sides.charAt(1)=='1'){
    		edge_length+=final_length;
    	}
    	
    	if (final_sides.charAt(2)=='1'){
    		edge_length+=final_width;
    	}
    	
    	if (final_sides.charAt(3)=='1'){
    		edge_length+=final_width;
    	}
    	return edge_length/12;
    }
    
    function recalculate(){
    	var edge_length=get_edge_length();
    	
        var $ul = $('ul.js_add_cart_variants:first');
        var $parent = $ul.closest('.js_product');
        var final_surface=$(".js_final_surface").val();
        var final_lenght=$(".js_final_length").val();
        var final_width=$(".js_final_width").val();
        var final_sides=$(".js_final_sides").val();
        var values = [];
        $parent.find('input.js_options_variants_change:checked, select.js_options_variants_change').each(function () {
        	values.push(+$(this).val());
        });
        var template_id=parseInt($('input[name="template_id"]').val() || 0);
        openerp.jsonRpc("/shop/product/get_attributes_lines_price", 'call', {'template_id':template_id ,
                                                                              'values':values,
                                                                              'final_surface':parseFloat(final_surface),
                                                                              'final_lenght':parseFloat(final_lenght),
                                                                              'final_width':parseFloat(final_width),
                                                                              'final_sides':final_sides,
                                                                              'edge_length':edge_length
                                                                              }).then(function (data) {
        		$(".js_final_cost").val(data['cost']);
        		$(".js_final_price_display").val(Math.round(data['price']*100)/100);
        		var p = $(".js_final_price_display").val();
        		if (parseFloat(p)>0 ){
        			$('#add_to_cart, .js_add_to_cart').removeAttr('disabled');
        		}
        		else{
        			$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
        		}
        });
    }
    
//    moez
    
    function check_geom(){
    	var f_width = parseInt($(".js_final_width").val()||1);
    	var f_length = parseInt($(".js_final_length").val()||1);
    	var geom="horizontal";
    	if (f_width < f_length ) {
    	  
    		geom="horizontal";

    		$(".wood_sides_right").css({"height":"100%"});
    		$(".wood_sides_right").css({"width":"90%"});
    		return geom;
    	}
     
    	if (f_width > f_length ) {
    	  
    		geom="vertical";
    		
    	    $(".wood_sides_right").css({"height":"100%"});
            $(".wood_sides_right").css({"width":"60%"});

    		return geom;
    		
    	}
    	if (f_width == f_length ) {
        	
    		geom="carre";
    		$(".wood_sides_right").css({"height":"100%"});
            $(".wood_sides_right").css({"width":"70%"});
    		
    		

    		return geom;
    	}
    }
//    moez
    function init_rec1(){
        var $input_length = $(".js_length");
        var $input_width = $(".js_width");
        var $max_length = $(".js_max_length").val();
        var $width_decimal = $("#width_decimal_select").val();
        var $length_decimal = $("#length_decimal_select").val();
        var $max_width = $(".js_max_width").val();
        

        if ($("input[name='measurement_system']:checked").val() == 'metric'){
            $max_length = $max_length * 25.4;
            $max_width = $max_width * 25.4;
        }
        
        var final_surface;
        var div_with=25.4;
        var div_length=25.4;
        
        if ($input_width.val()==25)
            div_with=25;
        if ($input_length.val()==25)
            div_length=25;
            
        if ($("input[name='measurement_system']:checked").val()=='imperial'){
            final_surface = Math.round(10000/144*[parseInt($input_length.val())+parseFloat($length_decimal)]*[parseInt($input_width.val())+parseFloat($width_decimal)])/10000;
            $(".js_final_surface").val(final_surface); 
            $(".js_final_width").val(parseInt($input_width.val())+parseFloat($width_decimal));
            $(".js_final_length").val(parseInt($input_length.val())+parseFloat($length_decimal));
            $(".td_fa-quote").html("<span class='fa-quote-right_mm' >po</span>");
        }else{
            final_surface =Math.round(10000*(parseInt($input_length.val())*parseInt($input_width.val())/(645.16*144)))/10000;
            $(".js_final_surface").val(final_surface); 
            $(".js_final_width").val(Math.round(parseInt($input_width.val())/div_with*10000)/10000);
            $(".js_final_length").val(Math.round(parseInt($input_length.val())/div_length*10000)/10000);
            var ll = $(".js_final_length").val();
            var ww = $(".js_final_width").val();
        }
        
    }  
    
    function check_width(){
        init_rec1();
        var value = Number($(".js_width" ).val());
        if (Math.floor(value) != value || value<=0 ) {
            $(".js_width" ).css("border","2px solid red");
            return -1;
        }
        var width = parseFloat($( ".js_final_width" ).val()|| 0);
        if (isNaN(width)) {
            $(".js_width").css("border","2px solid red");
            return -1;
        }
        var max_width = parseFloat($(".js_max_width").val() || Infinity);
        var min_width = parseFloat($(".js_min_width").val() || 1);
         if ((width > max_width) || (width < min_width)) {
                $(".js_width").css("border","2px solid red");
                return -2;
            }
        return 1;
    }
    
    
    function check_length(){
        init_rec1();
        var value = Number($(".js_length" ).val());
        if (Math.floor(value) != value || value<=0 ) {
            $(".js_length" ).css("border","2px solid red");
            return -1;
        }
        var length = parseFloat($(".js_final_length" ).val()|| 0);
        if (isNaN(length)) {
                $(".js_length").css("border","2px solid red");
                return -1;
            }
        var max_length = parseFloat($(".js_max_length").val()  || Infinity);
        var min_length = parseFloat($(".js_min_length").val()  || 1);        
        if ((length > max_length) || (length < min_length) ) {
                $(".js_length").css("border","2px solid red");
                return -2;
            } 
        return 1;
    }
    
    $(oe_website_sale).on('click','.js_button_calcul', function () {
  	  init_rec();
  	  var is_ok_length=check_length();
  	  var is_ok_width=check_width();
  	  if ((is_ok_length !== 1) || (is_ok_width !== 1)) { 
  		  if (is_ok_length == -1)
  			  $('#integer_length').show();
  		  if (is_ok_length == -2)
  			  $('#valid_length').show();
  		  if (is_ok_width == -1)   
  			  $('#integer_width').show();
  		  if (is_ok_width == -2)  
  			  $('#valid_width').show();
  		  empty();
  		  return false; 
  	  }
  	  hiding();
  	  recalculate();
  	  return true;
    });

    $(window).load(function() {

    	$("select.js_material_type").change();
    	var f_width_decimal = $("#width_decimal_select").find("option:selected").text();
        var f_width_mm = $(".js_width").val();
        if ($("input[name='measurement_system']:checked").val() == 'metric')
        	$(".measurePreviewWidth").html(f_width_mm+" mm");
        else
        	$(".measurePreviewWidth").html(f_width_mm+" \" "+f_width_decimal);
         
        var f_length_decimal = $("#length_decimal_select").find("option:selected").text();
        var f_length_mm = $(".js_length").val();
        if ($("input[name='measurement_system']:checked").val() == 'metric')
        	$(".measurePreviewLength").html(f_length_mm+" mm");
        else
        	$(".measurePreviewLength").html(f_length_mm+" \" "+f_length_decimal);
        $('.js_top_side, .js_bottom_side, .js_left_side, .js_right_side').change();
        
        $("#js_thickness_select").change();
        
        
    });
 
    function price_to_str(price) {
        price = Math.round(price * 100) / 100;
        var dec = Math.round((price % 1) * 100);
        return price + (dec ? '' : '.0') + (dec%10 ? '' : '0');
    }
    
    function set_product_description() {
    	description="";
    	var $parent = $('.js_product, #id_detail_product');
    	description=$parent.find("input.js_options_variants_change:checked, select.js_options_variants_change, .js_final_sides, .js_final_length, .js_final_width, input[name='add_qty']").map(function(){
    		if ($(this).attr("attr_name")!=undefined){
    			if ($(this).is('select.js_options_variants_change')){
    				return $(this).attr("attr_name")+": "+$(this).find("option:selected").text().trim();	
    			}
    			if ($(this).is('input.js_options_variants_change:checked')){
    				return $(this).attr("attr_name")+": "+$(this).attr('title');
    			}	
    		}
    	
    		if ($(this).is('.js_final_sides')){
    			if ($('.js_product_category_measurement').val() == 'planches'){
    				return "Sides: "+$(this).val();
    			}
    		}
    		if ($(this).is('.js_final_length')){
    				return "Longueur: "+$(this).val();
    		}
    		
    		if ($(this).is('.js_final_width')){
				return "largeur: "+$(this).val();
			}
    		
    		if ($(this).is("input[name='add_qty']")){
				return "QTE: "+$(this).val();
			}
    		}).get().join(" | ");
    	
    	$('.js_product_desccription').html(description);
       return false;
    }
    
    
    function draw_epaisseur(){//TO BE REVIEWED
        var epaisseurs=[];
        var epaisseur_css=0;
        var dict = {};
        $("#js_thickness_select").find("option").each(function() {
        	if(/([1-9])+ ([0-9])+((\/[1-9]([0-9])*))?"$/.test($(this).attr("name"))){
        		
        		var arr = $(this).attr("name").replace('"','').split(' ');
        		
        		dict[eval(eval(arr[0])+eval(arr[1]))]=$(this).attr("name");
        		
        	}
        	else{
        		dict[eval($(this).attr("name").replace('"',''))]=$(this).attr("name");
        	}
       	 
      	});
        var max_epaisseur_key = Math.max.apply(null,Object.keys(dict));
        if(/([1-9])+ ([0-9])+((\/[1-9]([0-9])*))?"$/.test( $("#js_thickness_select option:selected").attr("name"))){
        	var arr = $("#js_thickness_select option:selected").attr("name").replace('"','').split(' ');
        	var epaisseur_selected_key=eval(eval(arr[0])+eval(arr[1]))

        }
        else{
        	var epaisseur_selected_key= eval($("#js_thickness_select option:selected").attr("name").replace('"',''));
        }
        
        epaisseur_css=epaisseur_selected_key/max_epaisseur_key*100;
        var epaisseur_variation= $(".css_thickness_select").find("option:selected").attr("name");
        $(".span_epaisseur_variation").html(epaisseur_variation);
        $(".span_epaisseur_max").html(dict[max_epaisseur_key]);
        $(".epaisseur_variation").css({"height":epaisseur_css+'%'});
    }
    
    $(oe_website_sale).on('change','input.js_galerie_change', function () {
  
    	$('input.js_galerie_change:checked').each(function () {
    		$('.css_mop_galerie_image').removeClass("active");
    		$('.css_mop_galerie_image:has(input:checked)').addClass("active");
    		$('.product_model_mop_img').attr("src", "/website/image/product.attribute.value.galerie/" + $(this).val() + "/file_db_store");
    		
    		if ($(this).parents('li').prev().length==0){
    			$('.js_prev').css('visibility','hidden')
    		}
    		else{
    			
    			if ($(this).parents('li').prev().find('label').css('display') == 'none'){
        			$('.js_prev').css('visibility','hidden');
        		}
        		else{
        			$('.js_prev').css('visibility','visible');
        		}
    		}
    		
    		if ($(this).parents('li').next().length==0){
    			$('.js_next').css('visibility','hidden');
    		}
    		else{
    			
    			if ($(this).parents('li').next().find('label').css('display') == 'none'){
        			$('.js_next').css('visibility','hidden');
        		}
        		else{
        			$('.js_next').css('visibility','visible');
        		}
    		}
    		
    	});
    	
    });
    
    $(oe_website_sale).on('click','#prev', function () {
    	$('input.js_galerie_change:checked').removeAttr( "checked" )
					.parents('li').prev().find('input').attr( "checked","checked");
    	$('input.js_galerie_change:checked').change();
    	
    });
    

    
    function main_value_change($main,$parent) {

    	if ($main.attr("friends")!=undefined){
    	
    		friends_ids=$main.attr("friends");
    		var sof=0;
    		var sif=0;
    		$parent.find('input.js_basic_characteristics_change').each(function () {
    			if (! $(this).hasClass("js_main_value")){
		        	if (friends_ids.indexOf($(this).val()) < 0){
		        		$(this).closest("li").hide();
		  
		        	}
		        	else{
		        		$(this).closest("li").show();
		        	
		        		if (sif==0){
		        			$(this).attr( "checked","checked");
		        			sif++;
		        		}
		        	}
		        }
    		});
    		$parent.find('select.js_basic_characteristics_change').find('option').each(function () {
    			  if (! $(this).parent().hasClass("js_main_value")){
    		        	if (friends_ids.indexOf($(this).val()) < 0){
    		        		$(this).hide();
    		        	}
    		        	else{
    		        		$(this).show();
    		        		if (sof==0){
    		        			$(this).attr("selected","selected");
    		        			sof++;
    		        		}
    		        	}
    		        }
    		});
    	
    			
    		
    	}
    
    	
    }

    
    $(oe_website_sale).on('click','#next', function () {
    	$('input.js_galerie_change:checked').removeAttr( "checked" )
    										.parents('li').next().find('input').attr( "checked","checked");
    	$('input.js_galerie_change:checked').change();
    });
    
    function update_template_values(basic_characteristics,$parent) {
    	$parent.find('input.js_basic_characteristics_change:checked').each(function () {
	    	
    		$('.css_basic_characteristic_color').removeClass("active");
	        $('.css_basic_characteristic_color:has(input:checked)').addClass("active");
	        //TO BE REVIEWED: DICTIONARY BC
	        basic_characteristics.push("("+parseInt($(this).attr('attribute_id'))+","+parseInt($(this).val())+")");
	        
	        if (($(this).data("v_bc_type") !=undefined) && ($(this).data("v_bc_type")=='color_texture')){
            	$(".bc_value_color_texture_img").attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
            	$(".bc_value_color_texture_name").html("<span>"+ $(this).attr("title")+ "</span>");
                if ($(this).data("v_color_texture_desc") !=  undefined)
            	   $(".bc_value_color_texture_desc").html("<span>"+ $(this).data("v_color_texture_desc")+ "</span>");   
            	else
            	   $(".bc_value_color_texture_desc").html("<span>" + ' ' + "</span>");    
            }
	        
            if (($(this).data("v_bc_type_ess") !=undefined) && ($(this).data("v_bc_type_ess")=='essence_color')){
                $(".bc_value_essence_color_img").attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
                $(".bc_value_essence_color_name").html("<span>"+ $(this).attr("title")+ "</span>");
                if ($(this).data("v_essence_color_desc") !=  undefined)
                    $(".bc_value_essence_color_desc").html("<span>"+ $(this).data("v_essence_color_desc")+ "</span>");
                else
                    $(".bc_value_essence_color_desc").html("<span>" + ' ' + "</span>"); 
            }
            
            if (($(this).data("v_bc_type_teint") !=undefined) && ($(this).data("v_bc_type_teint")=='color_teinture')){ 
                $(".bc_value_color_teinture_img").attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
                $(".bc_value_color_teinture_name").html("<span>"+ $(this).attr("title")+ "</span>");
                if ($(this).data("v_color_teinture_desc") !=  undefined)
                    $(".bc_value_color_teinture_desc").html("<span>"+ $(this).data("v_color_teinture_desc")+ "</span>");    
                else
                    $(".bc_value_color_teinture_desc").html("<span>" + ' ' + "</span>");
            }
	    });
	    
	    $parent.find('select.js_basic_characteristics_change').each(function () {
	    	//TO BE REVIEWED: DICTIONARY BC
	        basic_characteristics.push("("+parseInt($("option:selected", this).attr('attribute_id'))+","+parseInt($(this).val())+")");
	        if ($("option:selected", this).parent().hasClass("css_thickness_select")){
	        	draw_epaisseur();
	        }
	        
	        if ($("option:selected", this).parent().hasClass("css_mop_select")){
	        	$('label.css_mop_galerie_image').removeClass("active").css('display','none');
	        	$('input.js_galerie_change').removeAttr( "checked" );
	        	$('.color_texture_all').css('display','none');

	        	if ($("option:selected", this).attr('galerie_image_ids')!= undefined){
	        		var arr=$.makeArray( $("option:selected", this).attr('galerie_image_ids').replace('product.attribute.value.galerie(','').replace(')','').replace(/\s/g, '').split(",") )
	        		var i=0;
	        		$('label.css_mop_galerie_image').each(function () {
	        			if ($.inArray($(this).children().first().val(), arr )>-1){
	        				if (i==0){
	        					$(this).children().first().attr( "checked","checked");
	        					$('.color_texture_all').css('display','block');

	        					$('.product_model_mop_img').attr("src", "/website/image/product.attribute.value.galerie/" + $(this).children().first().val() + "/file_db_store");
	        					i++;
	        				}
	        				$(this).css('display','block');
	        				
	        			}
	        			
	        		});
	        		 $('label.css_mop_galerie_image:has(input:checked)').addClass("active");
	        		 $('input.js_galerie_change:checked').change();
	        	}
	        } 
	    });
	    
	   HoldOn.open({
	    	theme:'sk-fading-circle',
	    	message:"<h4>Veuillez patienter un instant.</h4>"
	    });
	   openerp.jsonRpc("/shop/material/get_product_model", 'call', {'material_type_id':parseInt($('.js_material_type').val()),'category_measurement':$('#category_measurement').val(),'category_id':parseInt($('.js_public_category').val()), 'basic_characteristics':basic_characteristics}).then(function (data) {
		   
		   $('#selling_options').replaceWith(data['selling_options']);
           $('#bloc_product_detail_ids').replaceWith(data['product_detail_ids']);
           $('#bloc_options_variants').replaceWith(data['options_variants']);
           $("input.js_options_variants_change:checked, select.js_options_variants_change").change();
           $(".js_width, #width_decimal_select,.js_length, #length_decimal_select").change();
           
           if (data['template_name'] != undefined) {
        	   $('.js_template_name').replaceWith("<li t-if=\"category\" class=\"js_template_name\">"+data['template_name']+"</li> ");
        	   
        	   $('#template_id').val(data['template_id']);
           }
           else{
        	   $('.js_template_name').replaceWith("<li t-if=\"category\" class=\"js_template_name\"></li>");
        	   $('#template_id').val(0);
           }
		   HoldOn.close()
           
       });

	}
    
    $(oe_website_sale).on('change','.js_length, #length_decimal_select', function () {
        init_rec();
        update_length();
        set_product_description();
        check_geom();
    });
    
    $(oe_website_sale).on('change','.js_width, #width_decimal_select', function () {
        init_rec();
        update_width();
        set_product_description();
        check_geom();
    });
    
   
    
    $(oe_website_sale).on('change',"input[name='add_qty']", function () {
    	set_product_description();
        return false;
    });
    
    $(oe_website_sale).on('change','input.js_basic_characteristics_change:checked, select.js_basic_characteristics_change, select.js_material_type', function (ev) {
    	var $parent = $(this).closest('.js_bloc_search');
	    var basic_characteristics = [];
	    if ($(this).hasClass('js_material_type')){
	    	
	    	$parent.find('select.js_material_type').each(function () {
	    		var type_material= parseInt($(this ).val());
	    		openerp.jsonRpc("/shop/material/get_basic_characteristics", 'call', {'material_type_id':type_material,'category_measurement':$('#category_measurement').val()}).then(function (data) {
	    			$("#area_basic_characteristics_div").replaceWith(data['website_sale_4cotes_cogen.area_basic_characteristics']);
	    			$("#area_basic_characteristics_thickness").replaceWith(data['website_sale_4cotes_cogen.area_basic_characteristics_thickness']);//HR19052016
	    		}).then(function(res){
	    			update_template_values(basic_characteristics,$parent);
	    			$(".js_main_value:first").change();
	    		}) ;
	    	});
	    }
	    else {
	    	if($(this).hasClass("js_main_value")){
	    		if($(this).is("select")) {
	    			main_value_change($("option:selected", this),$parent);
	    		}
	    		else{
	    			main_value_change($(this),$parent);
	    		}
	    	}
	    	update_template_values(basic_characteristics,$parent);

	   }
	    
	  });

    $(oe_website_sale).on('change','.css_attribute_color input', function (ev) {
    	$('.css_attribute_color').removeClass("active");
        $('.css_attribute_color:has(input:checked)').addClass("active");
      //sihem
        $('.color_site_display').parents('li').attr("class", "product-step-box");
    });
   /*Akrem BOUSSAHA*/
    $(oe_website_sale).on('click','input.js_basic_characteristics_change:checked', function (ev) {
        var $parent=$(this).closest('label.css_basic_characteristic_color');
        var source = $parent.find('.img_radio').attr('src');
       
        
        var sourceDeux = $('p img:first-child[class="bc_value_color_model_img"]').attr('src');
        $('p img:first-child[class="bc_value_color_model_img"]').attr('src', source);
        
        $('p img:first-child[class="bc_value_color_model_img_type"]').attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/type_image");

    });
        
        $(oe_website_sale).on('keyup','.js_adresse_street', function (ev) {
            var street = $('.js_adresse_street').val();
            $('.js_street').html(street);
        });
        
        $(oe_website_sale).on('keyup','.js_adresse_ville', function (ev) {
            var ville = $('.js_adresse_ville').val();
            $('.js_ville').val(ville);
        });
        
        $(oe_website_sale).on('keyup','.js_adresse_code_postal', function (ev) {
            var codePostal = $('.js_adresse_code_postal').val();
            $('.js_code_postal').val(codePostal);
        });
        
        $(oe_website_sale).on('keyup','.js_livraison_adresse', function (ev) {
            var streetLivraison = $('.js_livraison_adresse').val();
            $('.js_streetLivraison').html(streetLivraison);
        });
        
        $(oe_website_sale).on('keyup','.js_livraison_ville', function (ev) {
            var villeLivraison = $('.js_livraison_ville').val();
            $('.js_villeLivraison').val(villeLivraison);
        });
        
        $(oe_website_sale).on('keyup','.js_livraison_codePostal', function (ev) {
            var codePostalLivraison = $('.js_livraison_codePostal').val();
            $('.js_code_postalLivraison').val(codePostalLivraison);
        });
   
    
    $(oe_website_sale).on('change','input.js_options_variants_change, select.js_options_variants_change', function (ev) {
    	var $ul = $(this).parents('ul.js_add_cart_variants:first');
    	var $parent = $ul.closest('.js_options_variants');
    	var $product_id = $('.js_product_id');
    	
    	var $price = $parent.find(".oe_price:first .oe_currency_value");
    	var $default_price = $parent.find(".oe_default_price:first .oe_currency_value");
    	var variant_ids = $ul.data("attribute_value_ids");
    	var values = [];
    	
    	$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
        set_product_description();
        
      //End Galerie Image change
        $parent.find('input.js_options_variants_change:checked, select.js_options_variants_change').each(function () {
        	
        	values.push(+$(this).val());
            if (($(this).data("v_attribute_color_texture") !=undefined) && ($(this).data("v_attribute_color_texture")=='color_texture')){
            	var $img = $(".product_color_img");
            	$img.attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
            	var $color_texture_name_desc = $(".color_texture_name_desc");
            	var $color_texture_desc = $(".color_texture_desc");
            	$color_texture_name_desc.html("<span>"+ $(this).attr("title")+ "</span>");
            	if (($(this).data("v_color_texture_desc") != undefined)){
            		$color_texture_desc.html("<span>"+ $(this).data("v_color_texture_desc")+ "</span>");
            	} 
                else{
                	$color_texture_desc.html("<span> </span>");
                }   
            }
            
            if (($(this).data("v_attribute_essence_color") !=undefined) && ($(this).data("v_attribute_essence_color")=='essence_color')){
            	var $img2 = $(".product_essence_color_img");
                $img2.attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
                var $essence_color_name_desc = $(".essence_color_name_desc");
                var $essence_color_desc = $(".essence_color_desc");
                $essence_color_name_desc.html("<span>"+ $(this).attr("title")+ "</span>");
                if (($(this).data("v_essence_color_desc") != undefined)){
                    $essence_color_desc.html("<span>"+ $(this).data("v_essence_color_desc")+ "</span>");
                }    
                else{
                    $essence_color_desc.html("<span> </span>");
                }        
            } 
//          Begin WBO 20160120
            if (($(this).data("v_attribute_color_teinture") !=undefined) && ($(this).data("v_attribute_color_teinture")=='color_teinture')){
                var $img2 = $(".product_color_teinture_img");
                $img2.attr("src", "/website/image/product.attribute.value/" + $(this).val() + "/image");
                var $color_teinture_name_desc = $(".color_teinture_name_desc");
                var $color_teinture_desc = $(".color_teinture_desc");
                $color_teinture_name_desc.html("<span>"+ $(this).attr("title")+ "</span>");
                if (($(this).data("v_color_teinture_desc") != undefined)){
                    $color_teinture_desc.html("<span>"+ $(this).data("v_color_teinture_desc")+ "</span>");
                }    
                else{
                    $color_teinture_desc.html("<span> </span>");
                }        
            } 
//          End   WBO 20160120
            
            
               
        });
        
        var product_id = false;
        for (var k in variant_ids) {
            if (_.isEmpty(_.difference(variant_ids[k][1], values))) {
                $price.html(price_to_str(variant_ids[k][2]));
                $default_price.html(price_to_str(variant_ids[k][3]));
                if (variant_ids[k][3]-variant_ids[k][2]>0.2) {
                    $default_price.closest('.oe_website_sale').addClass("discount");
                } else {
                    $default_price.closest('.oe_website_sale').removeClass("discount");
                }
                product_id = variant_ids[k][0];
                break;
            }
        }
        if (product_id) {
            var $img = $(this).closest('tr.js_product, .oe_website_sale').find('span[data-oe-model^="product."][data-oe-type="image"] img');
            $img.attr("src", "/website/image/product.product/" + product_id + "/image");
            $img.parent().attr('data-oe-model', 'product.product').attr('data-oe-id', product_id)
                .data('oe-model', 'product.product').data('oe-id', product_id);
        }
        $parent.find("input.js_options_variants_change:radio, select.js_options_variants_change").each(function () {
        	var $input = $(this);
            var id = +$input.val();
            var values = [id];

            $parent.find("ul:not(:has(input.js_options_variants_change[value='" + id + "'])) input.js_options_variants_change:checked, select").each(function () {
                values.push(+$(this).val());
            });
            for (var k in variant_ids) {
                if (!_.difference(values, variant_ids[k][1]).length) {
                    return;
                }
            }
        });
        if (product_id) {
            $product_id.val(product_id);;
        } else {
            $product_id.val(0);
        }
		$('.js_current_variant_value_ids').val(values);
   
        init_init();
		return false;
    });

    $(oe_website_sale).on('click','.js_add_to_cart',function (event) {
		var form = $('#submit_form');
		if ($.inArray($('.js_product_category_measurement').val() ,['planches','portes','tablettes','ensembles_monter']) > -1){
			var is_ok_length=check_length();
            var is_ok_width=check_width();
            if ((is_ok_length !== 1) || (is_ok_width !== 1)) { 
                if (is_ok_length == -1)
                    $('#integer_length').show();
                if (is_ok_length == -2)
                    $('#valid_length').show();
                if (is_ok_width == -1) 
                    $('#integer_width').show();
                if (is_ok_width == -2) 
                    $('#valid_width').show();
                return false; 
            }
			else{ 
				console.log("***create_variants****");
				create_variants(form);
			}
		}
		else{
			form.closest('form').submit();
		}
		return false;	
});
    
    function init_init(){
	       $('.js_product, #id_detail_product').find('input.js_variant_change').each(function () {
             $(this).removeAttr('disabled');
         });
         
         $('.js_product, #id_detail_product').find('select.js_variant_change').each(function () {
             $(this).find("option:disabled").removeAttr('disabled').css("color", "grey");
         });   
 	}
    
    
    
	function create_variants(form){
		
		var product_id=parseInt($('input[name="product_id"]').val());
		var template_id=parseInt($('input[name="template_id"]').val() || 0);
		openerp.jsonRpc("/shop/product/get_attributes_lines_values_ids", 'call', {'template_id':template_id , 'length': $(".js_final_length").val(), 'width': $(".js_final_width").val(), 
		'sides': $(".js_final_sides").val()}).then(function (data) {
			values_input=get_current_input_values_id();
			values_select=get_current_select_values_id();
			$('#bloc_options_variants').replaceWith(data['options_variants']);
			set_new_values_id(values_input,values_select);
			$("#select_length").val(data.length_id).trigger('change');
			$("#select_width").val(data.width_id).trigger('change');
			$("#select_sides").val(data.side_id).trigger('change');
		}).then(function(cart){
			
			var formData = $('.js_add_cart_variants').serializeJSON();
			openerp.jsonRpc("/shop/product/add_to_cart", 'call',formData).then(function (res) {
					var $q = $(".my_cart_quantity");
					$('#selling_options').replaceWith(res['selling_options']);
					$('#div_panier').replaceWith(res['icon_panier']);
					console.log("#selling_options replaced");
					$(".js_finition_sides_change:first").change();
					$(".js_portes_tiroirs_change:first").change();
					$q.parent().parent().removeClass("hidden",!res.quantity);
					$q.html(res.cart_quantity).hide().fadeIn(600);
					$('#add_to_cart, .js_add_to_cart').attr('disabled','disabled');
					
					//set_product_description();
				});
			
		});
	}	
	function get_current_input_values_id() {
		var $parent = $('.js_options_variants');
		var values_input = [];
		$parent.find('input.js_options_variants_change:checked').each(function () {
			values_input.push(+$(this).val());
		});
		return values_input;
	}
	function get_current_select_values_id() {
		var $parent = $('.js_options_variants');
		var values_select = [];
		$parent.find('select.js_options_variants_change').each(function () {
			values_select.push(+$(this).val());
		});
		return values_select;
	}
	function set_new_values_id(values_input,values_select) {
		var $parent = $('.js_options_variants');
		var i=0;
		$parent.find('input.js_options_variants_change').each(function () {
			if ($.inArray( parseInt($(this).val()), values_input )!= -1) {
				$(this).prop( "checked", true );
			}
		});
		$parent.find('select.js_options_variants_change').each(function () {
			$(this).val(values_select[i]);
			i=i+1;
		});	
		return true;
	}
	
    
	 $(oe_website_sale).on('dblclick','.js_client_order_ref', function () {
	    	$(this).attr('contentEditable',true);
	 });
	 
	 $(oe_website_sale).on('blur','.js_client_order_ref', function () {
	    	$(this).attr('contentEditable',false);
	    	var client_order_ref=$(this).text();
	    	var order_name=$(this).parents('tr').find('.js_order_name').text();
	    	openerp.jsonRpc("/shop/project_set_ref", 'call',{"order_name":order_name,"client_order_ref":client_order_ref}).then(function (res) {
			});
	    });
	 
	 
	 
	 
	 $(oe_website_sale).on('change','input.js_finition_sides_change, select.js_finition_sides_change', function (ev) {
		 var $parent = $('.js_finition_sides');
		  $parent.find('input.js_finition_sides_change:checked').each(function () {
	        	
	        	$('.js_options_variants').find("input[value='"+$(this).val()+"']").each(function () {
	        		$(this).attr("checked", "checked").change();
	        	});
	        	
	        });
		  $parent.find('select.js_finition_sides_change').each(function () {
	        	
	        	var value=$(this).val()
	        	$('.js_options_variants').find("[name='"+$(this).attr("name")+"']").each(function () {
	        		$(this).val(value).change();
	        	});
	        });
		  
		  set_product_description();
	    });
	    
	    
// 	 <!--Begin WBO 20151215-->

    $(oe_website_sale).on('change','input.js_portes_tiroirs_change, select.js_portes_tiroirs_change', function (ev) {
         var $parent = $('.js_portes_tiroirs');
          $parent.find('input.js_portes_tiroirs_change:checked').each(function () {
                $('.js_options_variants').find("input[value='"+$(this).val()+"']").each(function () {
                    $(this).attr("checked", "checked").change();
                });
                
            });
          $parent.find('select.js_portes_tiroirs_change').each(function () {
                 
                var value=$(this).val()
                $('.js_options_variants').find("[name='"+$(this).attr("name")+"']").each(function () {
                    $(this).val(value).change();
                });
            });
          
          set_product_description();
        });
	 
    });
});

