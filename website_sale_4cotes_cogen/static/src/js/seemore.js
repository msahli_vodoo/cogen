
var showChar = 32;
var ellipsestext = "...";
var moretext ='<i class="fa fa-arrow-circle-down"></i>';
var lesstext = '<i class="fa fa-arrow-circle-up"></i>';
$('.more').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
        var show_content = content.substr(0, showChar);
        var hide_content = content.substr(showChar, content.length - showChar);
            var html = show_content + '<span class="moreelipses">' + ellipsestext;        
        $(this).html(html);
    }
});

$(".morelink").click(function () {
    if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
         
    } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
});

