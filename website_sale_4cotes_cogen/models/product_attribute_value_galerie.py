# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants 
#    (Hafedh RAHMANI) (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import base64
import urllib
import os

from openerp.osv import fields, orm, osv
from openerp.tools.translate import _

import logging
logger= logging.getLogger('__ 4cotes module: __')

class product_attribute_value_galerie(orm.Model):
    "Products attribute value Image gallery"
    _name = "product.attribute.value.galerie"
    _description = """ Products attribute value Image gallery """

    def _check_filestore(self, image_filestore):
        """check if the filestore is created, if not it create it
        automatically
        """
        try:
            dir_path = os.path.dirname(image_filestore)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
        except OSError, e:
            raise osv.except_osv(
                    _('Error'),
                    _('The image filestore can not be created, %s') % e)
        return True

    def _save_file(self, path, b64_file):
        """Save a file encoded in base 64"""
        self._check_filestore(path)
        with open(path, 'w') as ofile:
            ofile.write(base64.b64decode(b64_file))
        return True
    
    def _image_path(self, cr, uid, image, context=None):
        full_path = False
        local_media_repository = self.pool.get('res.company').\
             get_local_media_repository(cr, uid, context=context)
        if local_media_repository:
            full_path = os.path.join(
                local_media_repository,
                image.value_id.name,
                '%s%s' % (image.name or '', image.extension or ''))
        return full_path

    def _set_image(self, cr, uid, id, name, value, arg, context=None):
        image = self.browse(cr, uid, id, context=context)
        full_path = self._image_path(cr, uid, image, context=context)
        if full_path:
            return self._save_file(full_path, value)
        return self.write(cr, uid, id, {'file_db_store': value}, context=context)
    
    
    
    def get_image(self, cr, uid, id, context=None):
        image = self.browse(cr, uid, id, context=context)
        if image.link:
            if image.url:
                (filename, header) = urllib.urlretrieve(image.url)
                with open(filename, 'rb') as f:
                    img = base64.b64encode(f.read())
            else:
                return False
        else:
            try:
                if isinstance(image.product_id.default_code, bool):
                    logger.debug('value not completely setup, no image available')
                    full_path = False
                else:
                    full_path = self._image_path(cr, uid, image, context=context)
            except Exception, e:
                logger.error("Can not find the path for image %s: %s", id, e, exc_info=True)
                return False
            if full_path:
                if os.path.exists(full_path):
                    try:
                        with open(full_path, 'rb') as f:
                            img = base64.b64encode(f.read())
                    except Exception, e:
                        logger.error("Can not open the image %s, error : %s",
                                      full_path, e, exc_info=True)
                        return False
                else:
                    logger.error("The image %s doesn't exist ", full_path)
                    return False
            else:
                img = image.file_db_store
        return img
    
    def _get_image(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for each in ids:
            res[each] = self.get_image(cr, uid, each, context=context)
        return res

    _columns = {
        'name': fields.char('Image Title', required=True),
        'extension': fields.char('file extension', oldname='extention'),
        'link': fields.boolean('Link?',
                               help="Images can be linked from files on "
                                    "your file system or remote (Preferred)"),
        'file_db_store': fields.binary('Image stored in database'),
        'file': fields.function(_get_image,
                                fnct_inv=_set_image,
                                type="binary",
                                string="File",
                                filters='*.png,*.jpg,*.gif'),
        'url': fields.char('File Location'),
        'comments': fields.text('Comments'),
        'value_id': fields.many2one('product.attribute.value', 'Product attribute value')
        }

    _defaults = {
        'link': False,
        }

    _sql_constraints = [('uniq_name_product_attribute_value_id',
                         'UNIQUE(value_id, name)',
                         _('A Product attribute value can have only one '
                           'image with the same name'))]
