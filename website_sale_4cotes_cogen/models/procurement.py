# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import random
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp import SUPERUSER_ID
import logging
logger= logging.getLogger('__ 4cotes module: __')

class procurement_order(osv.osv):
    _inherit = 'procurement.order'
    
    def _get_default_stage_sequence (self, cr, uid,context=None):
        stage_ids = self.pool.get('hvc.mrp.state').search(cr, uid, [('fold', '=', False),('case_default', '=', True)], order='sequence', limit=1, context=context)
        if stage_ids:
            return self.pool.get('hvc.mrp.state').browse(cr, uid, stage_ids[0], context=None).sequence
        return False
    
    def  _get_location_src_id(self,cr, uid,context=None):
        location_src_ids=self.pool.get('stock.location').search(cr, uid, [('usage','=','internal'),('raw_material','=',True)])
        if location_src_ids:
            return location_src_ids[0]
        return False
    
    def _get_color(self,cr, uid, order_id, context=None):
        color=random.randrange(10)
        if order_id:
            same_so_ids=self.pool.get('mrp.production').search(cr, uid, [('order_id','=',order_id)],context=context)
            if same_so_ids:
                color=self.pool.get('mrp.production').browse(cr, uid,same_so_ids[0],context=context).color
        return color
    
    def _get_order_id(self,cr, uid,name,context=None):
        res=self.pool.get('sale.order').search(cr, uid, [('name','=',name)])
        if res:
            return res[0]
        return False
        
    def _get_inventory_lines(self, cr, uid, location_src_id,produit_id, context=None):
        location_obj = self.pool.get('stock.location')
#         location_ids = location_obj.search(cr, uid, [('id', 'child_of', [production.location_src_id.id])], context=context)
        location_ids = location_obj.search(cr, uid, [('id', '=', location_src_id)], context=context)
        domain = ' location_id in %s'
        args = (tuple(location_ids),)
        if produit_id:
            domain += ' and product_id = %s'
            args += (produit_id,)
        cr.execute('''
           SELECT product_id, sum(qty) as product_qty, location_id
           FROM stock_quant WHERE''' + domain + '''
           GROUP BY product_id, location_id
        ''', args)
        vals = []
        for product_line in cr.dictfetchall():
            logger.warning('product_line%s',product_line)
            #replace the None the dictionary by False, because falsy values are tested later on
            for key, value in product_line.items():
                if not value:
                    product_line[key] = False
            vals.append(product_line)
        return vals    
    
    def _get_move_line_propositions(self, cr, uid,produit_id,location_src_id,product_qty,context=None):
        details_ids=[]
        if produit_id :
            product=self.pool.get('product.product').browse(cr, uid,produit_id,context=context)
            args = (product.product_tmpl_id.id,product.length,product.width)
#             TO BE TESTED
#             domain = ' id != %s AND product_tmpl_id =%s AND length >= %s AND width >= %s '  #AND purchase_ok=True : no
            domain = ' product_tmpl_id =%s AND length >= %s AND width >= %s ' 
            order= ' length, width'
            if product.width > product.length:
                order= ' width, length'
            cr.execute(''' SELECT id FROM product_product WHERE ''' + domain + ''' ORDER BY ''' + order , args)
            product_obj=self.pool.get('product.product')
            for p in cr.dictfetchall():
#                 logger.error('p %s',p)
                inventory=self._get_inventory_lines(cr, uid, location_src_id,p['id'], context)
#                 logger.info('inventory %s',inventory)
                virtual_available=product_obj.browse(cr, uid, p['id'], context=context).virtual_available
#                 logger.info('virtual_available %s', virtual_available)
                if len (inventory)>0 and virtual_available>0.0:
                    allowed_qty=0.0
                    if product_qty<virtual_available:
                        allowed_qty=product_qty
                    else:
                        allowed_qty=virtual_available
                    details_ids.append((0, 0,  {'quantity':allowed_qty,
                                                'product_id':p['id'],
                                                'length_input':product_obj.browse(cr, uid, p['id'], context=context).length,
                                                'width_input':product_obj.browse(cr, uid, p['id'], context=context).width,
                                                'qty_available':virtual_available,
                                                'qty_available_display':virtual_available,
                                                'uom_id':product_obj.browse(cr, uid, p['id'], context=context).uom_id,
                                                'uom_id_display':product_obj.browse(cr, uid, p['id'], context=context).uom_id,
                                                'code_product':product_obj.browse(cr, uid, p['id'], context=context).code_product,
                                                'location_src_id':inventory[0]['location_id']
                                                }
                                       ))
                    if context.get('from_procurement',False):
                        break
#         logger.info('details_ids %s',details_ids)
        return details_ids

    def _get_date_planned(self, cr, uid, procurement, context=None):
        format_date_planned = datetime.strptime(procurement.date_planned,
                                                DEFAULT_SERVER_DATETIME_FORMAT)
        date_planned = format_date_planned + relativedelta(days=procurement.product_id.produce_delay or 0.0)
        date_planned = date_planned + relativedelta(days=procurement.company_id.manufacturing_lead)
        logger.warning('date_planned %s',date_planned)
        return date_planned
    def _prepare_mo_vals(self, cr, uid, procurement, context=None):
        res_id = procurement.move_dest_id and procurement.move_dest_id.id or False
        newdate = self._get_date_planned(cr, uid, procurement, context=context)
        bom_obj = self.pool.get('mrp.bom')
        if procurement.bom_id:
            bom_id = procurement.bom_id.id
            routing_id = procurement.bom_id.routing_id.id
        else:
            properties = [x.id for x in procurement.property_ids]
            bom_id = bom_obj._bom_find(cr, uid, product_id=procurement.product_id.id,
                                       properties=properties, context=dict(context, company_id=procurement.company_id.id))
            bom = bom_obj.browse(cr, uid, bom_id, context=context)
            routing_id = bom.routing_id.id
        stage_ids=self.pool.get('product.product').browse(cr, uid, procurement.product_id.id, context=None).material_type_id.stages_ids.ids
        order_id=self._get_order_id(cr, uid,procurement.group_id.name,context=context)
        color=self._get_color(cr, uid, order_id, context=context)
        location_src_id=self._get_location_src_id(cr, uid,context=context)
        return {
            'origin': procurement.origin,
            'product_id': procurement.product_id.id,
            'product_qty': procurement.product_qty,
            'product_uom': procurement.product_uom.id,
            'product_uos_qty': procurement.product_uos and procurement.product_uos_qty or False,
            'product_uos': procurement.product_uos and procurement.product_uos.id or False,
            'location_src_id': location_src_id,
#             'location_src_id': procurement.location_id.id,
            'location_dest_id': procurement.location_id.id,
            'bom_id': bom_id,
            'routing_id': routing_id,
            'date_planned': newdate.strftime('%Y-%m-%d %H:%M:%S'),
            'move_prod_id': res_id,
            'company_id': procurement.company_id.id,
            'stage_domain_ids':[(6,0,stage_ids)],
#             'stage_sequence': self._get_default_stage_sequence(cr, uid, context),
            'sequence':self._get_default_stage_sequence(cr, uid, context),
            'order_id':order_id,
            'color':color
        }

    def _create_move_lines(self,cr,uid,production,move_line_prop,context=None):
        product_obj = self.pool.get('product.product')
        production_obj=self.pool.get('mrp.production')
        if production:
            source_location_id = production.product_id.property_stock_production.id
            destination_location_id = production.location_src_id.id
            for l in move_line_prop:
                detail=list(l)[2]
                product=self.pool.get('product.product').browse(cr, uid,detail['product_id'],context=context)
                prev_move= False
                if production.bom_id.routing_id and production.bom_id.routing_id.location_id and production.bom_id.routing_id.location_id.id != source_location_id:
                    source_location_id = production.bom_id.routing_id.location_id.id
                    prev_move = True
                
#                 allowed_qty=0.0
#                 if production.quantity<detail['quantity']:
#                     allowed_qty=production.quantity
#                 else:
#                     allowed_qty=detail['quantity']
                self.pool.get('stock.move').create(cr, uid, {
                    'name': product.name,
                    'date': production.date_planned,
                    'product_id': detail['product_id'],
                    'product_uom_qty': detail['quantity'],
                    'product_uom': detail['uom_id'].id,
                    'product_uos_qty': product.uos_id and product.uos_qty or False,
                    'product_uos': product.uos_id or False,
                    'location_id': destination_location_id,
                    'location_dest_id': source_location_id,
                    'company_id': production.company_id.id,
                    'procure_method': prev_move and 'make_to_stock' or production_obj._get_raw_material_procure_method(cr, uid, product_obj.browse(cr, uid,detail['product_id'],context=context ), context=context), #Make_to_stock avoids creating procurement
                    'raw_material_production_id': production.id,
                    #this saves us a browse in create()
                    'price_unit': product.standard_price,
                    'origin': production.name,
                    'warehouse_id': self.pool.get('stock.location').get_warehouse(cr, uid, production.location_src_id, context=context),
                    'group_id': production.move_prod_id.group_id.id,
                }, context=context)
                line = {
                    'name': production.name,
                    'product_id': detail['product_id'],
                    'product_qty':detail['quantity'],
                    'product_uom': detail['uom_id'].id,
                    'product_uos_qty': product.uos_id and product.uos_qty or False,
                    'product_uos': product.uos_id or False,
                    'production_id': production.id,
                }
                self.pool.get('mrp.production.product.line').create(cr, uid, line, context=context)
        return True
    
    def make_mo(self, cr, uid, ids, context=None):
        """ Make Manufacturing(production) order from procurement
        @return: New created Production Orders procurement wise
        """
        res = {}
        production_obj = self.pool.get('mrp.production')
        procurement_obj = self.pool.get('procurement.order')
        for procurement in procurement_obj.browse(cr, uid, ids, context=context):
            if self.check_bom_exists(cr, uid, [procurement.id], context=context):
                #create the MO as SUPERUSER because the current user may not have the rights to do it (mto product launched by a sale for example)
                vals = self._prepare_mo_vals(cr, uid, procurement, context=context)
                produce_id = production_obj.create(cr, SUPERUSER_ID, vals, context=dict(context, force_company=procurement.company_id.id))
                res[procurement.id] = produce_id
                self.write(cr, uid, [procurement.id], {'production_id': produce_id})
                self.production_order_create_note(cr, uid, procurement, context=context)
                production_obj.action_compute(cr, uid, [produce_id], properties=[x.id for x in procurement.property_ids])
                production_obj.signal_workflow(cr, uid, [produce_id], 'button_confirm')
                 
                production=production_obj.browse(cr, uid, produce_id, context=context)
                location_src_id=self._get_location_src_id(cr, uid,context=context)
                context.update({'from_procurement': True})
                move_line_prop=self._get_move_line_propositions(cr, uid,production.product_id.id,location_src_id,production.product_qty,context=context)
                self._create_move_lines(cr,uid,production,move_line_prop,context=context)
            else:
                res[procurement.id] = False
                self.message_post(cr, uid, [procurement.id], body=_("No BoM exists for this product!"), context=context)
        return res
    
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
