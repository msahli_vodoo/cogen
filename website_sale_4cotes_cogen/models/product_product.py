# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants 
#    (Hafedh RAHMANI/Wided BEN OTHMEN) (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import random
from openerp import SUPERUSER_ID
from openerp.osv import osv, orm, fields
from openerp.addons.web.http import request
import openerp.addons.decimal_precision as dp

from openerp.tools.translate import _
import psycopg2
from openerp import tools
import re
import logging
from decimal import Context
_logger= logging.getLogger(__name__)
CATEGORY_MEASUREMENT_SELECTION = [('planches', 'Planches'),
                                  ('portes', 'Portes Et Tiroirs'),
                                  ('tablettes', 'Tablettes'),
                                  ('quincaillerie', 'Quincaillerie'),
                                  ('normal','Normal'),
                                  ('edge','Edge'),
                                  ('ensembles_monter',u'Ensembles à Monter')]
def base10to36(num,n):
    """Change a  to a base-n number.
    Up to base-36 is supported without special notation."""
    num_rep={10:'A',
         11:'B',
         12:'C',
         13:'D',
         14:'E',
         15:'F',
         16:'G',
         17:'H',
         18:'I',
         19:'J',
         20:'K',
         21:'L',
         22:'M',
         23:'N',
         24:'O',
         25:'P',
         26:'Q',
         27:'R',
         28:'S',
         29:'T',
         30:'U',
         31:'V',
         32:'W',
         33:'X',
         34:'Y',
         35:'Z'}
    new_num_string=''
    current=num
    while current!=0:
        remainder=current%n
        if 36>remainder>9:
            remainder_string=num_rep[remainder]
        elif remainder>=36:
            remainder_string='('+str(remainder)+')'
        else:
            remainder_string=str(remainder)
        new_num_string=remainder_string+new_num_string
        current=current/n
    return new_num_string

def repeat_to_length(string_to_expand, length):
    return (string_to_expand * ((length/len(string_to_expand))+1))[:length]

class hvc_fabricant_template(osv.osv):
    _name = "hvc.fabricant.template"
    _columns = {
        'name':fields.char('Fabricant'),
    }
    
class hvc_application_utilisation(osv.osv):
    _name = "hvc.application.utilisation"
    _columns = {
        'name':fields.text('Application'),
    }

class hvc_wood_sides_price_by_material_type(osv.Model):
    _name = "hvc.wood.sides.price"
    _rec_name = 'material_type_id'

    _columns = {
        'price_pi': fields.float('Price/ pi', digits_compute=dp.get_precision('Product Price')),
        'material_type_id': fields.many2one('hvc.product.material.type', 'Material type'),
        'side_id': fields.many2one('hvc.wood.sides', 'Side'),
    } 
    
#     _sql_constraints = [
#         ('material_type_id_uniq', 'unique(material_type_id)', 'Price must be unique by material type!'),
#     ]       

class hvc_wood_sides(osv.Model):
    _name = "hvc.wood.sides"

    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
        return result
    
    def _set_image(self, cr, uid, id, name, value, args, context=None):
        return self.write(cr, uid, [id], {'image': tools.image_resize_image_big(value)}, context=context)
    
    def _check_base36_code(self, cr, uid, ids, context=None):
        base36="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for pp in self.browse(cr, uid, ids, context=context):
            if pp.code not in base36:  
                return False
        return True

    _columns = {
        'code': fields.char('Code',size=1, required=True),
        'name': fields.char('Name', size=60, required=True, select=True),
        'description': fields.text('Description',translate=True, help="The Side that client can choose to have after cut for each edge of the board."),
        'image': fields.binary("Image",help="This field holds the image of Sides, limited to 1024x1024px."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={
                'hvc.wood.sides': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized image of the sides. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image", 
            store={
                'hvc.wood.sides': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized image of the product. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved, "\
                 "only when the image exceeds one of those sizes. Use this field in form views or some kanban views."),
        'color': fields.char('HTML Color Index'),
#         'price_ids': fields.many2many('hvc.wood.sides.price', id1='side_id', id2='price_id', string='Prices'),
        'price_ids': fields.one2many('hvc.wood.sides.price','side_id','Prices'),
       
        
    }

    _constraints = [
        (_check_base36_code, 'The code must be in "0123456789ABCDEFGHIJKLMNOPQRSTUV" ', ['code']),
    ]

class hvc_family_attribute_line_one_value(osv.osv):
    _name = "hvc.family.attribute.line.one.value"
    _columns = {
        'family_id': fields.many2one('hvc.product.family', 'Family'),
        'attribute_id': fields.many2one('product.attribute', 'Attribute'),
        'padding': fields.integer('Number of digits', help="Gives the number of digits when displaying a list of product attribute line."),
        'sequence': fields.integer('Digit Position', help="Gives the sequence order when displaying a list of product attribute line."),
        'value_id': fields.many2one('product.attribute.value', 'Value'),
                }
    _defaults = {
        'sequence': 1,
    }
    
    def onchange_sequence(self, cr, uid,ids, sequence,context=None):
        if sequence<=0:
            return {'value':{'sequence':1},'warning':{'title':'warning','message':'Digit position must be greater than or equal 1 !.'}}
        else:
            return{}
            
class hvc_family_attribute_line(osv.osv):
    _name = "hvc.family.attribute.line"
    _columns = {
        'family_id': fields.many2one('hvc.product.family', 'Family',required=True, ondelete='cascade'),
        'attribute_id': fields.many2one('product.attribute', 'Attribute',required=True, ondelete='cascade'),
        'padding': fields.integer('Number of digits', help="Gives the number of digits when displaying a list of product attribute line."),
        'sequence': fields.integer('Digit Position', help="Gives the sequence order when displaying a list of product attribute line."),
        'value_ids': fields.many2many('product.attribute.value', id1='line_id', id2='val_id', string='Product Attribute Value'),
                }
    _defaults = {
        'sequence': 1,
    }
    
    def onchange_sequence(self, cr, uid,ids, sequence,context=None):
        if sequence<=0:
            return {'value':{'sequence':1},'warning':{'title':'warning','message':'Digit position must be greater than or equal 1 !.'}}
        else:
            return{}

class hvc_product_family(osv.osv):
    _name = "hvc.product.family"
            
    def _check_base36_code(self, cr, uid, ids, context=None):
        base36="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for pp in self.browse(cr, uid, ids, context=context):
            if pp.code not in base36:  
                return False
        return True
    
    def _exist_value(self, cr, uid, family_name,attribute_id , context=None):
        if self.pool.get('product.attribute.value').search(cr,uid,[('attribute_id','=',attribute_id),('name','=',family_name)]):
            return True
        else:
            return False
    
    def _check_attribute_lin_ids(self, cr, uid, ids, context=None):
        for family in self.browse(cr, uid, ids, context=context):
            if family.attribute_lin_ids:
                for a in family.attribute_lin_ids:
                    if a.attribute_id.type =='only_one' and a.attribute_id.code=='2' and a.value_id.id==False:
                        if not self._exist_value(cr, uid, family.name,a.attribute_id.id):
                            attribute_value={'name':family.name,
                                             'code':family.name[0],
                                             'attribute_id':a.attribute_id.id
                                             }
                            value_id=self.pool.get('product.attribute.value').create(cr, uid, attribute_value, context=context)
                            a.value_id=value_id
                        return True
                return True
        return True
    
    def _family_details(self, cr, uid, ids, field_names=None, arg=False, context=None):
        context = context or {}
        res = {}
        digit_length=digit_length0=0
        max_position=0
        for family in self.browse(cr, uid, ids, context=context):
            cr.execute("SELECT sum(padding), max(sequence) FROM hvc_family_attribute_line WHERE family_id=%s",(family.id,))
            try: 
                db_result = cr.fetchall()
                if db_result[0][0]!=None:
                    digit_length0=db_result[0][0]
                    max_position=db_result[0][1]
            except:
                raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
            cr.execute("SELECT sum(padding),max(sequence) FROM hvc_family_attribute_line_one_value WHERE family_id=%s",(family.id,))
            try: 
                db_result = cr.fetchall()
                if db_result[0][0]!=None:
                    digit_length=db_result[0][0]+digit_length0
                    if (db_result[0][1]> max_position):
                        max_position=db_result[0][1]
            except:
                raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
            res[family.id] = {
                'digit_length': digit_length,
                'max_position': max_position,
            }
        return res
    _columns = {
        'code': fields.char('Code',size=1,required=True),
        'name': fields.char('Name', required=True),
        'color': fields.char('HTML Color Index'),
        'category_measurement': fields.selection(CATEGORY_MEASUREMENT_SELECTION,'Measurement category', required=True),
        'attribute_ids': fields.one2many('hvc.family.attribute.line','family_id','Attribute'),
        'attribute_lin_ids': fields.one2many('hvc.family.attribute.line.one.value','family_id','Attribute'),
        'uom_id': fields.many2one('product.uom', 'Unit of Measure', help="Default Unit of Measure for family."),
        'uom_po_id': fields.many2one('product.uom', 'Purchase Unit of Measure', help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure."),
        'route_ids': fields.many2many('stock.location.route', 'stock_route_product_family', 'product_id', 'route_id', 'Routes', domain="[('product_selectable', '=', True)]",
                     help="Depending on the modules installed, this will allow you to define the route of the product: whether it will be bought, manufactured, MTO/MTS,..."),
        'categ_id': fields.many2one('product.category','Internal Category', change_default=True, domain="[('type','=','normal')]" ,help="Select category for the current family"),
        'public_categ_ids': fields.many2many('product.public.category', string='Public Category', help="Those categories are used to group similar families for e-commerce."),
        'digit_length': fields.function(_family_details,type='integer',multi='digit_length', string='Digit Length' ),
        'max_position': fields.function(_family_details,type='integer',multi='max_position', string='Max Position' ),
        'basic_price': fields.float('Minimum amount for sale',digits_compute=dp.get_precision('Product Price'),),
    }
    _constraints = [
        (_check_base36_code, 'The code must be in "0123456789ABCDEFGHIJKLMNOPQRSTUV" ', ['code']),
        (_check_attribute_lin_ids, ' !!', ['attribute_lin_ids']),
    ]
    
    
    
    def onchange_uom(self, cr, uid, ids, uom_id, uom_po_id):
        if uom_id:
            return {'value': {'uom_po_id': uom_id}}
        return {}

class product_product(osv.osv):
    _inherit = "product.product"
    _name = "product.product"

    def _get_side_id(self,cr,uid,code):
        ws_obj=self.pool.get('hvc.wood.sides')
        ws_ids= ws_obj.search(cr,uid,[('code','=',code)],limit=1)
        if ws_ids:
            return ws_obj.browse(cr,uid, ws_ids[0]).id
        else:
            return False
        
    def _product_details(self, cr, uid, ids, field_names=None, arg=False, context=None):
        context = context or {}
        attribut_obj=self.pool.get('product.attribute')
        attribut_value_obj=self.pool.get('product.attribute.value')
        res = {}
        length=""
        width=""
        thickness=1
        for product in self.browse(cr, uid, ids, context=context):
            prod=self.browse(cr, uid, product.id, context=context)
            attribute_value_ids=prod.attribute_value_ids.ids
            length_attribut_id=attribut_obj.search(cr, uid, [('type', '=', "input_length")], context=context)
            width_attribut_id=attribut_obj.search(cr, uid, [('type', '=', "input_width")], context=context)
            side_attribut_id=attribut_obj.search(cr, uid, [('type', '=', "sides")], context=context)
            thickness_attribut_id=attribut_obj.search(cr, uid, [('type', '=', "thickness")], context=context)
            
            if length_attribut_id:
                length_value_id=attribut_value_obj.search(cr, uid, [('attribute_id', 'in', length_attribut_id),('id', 'in', attribute_value_ids)], context=context)
                if length_value_id:
                    length=attribut_value_obj.browse(cr, uid, length_value_id, context=context).name
            
            if width_attribut_id:
                width_value_id=attribut_value_obj.search(cr, uid, [('attribute_id', 'in', width_attribut_id),('id', 'in', attribute_value_ids)], context=context)
                if width_value_id:
                    width=attribut_value_obj.browse(cr, uid, width_value_id, context=context).name
            affected_sides_ids=[]
            if side_attribut_id:
                sides_value_ids=attribut_value_obj.search(cr, uid, [('attribute_id', 'in', side_attribut_id),('id', 'in', attribute_value_ids)], context=context)
                if sides_value_ids:
                    sides_codes=attribut_value_obj.browse(cr, uid, sides_value_ids, context=context).name
                    for cd in sides_codes:
                        affected_sides_ids.append(self._get_side_id(cr, uid,cd))
                        
            if thickness_attribut_id:
                thickness_value_id=attribut_value_obj.search(cr, uid, [('attribute_id', 'in', thickness_attribut_id),('id', 'in', attribute_value_ids)], context=context)
                if thickness_value_id:
                    thickness=(attribut_value_obj.browse(cr, uid, thickness_value_id, context=context).name).split("/")
                    if len(thickness)>1:
                        pg_thickness=thickness[0].split(" ")
                        if len(pg_thickness)>1:
                            thickness=float(pg_thickness[0])+float(pg_thickness[1])/float((thickness[1].split("\""))[0])
                        else:
                            thickness=float(thickness[0])/float((thickness[1].split("\""))[0])
                    else:
                        thickness=float((thickness[0].split("\""))[0])

            res[product.id] = {
                'length': length,
                'width': width,
                'thickness':thickness,
                'affected_sides_ids':[(6,0,affected_sides_ids)],
                'volume_pi3': thickness *prod.surface,
                'weight_kg': prod.surface* prod.weight_kg_pi2,
                'weight': prod.surface* prod.weight_kg_pi2*2.2046,
            }
        return res
    
    def _get_uom_id(self, cr, uid,*args):
        return self.pool["product.uom"].search(cr, uid, [], limit=1, order='id')[0]
        
    def _get_code_sides(self, cr, uid,values,context=None):
        attribut_value_obj=self.pool.get('product.attribute.value')
        side_attribut_id=self.pool.get('product.attribute').search(cr, uid, [('type', '=', "sides")], context=context)
        if side_attribut_id:
            sides_value_ids=attribut_value_obj.search(cr, uid, [('attribute_id', 'in', side_attribut_id),('id', 'in', values)], context=context)
            if sides_value_ids:
                return attribut_value_obj.browse(cr, uid, sides_value_ids, context=context).name
        return '0000'
    
    def _product_lst_price(self, cr, uid, ids, name, arg, context=None):
        product_uom_obj = self.pool.get('product.uom')
        res = dict.fromkeys(ids, 0.0)
       
        for product in self.browse(cr, uid, ids, context=context):
            values=[]
            if 'uom' in context:
#                 _logger.warn('__________1111__________')
                uom = product.uos_id or product.uom_id
                res[product.id] = product_uom_obj._compute_price(cr, uid,uom.id, product.list_price, context['uom'])
                _logger.warn('  res[product.id] %s', res[product.id])        
            else:
#                 _logger.warn('__________2222__________')
                res[product.id] = product.list_price
#                 _logger.warn('  res[product.id] %s', res[product.id])        
            if product.category_measurement in ['planches','tablettes','ensembles_monter']:
#                 Begin WBO 20160113
                if product.purchase_ok:
                    res[product.id] = 0.0 #seulement on achete  lst_price=0.0
                else:
#                     _logger.warn('__________3333__________')
                    for v in product.attribute_value_ids:
                        values.append(v.id)
                    final_sides=self._get_code_sides(cr, uid,values) 
                    
                    result=self.get_result(cr, uid, product.product_tmpl_id.id, values, product.surface, 0.0, product.length,product.width, final_sides, context)
    #                 res[product.id] =  (res[product.id] + product.price_extra)*product.surface + product.price_extra_pi*product.edge_length
                    res[product.id] =  result['price']

            elif product.category_measurement =='portes':
#                 if product.purchase_ok:
#                     res[product.id] = 0.0 #seulement on achete  lst_price=0.0 en principe cas non figurant TO BE REV
#                 else:
#                 _logger.warn('__________4444__________')
#                 _logger.warn('product.surface %s',product.surface)
                result=self.get_result(cr, uid, product.product_tmpl_id.id, values, product.surface, 0.0, product.length,product.width, '0000', context)
               
                res[product.id] =  result['price']
#                 End   WBO 20160113
            else:
#                 _logger.warn('__________5555__________')
                res[product.id] = res[product.id] + product.price_extra
        return res
    
    def _product_cost_price_shop(self, cr, uid, ids, name, arg, context=None):
        res = dict.fromkeys(ids, 0.0)
       
        for product in self.browse(cr, uid, ids, context=context):
            values=[]
            if product.category_measurement in ['planches','tablettes','ensembles_monter']:
                for v in product.attribute_value_ids:
                    values.append(v.id)
                final_sides=self._get_code_sides(cr, uid,values) 
                result=self.get_result(cr, uid, product.product_tmpl_id.id, values, product.surface, 0.0, product.length,product.width, final_sides, context)
                res[product.id] =  result['cost']
        return res
            
    def _set_product_lst_price(self, cr, uid, id, name, value, args, context=None):
#             product_uom_obj = self.pool.get('product.uom')
#        
#             product = self.browse(cr, uid, id, context=context)
#             if 'uom' in context:
#                 uom = product.uos_id or product.uom_id
#                 value = product_uom_obj._compute_price(cr, uid,
#                         context['uom'], value, uom.id)
#             if (product.category_measurement in ['planches','portes','tablettes','ensembles_monter']) and product.surface>0.0:
#                     value =  (value/product.surface) - product.price_extra
#             else:
#                 value =  value - product.price_extra    
        return 
    
    def _product_surface(self, cr, uid, ids, field_names=None, arg=False, context=None):
        res={}
        for product in self.browse(cr, uid, ids, context=context):
            surface = 0.0
            if (product.length*product.width)>0.0:
                surface= (product.length*product.width)/144
            res[product.id] = surface
        return res
    
    def _product_edge_length(self, cr, uid, ids, field_names=None, arg=False, context=None): #to delete
        res={}
        for product in self.browse(cr, uid, ids, context=context):
            edge_length = 0.0
            if product.attribute_value_ids:
                sides=[v.name for v in product.attribute_value_ids if v.attribute_id.type=='sides']
                if len(sides):
                    side=sides[0]
                    if int(side[0])==1:
                        edge_length+=product.length
                    if int(side[1])==1:
                        edge_length+=product.length
                    if int(side[2])==1:
                        edge_length+=product.width
                    if int(side[3])==1:
                        edge_length+=product.width
                    
            res[product.id] = edge_length/12
        return res
    
    def _get_price_extra_pi(self, cr, uid, ids, name, args, context=None): #to delete
        result = dict.fromkeys(ids, False)
        for product in self.browse(cr, uid, ids, context=context):
            price_extra_pi= 0.0
            for variant_id in product.attribute_value_ids:
                for price_id in variant_id.price_ids:
                    if price_id.product_tmpl_id.id == product.product_tmpl_id.id:
                        price_extra_pi += price_id.price_extra_pi
            result[product.id] =price_extra_pi
        return result
    
    def _product_price(self, cr, uid, ids, name, arg, context=None):
        plobj = self.pool.get('product.pricelist')
        res = {}
        if context is None:
            context = {}
        quantity = context.get('quantity') or 1.0
        pricelist = context.get('pricelist', False)
        partner = context.get('partner', False)
        if pricelist:
            # Support context pricelists specified as display_name or ID for compatibility
            if isinstance(pricelist, basestring):
                pricelist_ids = plobj.name_search(
                    cr, uid, pricelist, operator='=', context=context, limit=1)
                pricelist = pricelist_ids[0][0] if pricelist_ids else pricelist

            if isinstance(pricelist, (int, long)):
                products = self.browse(cr, uid, ids, context=context)
        
                qtys = map(lambda x: (x, quantity, partner), products)
                pl = plobj.browse(cr, uid, pricelist, context=context)
                price = plobj._price_get_multi(cr,uid, pl, qtys, context=context)
  
                for p in self.browse(cr,uid,ids):
                    res[p.id] = price.get(p.id, 0.0)+(p.price_extra_pi * p.edge_length)+(p.price_extra * p.surface)
        for id in ids:
            res.setdefault(id, 0.0)
        return res
    
#   Begin WBO 20160113    
    
    def _get_cost_edge_X(self,cr, uid,template,values):
        cost_edge_X=0.0
        cr.execute("""SELECT sum(price_extra_pi) 
                        FROM product_attribute_price 
                        WHERE product_tmpl_id=%s and value_id in %s
            """,(template.id,tuple(values)))
        try: 
            db_result = cr.fetchall()
            if db_result[0][0]!=None:
                cost_edge_X=db_result[0][0]
        except:
            raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
        return cost_edge_X

    def _get_edging_cost(self, cr,uid,template, values,final_lenght,final_width,final_sides,context=None):
        side_price_obj=self.pool.get('hvc.wood.sides.price')
        cost_edge_N=0.0
        cost_edge_S=0.0
        cost_edge_W=0.0
        cost_edge_E=0.0
        if final_sides:
            if final_sides[0]!='1': # non Bande de chant
                price_nord=side_price_obj.search(cr, uid,[('side_id', '=', self._get_side_id(cr, uid,final_sides[0])),('material_type_id', '=', template.material_type_id.id)], context=context)
                if price_nord:
                    cost_edge_N=side_price_obj.browse(cr, uid, price_nord[0],context).price_pi
            else:
                cost_edge_N=self._get_cost_edge_X(cr, uid, template, values)
            
            if final_sides[1]!='1': # non Bande de chant
                price_sud=side_price_obj.search(cr, uid,[('side_id', '=', self._get_side_id(cr, uid,final_sides[1])),('material_type_id', '=', template.material_type_id.id)], context=context)
                if price_sud:
                    cost_edge_S=side_price_obj.browse(cr, uid, price_sud[0],context).price_pi
            else:
                cost_edge_S=self._get_cost_edge_X(cr, uid, template, values)
                
            if final_sides[2]!='1': # nonBande de chant
                price_west=side_price_obj.search(cr, uid,[('side_id', '=', self._get_side_id(cr, uid,final_sides[2])),('material_type_id', '=', template.material_type_id.id)], context=context)
                if price_west:
                    cost_edge_W=side_price_obj.browse(cr, uid, price_west[0],context).price_pi
            else:
                cost_edge_W=self._get_cost_edge_X(cr, uid, template, values)
                
            if final_sides[3]!='1': # non Bande de chant
                price_est=side_price_obj.search(cr, uid,[('side_id', '=', self._get_side_id(cr, uid,final_sides[3])),('material_type_id', '=', template.material_type_id.id)], context=context)
                if price_est:
                    cost_edge_E=side_price_obj.browse(cr, uid, price_est[0],context).price_pi
            else:
                cost_edge_E=self._get_cost_edge_X(cr, uid, template, values)
        price_edge_N=cost_edge_N/12*final_lenght 
        price_edge_S=cost_edge_S/12*final_lenght
        price_edge_W=cost_edge_W/12*final_width
        price_edge_E=cost_edge_E/12*final_width
        return price_edge_N,price_edge_S,price_edge_W,price_edge_E
    
    def get_result(self,cr,uid,template_id,values,final_surface,edge_length=0.0,final_lenght=0.0,final_width=0.0,final_sides='0000',context=None):
        template_obj =self.pool.get('product.template')
        product_obj = self.pool.get('product.product')
        _logger.warn('---- final_lenght ---- %s:',final_lenght)
        _logger.warn('---- final_width ---- %s:',final_width)
        price=0.0
        cost_price_shop=0.0
#         extra_price_pi2=0.0
        template = template_obj.browse(cr,SUPERUSER_ID,template_id,context=None)
        if context.get('from_scraps',False):
#             cost_price_shop=template.list_price #!!!!!!!TO BE REVIEWED EMRGENGY
            cost_price_shop=template.standard_price
        else:
            if values:
                _logger.warn('---- values ---- %s:',values)
                extra_price_pi=product_obj._get_sum_extra_price_pi(cr, template_id, values) # == extra_price
                _logger.warn('---- extra_price_pi ---- %s:',extra_price_pi)
            if template.category_measurement in ['quincaillerie','normal']:# edge : purchase_ok=False
                cost_price_shop=template.list_price
            elif template.category_measurement in ['portes']:
                unit_price=product_obj._get_price(cr, uid,template_id,template,cost_price_shop,context=context) #cost_price_shop=0.0
                _logger.warn('---- unit_price ---- %s:',unit_price)
                _logger.warn('---- final_surface ---- %s:',final_surface)
                
                price=unit_price*final_surface
            else:
                cut_price=(final_lenght*2/12 + final_width*2/12)*template.fixed_price_pi
                _logger.warn('---- final_surface ---- %s:',final_surface)
                _logger.warn('---- cut_price ---- %s:',cut_price)
#                 matriel_price= (template.standard_price + extra_price_pi2)*final_surface 
                matriel_price= template.standard_price*final_surface 
                _logger.warn('---- matriel_price ---- %s:',matriel_price)
                price_edge_N,price_edge_S,price_edge_W,price_edge_E=product_obj._get_edging_cost(cr,uid, template, values,final_lenght,final_width,final_sides,context)
                _logger.warn('---- price_edge_N ---- %s:',price_edge_N)
                _logger.warn('---- price_edge_S ---- %s:',price_edge_S)
                _logger.warn('---- price_edge_W ---- %s:',price_edge_W)
                _logger.warn('---- price_edge_E ---- %s:',price_edge_E)
                cost_price_shop= cut_price + matriel_price + price_edge_N + price_edge_S + price_edge_W + price_edge_E
                _logger.warn('---- cost_price_shop ---- %s:',cost_price_shop)
#                 cr.execute("UPDATE product_template set cost_price_shop=%s  WHERE id =%s",(cost_price_shop,template_id))
#                 cr.commit()
                price=product_obj._get_price(cr, uid,template_id,template,cost_price_shop,context=context) 
        result={'price':price, 
                'cost':cost_price_shop
        }
        _logger.warn('---- result ---- %s:',result)
        return result
#   End   WBO 20160113
    
    _columns = {
        'code_product': fields.char('Code'),
        'surface' : fields.function(_product_surface, type='float', string='Surface (pi(2))',digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'edge_length' : fields.function(_product_edge_length, type='float', string='Edge length (pi)',digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'thickness': fields.function(_product_details, multi='val', string='Thickness (inch)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The thickness in  (inch)."),
        'volume_pi3': fields.function(_product_details, multi='val', string='Volume(pi3)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The Volume in  pi(3)."),
        'weight_kg': fields.function(_product_details, multi='val', string='Weight(kg)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The gross weight in  lbs."),
        'weight': fields.function(_product_details, multi='val', string='Weight(lbs)', digits_compute=dp.get_precision('Product Unit of Measure pi2') , help="The net weight in lbs."),
#         'cost_price_shop': fields.float('Cost Price After Cutting (PC)', digits_compute=dp.get_precision('Product Price')),
        'length': fields.function(_product_details,type='float',multi='val', string='Length (inch)',digits_compute=dp.get_precision('Product Unit of Measure pi2'),store=True),
        'width': fields.function(_product_details,type='float', multi='val', string='Width (inch)',digits_compute=dp.get_precision('Product Unit of Measure pi2'),store=True),
        'affected_sides_ids': fields.function(_product_details, type='many2many',relation='hvc.wood.sides', string='Sides', multi='val'),
        'uom_id': fields.many2one('product.uom', 'Unit of Measure', required=True, help="Default Unit of Measure used for all stock operation."),
        'uom_po_id': fields.many2one('product.uom', 'Purchase Unit of Measure', required=True, help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure."),
        'description_sale': fields.text('Sale Description',translate=True,
            help="A description of the Product that you want to communicate to your customers. "
                 "This description will be copied to every Sale Order, Delivery Order and Customer Invoice/Refund"),
        'price_extra_pi': fields.function(_get_price_extra_pi, type='float', string='Variant Extra Price in pi', help="This is the sum of the extra price in pi of all attributes", digits_compute=dp.get_precision('Product Price')),
#         'price_extra_p2': fields.function(_get_price_extra_pi_pi2,multi='val', type='float', string='Variant Extra Price in pi(2)', help="This is the sum of the extra price in pi2 of all attributes", digits_compute=dp.get_precision('Product Price')),
        'lst_price': fields.function(_product_lst_price, fnct_inv=_set_product_lst_price, type='float', string='Public Price (PCE)', digits_compute=dp.get_precision('Product Price')),
        'cost_price_shop': fields.function(_product_cost_price_shop, type='float', string='Cost Price After Cutting (PC)', digits_compute=dp.get_precision('Product Price')),
        
        'price': fields.function(_product_price, type='float', string='Price', digits_compute=dp.get_precision('Product Price')),
        'sale_ok': fields.boolean('Can be Sold', help="Specify if the product can be selected in a sales order line."),
        'purchase_ok': fields.boolean('Can be Purchased', help="Specify if the product can be selected in a purchase order line."),
        'seller_ids': fields.one2many('hvc.product.supplierinfo', 'product_id', 'Supplier'),
        'route_ids': fields.many2many('stock.location.route', 'stock_route_product_rel', 'product_id', 'route_id', 'Routes', domain="[('product_selectable', '=', True)]",
                                    help="Depending on the modules installed, this will allow you to define the route of the product: whether it will be bought, manufactured, MTO/MTS,..."),
    
    }
    def action_view_routes(self, cr, uid, ids, context=None):
        route_obj = self.pool.get("stock.location.route")
        act_obj = self.pool.get('ir.actions.act_window')
        mod_obj = self.pool.get('ir.model.data')
        product_route_ids = set()
        for product in self.browse(cr, uid, ids, context=context):
            product_route_ids |= set([r.id for r in product.route_ids])
            product_route_ids |= set([r.id for r in product.categ_id.total_route_ids])
        route_ids = route_obj.search(cr, uid, ['|', ('id', 'in', list(product_route_ids)), ('warehouse_selectable', '=', True)], context=context)
        result = mod_obj.xmlid_to_res_id(cr, uid, 'stock.action_routes_form', raise_if_not_found=True)
        result = act_obj.read(cr, uid, [result], context=context)[0]
        result['domain'] = "[('id','in',[" + ','.join(map(str, route_ids)) + "])]"
        return result
    
    _defaults = {
        'uom_id': _get_uom_id,
        'uom_po_id': _get_uom_id,
        'purchase_ok': 0,
        'sale_ok': 0,
    }
    
    def name_get(self, cr, user, ids, context=None):
        if context is None:
            context = {}
        if isinstance(ids, (int, long)):
            ids = [ids]
        if not len(ids):
            return []

        def _name_get(d):
            name = d.get('name','')
            code = context.get('display_default_code', True) and d.get('default_code',False) or False
            if code:
                name = '[%s] %s' % (code,name)
            return (d['id'], name)

        partner_id = context.get('partner_id', False)
        if partner_id:
            partner_ids = [partner_id, self.pool['res.partner'].browse(cr, user, partner_id, context=context).commercial_partner_id.id]
        else:
            partner_ids = []

        # all user don't have access to seller and partner
        # check access and use superuser
        self.check_access_rights(cr, user, "read")
        self.check_access_rule(cr, user, ids, "read", context=context)

        result = []
        for product in self.browse(cr, SUPERUSER_ID, ids, context=context):
            variant = ", ".join([v.attribute_id.name+": "+v.name for v in product.attribute_value_ids])
            type_attribute=['input_width','input_length']  
            variant_purchase= ", ".join([v.attribute_id.name+": "+v.name  for v in product.attribute_value_ids if v.attribute_id.type in type_attribute])
            if product.sale_ok:
                name = variant and "%s (%s)" % (product.name, variant) or product.name
            else:
#                 name =  product.name
                name = variant and "%s (%s)" % (product.name, variant_purchase) or product.name
            sellers = []
            
            if partner_ids:
                sellers = filter(lambda x: x.name.id in partner_ids, product.seller_ids)
            if sellers:
                for s in sellers:
                    if product.sale_ok:
                        seller_variant = s.product_name and (variant and "%s (%s)" % (s.product_name, variant) or s.product_name) or False
                    else:
                        seller_variant = s.product_name 
                    mydict = {
                              'id': product.id,
                              'name': seller_variant or name,
                              'default_code': s.product_code or product.default_code,
                              }
                    result.append(_name_get(mydict))
            else:
                mydict = {
                          'id': product.id,
                          'name': name,
                          'default_code': product.default_code,
                          }
                result.append(_name_get(mydict))
        return result
    
    def unlink(self, cr, uid, ids, context=None):
        unlink_ids = []
        for product in self.browse(cr, uid, ids, context=context):
            # Check if product still exists, in case it has been unlinked by unlinking its template
            if not product.exists():
                continue
            unlink_ids.append(product.id)
        cr.execute("DELETE FROM product_product WHERE id in %s",(tuple(unlink_ids),))
        cr.commit()
        return True
    
    def set_sequence_code_attribute_value_id(self, cr, uid, ids, context=None):
        p_attr_line_obj= self.pool.get("product.attribute.line")
        res=[]
        for product in self.browse(cr,uid,ids,context=None):
            for lv in product.product_tmpl_id.attribute_line_one_value_ids:
                res.append((lv.sequence or 0,lv.value_id.code or repeat_to_length('0',lv.padding)))
                
            for value in product.attribute_value_ids:
                lines_ids=p_attr_line_obj.search(cr,uid,[('attribute_id','=',value.attribute_id.id),('product_tmpl_id','=',product.product_tmpl_id.id)],limit=1)
               
                if len(lines_ids):
                    line=p_attr_line_obj.browse(cr,uid,lines_ids[0])
                    res.append((line.sequence or 0,value.code or repeat_to_length('0',line.padding)))
            res.sort(key=lambda tup: tup[0])
            self.write(cr,uid,product.id,{'code_product':"".join([code for code in [c for s,c in res]])})
        return True

    def _get_sum_extra_price_pi(self, cr,template_id, values): 
        price_extra_sum=0.0
        cr.execute("SELECT sum(price_extra_pi) FROM product_attribute_price WHERE product_tmpl_id=%s and value_id in %s",(template_id,tuple(values)))
        try: 
            db_result = cr.fetchall()
            if db_result[0][0]!=None:
                price_extra_sum=db_result[0][0]
        except:
            raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
        return price_extra_sum

    def _get_precision(self, cr, uid):
        return  self.pool.get('decimal.precision').precision_get(cr,uid, 'Account')
        
    def _get_price(self, cr, uid,template_id,products,cost_price_shop,context=None):
        plobj = self.pool.get('product.pricelist')
        user=request.registry.get('res.users').browse(cr,uid,uid,context=None)
        pricelist=user.partner_id.property_product_pricelist
        price=0.0
        if pricelist :
            if isinstance(pricelist.id, (int, long)):
                qtys = map(lambda x: (x, 1, user.partner_id.id), products)
                pl = plobj.browse(cr, uid, pricelist.id, context=context)
                price = plobj._price_get_multi_cogen(cr,uid, pl, qtys,cost_price_shop, context=context)
                price=round(price[template_id],self._get_precision(cr, uid))
        return price  

class product_template(osv.osv):
    _inherit = "product.template"
    _name = "product.template"
    
    def _exist_exception(self, cr, uid, exception_vals, context=None):
        exception_id=0
        exception_obj = self.pool.get("template.measure.exception")
        args=[('product_tmpl_id','=',exception_vals.get('product_tmpl_id',False))]
        template_exceptions=exception_obj.search(cr, uid, args, context=context)
        for exception in template_exceptions:
            values=[v.id for v in exception_obj.browse(cr, uid, exception, context=context).attribute_value_ids]
            if set(values)==set(exception_vals.get('attribute_value_ids',False)[0][2]):
                exception_id=exception
                break
        return exception_id
    
    def get_template_exception(self,cr,uid,template_id,values,context=None):
        exception_vals={'product_tmpl_id': template_id,
                        'attribute_value_ids': [(6, 0, values)]}
        result={'exception':False}
        exception_id=self._exist_exception(cr, uid,exception_vals,context=context)
        if exception_id:
            result['exception']=True
            result['max_length']=self.pool.get('template.measure.exception').browse(cr, uid, exception_id, context=context).max_length, 
            result['max_width']=self.pool.get('template.measure.exception').browse(cr, uid, exception_id, context=context).max_width, 
            return result
        return result
    
    def _get_default_family(self, cr, uid, context=None):
        md = self.pool.get('ir.model.data')
        res = False
        try:
            res = md.get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'product_family_none')[1]
        except ValueError:
            res = False
        return res
    
    def _check_attribute_line(self, cr, uid, ids, context=None):
        attribute_ids=[]
        for template in self.browse(cr, uid, ids, context=context):
            if template.attribute_line_ids:
                for a in template.attribute_line_ids:
                    if a.attribute_id.id in attribute_ids:
                        return False
                    else:
                        attribute_ids.append(a.attribute_id.id)
                return True
        return True
    
    def _check_attribute_line_one_value(self, cr, uid, ids, context=None):
        attribute_ids=[]
        for template in self.browse(cr, uid, ids, context=context):
            if template.attribute_line_one_value_ids:
                for a in template.attribute_line_one_value_ids:
                    if a.attribute_id.id in attribute_ids:
                        return False
                    else:
                        attribute_ids.append(a.attribute_id.id)
                return True
        return True
    
    def _check_min_max_width_length(self, cr, uid, ids, context=None):
        init=0.0
        for template in self.browse(cr, uid, ids, context=context):
            if template.category_measurement in ['planches','portes','tablettes','ensembles_monter']:
                if template.max_length==init or template.min_length==init:
                    return False
                if template.max_width==init or template.min_width==init:
                    return False
                if template.max_length< template.min_length:
                    return False
                if template.max_width< template.min_width:
                    return False
                    
        return True
    _columns = {
        'max_width': fields.float('Maximum width(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum width in inches."),
        'min_width': fields.float('Minimum width(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The minimum width in inches."),
        'max_length': fields.float('Maximum length(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum length in inches."),
        'min_length': fields.float('Minimum length(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The minimum length in inches."),
        'sides_ids': fields.many2many('hvc.wood.sides', 'product_template_sides_rel', id1='template_id', id2='side_id', string='Sides'),
        'family_id': fields.many2one('hvc.product.family', 'Family', ondelete='restrict', required=True),
        'category_measurement': fields.related('family_id', 'category_measurement', type='char', string='Measurement category', 
         store={'product.template': (lambda self, cr, uid, ids, c={}: ids, ['family_id'], 10)},),# required=True
        'material_type_id': fields.many2one('hvc.product.material.type', 'Material type', ondelete='restrict'),
        'attribute_line_one_value_ids': fields.one2many('product.attribute.line.one.value', 'product_tmpl_id', 'Product Attributes have one Value'),
        'cost_price_shop': fields.float('Cost Price After Cutting (PC)', digits_compute=dp.get_precision('Product Price')),
#         'product_extra_costs_ids': fields.one2many('product.extra.costs', 'product_tmpl_id', 'Costs'),
        'application_id': fields.many2one('hvc.application.utilisation','Application'),
        'detail_use':fields.text('Detail'),
        'required_id': fields.many2one('product.product', 'Required'),
        'suggests_ids': fields.many2many('product.product', 'product_template_suggrere_rel','product_id','product_tmpl_id','Suggests'),
        'included_ids': fields.one2many('product.included.line', 'product_tmpl_id', 'Included Product'),
        'modele_id': fields.many2one('product.attribute.value', 'Modele'),
        'manufacturer_id': fields.many2one('hvc.fabricant.template', 'Manufacturer'),
        'sold_paquet':fields.integer('Sold paquet'),
        'price_sold_paquet':fields.float('Price sold in package'),
        'price_costing_paquet':fields.float('Price costing paquet'),
        'costing_high':fields.float('Costing the highest'),
        'code_product': fields.char('Code'),
        'template_exceptions_ids': fields.one2many('template.measure.exception', 'product_tmpl_id', 'Measure Exceptions'),
        'incentives_ids': fields.many2many('product.product', 'product_template_inclus_rel','product_tmpl_id','product_id','Incentives'),
        'template_inclusion_ids': fields.one2many('product.value.domain', 'product_tmpl_id', 'inclusions rules'),
        'digit_length': fields.related('family_id', 'digit_length', type='integer', string='Digit Length', readonly=True),
        'max_position': fields.related('family_id', 'max_position', type='integer', string='Max Position', readonly=True) ,
        'weight_kg_pi2': fields.float('Weight(Kg / pi2)', digits_compute=dp.get_precision('Stock Weight'), help="The weight in  (Kg / pi2)."),
        'color_id': fields.many2one('product.attribute.value', u'Couleur Edge'),
        'epaisseur_id': fields.many2one('product.attribute.value', u'Épaisseur Edge'),
        'fixed_price_pi': fields.related('material_type_id', 'fixed_price_pi', type='float', string='Fixed price cut(pi)'),
        'type_id': fields.many2one('hvc.type', u'Type'),
        'fini_id': fields.many2one('hvc.finition', u'Fini'),
        'material_id': fields.many2one('hvc.materiau', u'matériau'),
#          ,store={'product.template': (lambda self, cr, uid, ids, c={}: ids, ['material_type_id'], 10),
#                 })
    }
    _defaults={
        'family_id':_get_default_family,
        'category_measurement':'normal',
        'min_width':1,
        'min_length':1,
    }
    
    _constraints = [
        (_check_attribute_line, 'Please do not duplicate any attribute line one value !!', ['attribute_line_ids']),
        (_check_attribute_line_one_value, 'Please do not duplicate any attribute line !!', ['attribute_line_one_value_ids']),
        (_check_min_max_width_length, 'Please enter a valid min/max length/width number !!', ['max_width','min_width','max_length','min_length']),
    ]
    
    def onchange_uom(self, cr, uid, ids, uom_id, uom_po_id,family_id):
        err={}
        if family_id:
            uom_family=self.pool.get('hvc.product.family').read(cr, uid, family_id, ['uom_id'])['uom_id']
            if uom_family:
                if uom_family[0]!= uom_id:
                    err['warning']= {'title':_('User Alert!'),'message':_('UOM must be the same of family  !')}
                    err['value']={'uom_id':uom_family[0],'uom_po_id':uom_family[0]}
                    return err
            elif uom_id:    
                return {'value': {'uom_po_id': uom_id,'uom_id': uom_id}}
        return {}

    def onchange_family_id (self, cr, uid, ids,family_id,attribute_line_ids,context=None):
        values={}
        material_type_id=False
        value_id=False
        res={}
        if  family_id:
            family=self.pool.get('hvc.product.family').browse(cr, uid, family_id, context=None)
            if ids:
                template=self.read(cr, uid, ids[0], ['family_id','category_measurement'])
                product_ids=self.pool.get('product.product').search(cr,uid,[('product_tmpl_id','=',ids[0])],context=None)
                if product_ids:
                    res['warning']= {'title':_('User Alert!'),'message':_('you can\'t change family article that has variants !')}
                    res['value']={'family_id':template['family_id'][0]}
                    return res
                if template['category_measurement'] not in ['quincaillerie','normal'] and template['family_id'][0] != family_id: 
                    res['warning']= {'title':_('User Alert!'),'message':_('Changing family article will be irreversible')}    
          
            cr.execute("SELECT value_id FROM hvc_family_attribute_line_one_value WHERE family_id=%s and attribute_id= (select id from product_attribute where name like %s and code = %s)",(family.id,'Material type','1'))
            try: 
                db_result = cr.fetchall()
                if len(db_result)>0  and  db_result[0][0]!=None:
                    value_id=db_result[0][0]
            except:
                raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
            if value_id:
                cr.execute('SELECT id from hvc_product_material_type where name like %s',(self.pool.get('product.attribute.value').browse(cr, uid,value_id, context=context).name,))
                try: 
                    db_result = cr.fetchall()
                    if len(db_result)>0  and  db_result[0][0]!=None:
                        material_type_id=db_result[0][0]
                except:
                    raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
       
                
            values={
                'attribute_line_ids': [(0, 0,  {'attribute_id':line.attribute_id.id,'sequence':line.sequence,'padding':line.padding, 'value_ids': [(6, 0, [v.id for v in line.value_ids])] }) for line in family.attribute_ids],
                'attribute_line_one_value_ids': [(0, 0,  {'attribute_id':line.attribute_id.id,'sequence':line.sequence,'padding':line.padding, 'value_id':line.value_id.id or False}) for line in family.attribute_lin_ids],
                'uom_id':family.uom_id.id or False,
                'uom_po_id': family.uom_po_id.id or False,
                'category_measurement' :family.category_measurement,
                'categ_id':family.categ_id,
                'route_ids':family.route_ids,
                'public_categ_ids':family.public_categ_ids,
                'material_type_id':material_type_id
            }
            res['value']=values
        return res
    
    def _price_get(self, cr, uid, products, ptype='list_price', context=None):
        
        if context is None:
            context = {}
        if 'currency_id' in context:
            pricetype_obj = self.pool.get('product.price.type')
            price_type_id = pricetype_obj.search(cr, uid, [('field','=',ptype)])[0]
            price_type_currency_id = pricetype_obj.browse(cr,uid,price_type_id).currency_id.id

        res = {}
        product_uom_obj = self.pool.get('product.uom')
       
        for product in products:
            # standard_price field can only be seen by users in base.group_user
            # Thus, in order to compute the sale price from the cost price for users not in this group
            # We fetch the standard price as the superuser
            if (ptype != 'standard_price' and  ptype != 'cost_price_shop'):
#                 _logger.warn('11')
                res[product.id] = product[ptype] or 0.0
            else:
#                 _logger.warn('22')
                company_id = product.env.user.company_id.id
                product = product.with_context(force_company=company_id)
                res[product.id] = res[product.id] = product.sudo()[ptype]
                
            if ptype == 'list_price':
#                 _logger.warn('33')
                res[product.id] += product._name == "product.product" and product.price_extra or 0.0
            if 'uom' in context:
#                 _logger.warn('44')
                uom = product.uom_id or product.uos_id
                res[product.id] = product_uom_obj._compute_price(cr, uid,
                        uom.id, res[product.id], context['uom'])
                
                
            # Convert from price_type currency to asked one
            if 'currency_id' in context:
#                 _logger.warn('55')
                # Take the price_type currency from the product field
                # This is right cause a field cannot be in more than one currency
                res[product.id] = self.pool.get('res.currency').compute(cr, uid, price_type_currency_id,
                    context['currency_id'], res[product.id],context=context)

        return res
   

    
    def set_product_details(self, cr, uid, ids, context=None):
        for tmpl_id in self.browse(cr, uid, ids, context=context):
            route_ids=[]
            for route in tmpl_id.route_ids.ids:
                route_ids.append(route)
            
            if tmpl_id.product_variant_ids:
                for product in tmpl_id.product_variant_ids:
                    self.pool.get("product.product").write(cr, uid, product.id, {'code_product': tmpl_id.code_product,
                                                            'lst_price': tmpl_id.list_price,
                                                            'sale_ok': tmpl_id.sale_ok,
                                                            'purchase_ok': tmpl_id.purchase_ok,
                                                            'route_ids':[(6,0,route_ids)]
                                                            }, context=context)
        return True
    
    def create_product_sellers(self, cr, uid, tmpl_sellers_ids,product_id, context=None):
        seller_ids=[]
        for seller in tmpl_sellers_ids:
            vals={'name':seller.name.id,
                  'product_name':seller.product_name,
                  'product_code':seller.product_code,
                  'sequence':seller.sequence,
                  'min_qty':seller.min_qty,
                  'qty':seller.qty,
                  'product_id':product_id,
                  'delay':seller.delay,
                  'company_id':seller.company_id.id,
            }
            hvc_supplierinfo=self.pool.get('hvc.product.supplierinfo').create(cr, uid, vals, context)
            seller_ids.append(hvc_supplierinfo)
            for price in seller.pricelist_ids:
                pr_liste={'name':price.name,
                          'suppinfo_id':hvc_supplierinfo,
                          'min_quantity':price.min_quantity,
                          'price':price.price
                }
                self.pool.get('hvc.pricelist.partnerinfo').create(cr, uid, pr_liste, context)
        return seller_ids
        
    def set_product_sellers(self, cr, uid, ids, context=None):
        for tmpl_id in self.browse(cr, uid, ids, context=context):
            for product in tmpl_id.product_variant_ids:
                seller_ids=self.create_product_sellers(cr, uid, tmpl_id.seller_ids,product.id, context)
                self.pool.get("product.product").write(cr, uid, product.id, {'seller_ids':[(6,0,seller_ids)]}, context=context)
                cr.execute('DELETE FROM hvc_product_supplierinfo WHERE product_id IS NULL')
        return True

    def create_variant_ids(self, cr, uid, ids, context=None):
        for template in self.browse(cr,uid,ids,context=None):
            if template.category_measurement in ['planches','portes','tablettes','ensembles_monter']:
                return True
            else:
                res=super(product_template,self).create_variant_ids(cr,uid,ids,context=None)
                self.set_product_details(cr,uid,ids,context=context)
                self.set_product_sellers(cr,uid,ids,context=context)
                return res
        return True
    
    def write(self, cr, uid, ids, vals, context=None):
        for template in self.browse(cr,uid,ids,context=None):
            res=super(product_template,self).write(cr,uid,ids,vals,context=None)
            if template.category_measurement in  ['quincaillerie','normal','edge']:
                self.set_product_details(cr,uid,ids,context=context)
                self.set_product_sellers(cr,uid,ids,context=context)
            return res
        return True
        
    
    def _default_uom_cp_product(self, cr, uid,category_measurement, context=None):
        if context is None:
            context = {}
        md = self.pool.get('ir.model.data')
        res = False
        try:
            if category_measurement not in ['planches','portes','tablettes','ensembles_monter']:
                res = md.get_object_reference(cr, uid, 'product', 'product_uom_unit')[1]
            else:
                res = md.get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'product_uom_borad')[1]
        except ValueError:
            res = False
        return res
    
    def get_buy_route(self, cr, uid, ids, context=None):
        res=[]
        res.append(self.pool.get('ir.model.data').get_object_reference(cr, uid, 'purchase', 'route_warehouse0_buy')[1])
        res.append(self.pool.get('ir.model.data').get_object_reference(cr, uid, 'stock', 'route_warehouse0_mto')[1])
        route_ids = self.pool.get("stock.location.route").search(cr, uid, ['|', ('id', 'in', res), ('warehouse_selectable', '=', True)], context=context)
        return route_ids
    
    
    def create_product_id(self, cr, uid, ids, all_variants, surface,final_cost,lst_price,purchase_ok,sale_ok,context=None):
        product_obj = self.pool.get("product.product")
        product_id=0
        ctx = context and context.copy() or {}
        if ctx.get("create_product_variant"):
            return None
        ctx.update(active_test=False, create_product_variant=True)
        values={}
        for tmpl_id in self.browse(cr, uid, ids, context=ctx):
            # check product
            variant_ids_to_active = []
            variants_active_ids = []
            
            for product in tmpl_id.product_variant_ids:
                variants = map(int,product.attribute_value_ids)  
                if variants in all_variants: 
                    variants_active_ids.append(product.id)
                    all_variants.pop(all_variants.index(variants))
                    if not product.active:
                        variant_ids_to_active.append(product.id)
                    product_id=product.id
                
            if variant_ids_to_active:
                product_obj.write(cr, uid, variant_ids_to_active, {'active': True}, context=ctx)
                product_id=variant_ids_to_active
            
            # create new product
            if tmpl_id.category_measurement =='portes':
                purchase_ok=True
            if tmpl_id.category_measurement in ['planches','portes','tablettes','ensembles_monter']:
                route_ids=[]
                for variant_ids in all_variants:
                    if purchase_ok:
                        route_ids=self.get_buy_route(cr, uid, ids, context=context)
                    else:  
                        for route in tmpl_id.route_ids.ids:
                            route_ids.append(route)
                    values = {
                        'product_tmpl_id': tmpl_id.id,
                        'attribute_value_ids': [(6, 0, variant_ids)],
                        'cost_price_shop':final_cost,
                        'uom_id': self._default_uom_cp_product(cr,uid,tmpl_id.category_measurement,context=None) or tmpl_id.uom_id.id,
                        'uom_po_id': self._default_uom_cp_product(cr,uid,tmpl_id.category_measurement,context=None) or tmpl_id.uom_id.id,
                        'purchase_ok': purchase_ok,
                        'sale_ok': sale_ok,
                        'route_ids':[(6,0,route_ids)]
                    }
            else:
                for variant_ids in all_variants:
                    values = {
                        'product_tmpl_id': tmpl_id.id,
                        'attribute_value_ids': [(6, 0, variant_ids)],
                    }
            
            if values:
                product_id = product_obj.create(cr, uid, values, context=ctx)
                variants_active_ids.append(product_id)
            if purchase_ok and product_id:
                seller_ids=self.create_product_sellers(cr, uid, tmpl_id.seller_ids,product_id, context)
                product_obj.write(cr, uid, product_id, {'seller_ids':[(6,0,seller_ids)]}, context=context)
                    
        return product_id
    
class product_attribute(osv.Model):
    _inherit = "product.attribute"
    _name = "product.attribute"
    _order = 'sequence'
    
    def onchange_digits(self, cr, uid,ids, digits,context=None):
        if digits<=0:
            return {'value':{'digits':1},'warning':{'title':'warning','message':'digits must be greater than or equal 1 !.'}}
        else:
            return{}
    _columns = {
        'sequence': fields.integer('Sequence', help="Determine the display order"),
        'name': fields.char('Name', translate=False, required=True),
#         Moez Begin 20151210
#         'type': fields.selection([('select', 'Select'), ('color_texture', 'Color Texture'),('color_teinture_sides', 'Color / Teinture des CT'),
#                                   ('input_width', 'Input Width'),('input_length', 'Input Length'), ('hidden', 'Hidden'),('essence_sides', 'Essence sides'),('essence_color', 'Essence color'),
#                                   ('sides', 'Sides'), ('only_one', 'Only one Value'),('thickness', 'Thickness'),('model_mop','Model porte et tiroirs')], string="Type", type="char"),
#         
        'type': fields.selection([('select', 'Select'), ('color_texture', 'Color Texture'),('color_teinture_sides', 'Color / Teinture des CT'),
                                  ('input_width', 'Input Width'),('input_length', 'Input Length'), ('hidden', 'Hidden'),('essence_sides', 'Essence sides'),('essence_color', 'Essence color'),
                                  ('sides', 'Sides'), ('only_one', 'Only one Value'),
                                  ('thickness', 'Thickness'),('model_mop','Modèle Portes Et Tiroirs'),
                                  ('porte_tiroire', 'Portes Et Tiroirs'),('percage', 'Perçage'),
                                  ('color_teinture', 'Couleur / Teinture')], string="Type", type="char"),
        
       #         Moez End 20151210
        'digits': fields.integer('Number of digits', help="Determine the Number of digits to display on the product variant values"),
        'code': fields.char('Code', required=True)
    }
    _defaults = {
        'type': lambda *a: 'select',
        'digits':1,
        'sequence':20
    }
    
    _sql_constraints = [
        ('code_uniq', 'unique(code)', 'Code must be unique!'),
    ]

class product_attribute_value(osv.osv):
    _inherit = "product.attribute.value"
    _name = "product.attribute.value"
    _order = 'sequence desc'
    
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
        return result
    
    def _set_image(self, cr, uid, ids, name, value, args, context=None):
        return self.write(cr, uid, [ids], {'image': tools.image_resize_image_big(value)}, context=context)
    
    def _get_price_extra_pi(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, 0)
        if not context.get('active_id'):
            return result

        for obj in self.browse(cr, uid, ids, context=context):
            for price_id in obj.price_ids:
                if price_id.product_tmpl_id.id == context.get('active_id'):
                    result[obj.id] = price_id.price_extra_pi
                    break
        return result

    def _set_price_extra_pi(self, cr, uid, id, name, value, args, context=None):
        if context is None:
            context = {}
        if 'active_id' not in context:
            return None
        p_obj = self.pool['product.attribute.price']
        p_ids = p_obj.search(cr, uid, [('value_id', '=', id), ('product_tmpl_id', '=', context['active_id'])], context=context)
        if p_ids:
            p_obj.write(cr, uid, p_ids, {'price_extra_pi': value}, context=context)
        else:
            p_obj.create(cr, uid, {
                    'product_tmpl_id': context['active_id'],
                    'value_id': id,
                    'price_extra_pi': value,
                }, context=context)
            
    _columns = {
        'code': fields.char('Code'),
        'description': fields.text('Description'),
        
        'type_image': fields.binary("Image",help="This field holds the image of Sides, limited to 1024x1024px."),
        
        'image': fields.binary("Image",help="This field holds the image of Sides, limited to 1024x1024px."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={
                'hvc.wood.sides': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Small-sized image of the sides. It is automatically "\
                 "resized as a 64x64px image, with aspect ratio preserved. "\
                 "Use this field anywhere a small image is required."),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image", 
            store={
                'hvc.wood.sides': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            },
            help="Medium-sized image of the product. It is automatically "\
                 "resized as a 128x128px image, with aspect ratio preserved, "\
                 "only when the image exceeds one of those sizes. Use this field in form views or some kanban views."),
        'galerie_image_ids': fields.one2many('product.attribute.value.galerie','value_id',string='Galerie Images'),
        'price_extra_pi': fields.function(_get_price_extra_pi, type='float', string='Attribute Price Extra',
            fnct_inv=_set_price_extra_pi,
            digits_compute=dp.get_precision('Product Price'),
            help="Price Extra: Extra price for the variant with this attribute value on sale price. eg. 200 price extra, 1000 + 200 = 1200."),
        'type': fields.related('attribute_id', 'type', type='char', string='Type'),
#         'edge_id': fields.many2one('product.template', 'Edge',domain="[('color_id.code', '=', code)]",required=False),
        'edge_id': fields.many2one('product.template', 'Edge',domain="[('family_id.category_measurement', '=', 'edge')]",required=False),
    
    }
    
    def _check_thickness_structure(self, cr, uid, ids, context=None):
        #regexp='([0-9])+((/[1-9]([0-9])*))?"$'
        regexp='([1-9])* ([0-9])+((/[1-9]([0-9])*))?"$'
        for v in self.browse(cr, uid, ids, context=context):
            if v.attribute_id.type=='thickness' and re.match(regexp, v.name) is None:
                return False
            
        return True
    
    _constraints = [
        (_check_thickness_structure, 'The thickness must be of the following form:([number] [number]/[number]"  ) or ([number]/[number]"  )or ([number]" ) ', ['name','attribute_id']),
    ]

 
    def create(self, cr, uid, data, context=None):
        if 'attribute_id' in data:
            att_obj=self.pool.get('product.attribute')
            line_obj=self.pool.get('product.attribute.line')
            type_attribute=att_obj.browse(cr, uid, data['attribute_id'],context=context).type
            if type_attribute in ["input_length","input_width","sides"]:
                code=line_obj.get_variant_code(cr,uid,data['name'],type_attribute,context=context)
                data['code']=code
        return super(product_attribute_value, self).create(cr, uid, data, context)

class product_attribute_line(osv.osv):
    _inherit = "product.attribute.line"
    _order = 'sequence'
    _columns = {
        'sequence': fields.integer('Digit Position', help="Gives the sequence order when displaying a list of product attribute line."),
        'padding': fields.integer('Number of digits',required=True,help="Gives the number of pad digits when displaying the code of product."),
        'base_line':fields.boolean('Base Attribute',help="The base attribute to define Attribut's extra price")
    }

    def get_variant_code(self,cr,uid,value_name,type_attribute,context=None):
        cd=[]
        if type_attribute=="sides":
            return value_name
        if type_attribute=="input_length" or type_attribute=="input_width":
            lst=value_name.split(".")
            cint=base10to36(int(lst[0]),36)
            if len(cint)==1:
                cd.append("0"+cint)
            else:
                cd.append(cint)
            if len(lst)>1:
                cd.append(base10to36(int(float(("0."+lst[1]))*32),36))
            else:
                cd.append("0")
            
            return "".join(cd)
        return True

    def get_attribute_line_type(self,cr,uid,template_id,type_attribute,context=None):
        lines=self.search(cr,SUPERUSER_ID,[('product_tmpl_id','=',template_id)],context=None)
        for line in self.browse(cr,SUPERUSER_ID,lines,context=None):
            if line.attribute_id.type==type_attribute:
                return [line.id]
        return []

    def _set_attribute_line_values_id(self,cr,uid,value_name,line,context=None):
        product_attribute_value_obj=self.pool.get('product.attribute.value')
        exist_values=product_attribute_value_obj.search(cr,uid,[('name','=',value_name),('attribute_id','=',line.attribute_id.id)],context=None)
        if exist_values:
            self.write(cr,uid,line.id,{'value_ids':[(4, exist_values[0])]},context=None)
            return exist_values[0]
        else:
            if line.attribute_id.type in ['sides',"input_length","input_width"] :
                code=self.get_variant_code(cr,uid,value_name,line.attribute_id.type,context=None)
                value_id=product_attribute_value_obj.create(cr,uid,{'name':value_name,'code':code,'attribute_id':line.attribute_id.id},context=None)
                self.write(cr,uid,line.id,{'value_ids':[(4, value_id)]},context=None)
                return value_id
        return []
        
    def get_attribute_line_values_id(self,cr,uid,line_id,value_name,context=None):
        values={}
        line=self.browse(cr,SUPERUSER_ID,line_id,context=None)
        if line.value_ids:
            values=[(value.id,value.name) for value in line.value_ids]
            result=[item[0] for item in values if item[1] == value_name]
            if result:
                return result[0]
            else:
                return self._set_attribute_line_values_id(cr,SUPERUSER_ID,value_name,line,context=None)
        else:
            return self._set_attribute_line_values_id(cr,SUPERUSER_ID,value_name,line,context=None)
        
    def set_base_attribute(self,cr,uid,line_id,context=None):
        if self.browse(cr, uid, line_id, context=context).base_line==False:
            self.write(cr, uid, line_id, {'base_line':True}, context=context)
        else:
            self.write(cr, uid, line_id, {'base_line':False}, context=context)
        return True
        
class hvc_product_material_type(osv.osv):
    _name = "hvc.product.material.type"
    _order = 'sequence'

    def _check_base36_code(self, cr, uid, ids, context=None):
        base36="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        for pp in self.browse(cr, uid, ids, context=context):
            if pp.code not in base36:  
                return False
        return True
    def _get_default_stage_id(self, cr, uid, context=None):
        stage_ids = self.pool.get('hvc.mrp.state').search(cr, uid, [('case_default', '=', True)], order='sequence', context=context)
        if stage_ids:
            return  [(6,0,stage_ids)]
        return False
    _columns = {
        'code': fields.char('Code',size=1,required=True),
        'name': fields.char('Name', required=True),
        'planches_attribute_base_ids': fields.many2many('product.attribute','planches_materiau_attribute_rel', id1='type_material_id', id2='attribute_id', string='Basic characteristics for the boards'),
        'portes_attribute_base_ids': fields.many2many('product.attribute', 'portes_materiau_attribute_rel', id1='type_material_id', id2='attribute_id', string='Basic characteristics for doors and drawers'),
        'tablettes_attribute_base_ids': fields.many2many('product.attribute', 'tablettes_materiau_attribute_rel', id1='type_material_id', id2='attribute_id', string='Basic characteristics for the shelves'),
        'ensembles_monter_attribute_base_ids': fields.many2many('product.attribute', 'ensembles_monter_materiau_attribute_rel', id1='type_material_id', id2='attribute_id', string='Basic characteristics for the kits'),
        'active': fields.boolean('Active', help="By unchecking the active field you can disable a material type without deleting it."),
        'sequence': fields.integer('Sequence', help="Determine the display order"),
        'sides_ids': fields.many2many('hvc.wood.sides', 'materiau_sides_rel', id1='template_id', id2='side_id', string='Sides'),
        'fixed_price_pi': fields.float('Fixed price cut(pi)', digits_compute=dp.get_precision('Product Price')),
        'stages_ids': fields.many2many('hvc.mrp.state', 'material_stages_rel', id1='material_id', id2='stage_id', string='Stages'),
        
    }
    
    _defaults = {
        'active': 1,
        'sequence':20,
        'stages_ids': lambda s, cr, uid, c: s._get_default_stage_id(cr, uid, c),
        'fixed_price_pi':1
    }
    _constraints = [
        (_check_base36_code, 'The code must be in "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ" ', ['code']),
    ]
  
class product_attribute_line_one_value(osv.osv):
    _name = "product.attribute.line.one.value"
    _rec_name = 'attribute_id'
    _order = 'sequence'
        
        
    def _get_image(self, cr, uid, ids, name, args, context=None):
        result = dict.fromkeys(ids, False)
        for obj in self.browse(cr, uid, ids, context=context):
            result[obj.id] = tools.image_get_resized_images(obj.image, avoid_resize_medium=True)
        return result
    
    def _set_image(self, cr, uid, ids, name, value, args, context=None):
        return self.write(cr, uid, [ids], {'image': tools.image_resize_image_big(value)}, context=context)
    
    _columns = {
        'sequence': fields.integer('Digit position', help="Gives the sequence order when displaying a list of product attribute line."),
        'padding': fields.integer('Number of digits',required=True, help="Gives the number of pad digits when displaying the code of product."),
        'product_tmpl_id': fields.many2one('product.template', 'Product Template', required=True, ondelete='cascade'),
        'attribute_id': fields.many2one('product.attribute', 'Attribute', required=True, ondelete='restrict'),
        'value_id': fields.many2one('product.attribute.value', 'Value', required=True, ondelete='restrict'),
        'image': fields.binary("Image",help="This field holds the image of Sides, limited to 1024x1024px."),
        'image_small': fields.function(_get_image, fnct_inv=_set_image,
            string="Small-sized image", type="binary", multi="_get_image",
            store={
                'product.attribute.line.one.value': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            }),
        'image_medium': fields.function(_get_image, fnct_inv=_set_image,
            string="Medium-sized image", type="binary", multi="_get_image", 
            store={
                'product.attribute.line.one.value': (lambda self, cr, uid, ids, c={}: ids, ['image'], 10),
            }),
    }
    
class product_attribute_price(osv.osv):
    _inherit = "product.attribute.price"
    _columns = {
        'cost_extra': fields.float('Extra Cost / pi(2)', digits_compute=dp.get_precision('Product Price')),#not used
        'price_extra_pi': fields.float('Extra Price / pi', digits_compute=dp.get_precision('Product Price')),
        'edge_id': fields.many2one('product.template', 'Edge',required=False),
#         'price_extra_pi2': fields.float('Price Extra / pi(2)', digits_compute=dp.get_precision('Product Price')),
    }
    
class product_included_line(osv.osv):
    _name = "product.included.line"
    _rec_name='product_id'
    _columns = {
                'product_tmpl_id':fields.many2one('product.template', u'Modèle'),
                'product_id':fields.many2one('product.template', 'Produit'),
                'qty': fields.float('Qty', digits_compute=dp.get_precision('Product Price')),
    }
    def name_get(self,cr,uid,ids,context=None):
        result = {}
        for record in self.browse(cr,uid,ids,context=context):
            result[record.id] = str(record.qty) + " X "+ record.product_id.name
        return result.items()
    
class template_measure_exception(osv.osv):
    _name = "template.measure.exception"
    _rec_name='priority'
    _order = 'priority'
    _columns = {
        'priority': fields.integer('Priority',required=True,help="Gives the exception priority."),
        'attribute_value_ids': fields.many2many('product.attribute.value', id1='temp_exception_id', id2='value_id',required=True, string='Values'),
        'max_width': fields.float('Max width(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum width in inches."),
        'min_width': fields.float('Min width(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The minimum width in inches."),
        'max_length': fields.float('Max length(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum length in inches."),
        'min_length': fields.float('Min length(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The minimum length in inches."),
        'product_tmpl_id':fields.many2one('product.template', 'Template'),
    }
       
class product_value_domain(osv.osv):
    _name = "product.value.domain"
    _rec_name='product_tmpl_id'
    
    def _check_values(self, cr, uid, ids, context=None):
        for value_domain in self.browse(cr, uid, ids, context=context):
            value_obj=self.pool.get('product.attribute.value')
            values_ids_attributes=[value_obj.browse(cr, uid, v, context=context).attribute_id.id for v in value_domain.values_ids.ids ]
            if len(list(set(values_ids_attributes))) < len(values_ids_attributes):
                return False
            exclusions_ids_attributes=[value_obj.browse(cr, uid, v, context=context).attribute_id.id for v in value_domain.exclusions_ids.ids ]
            if list(set(value_domain.values_ids.ids) & set(value_domain.exclusions_ids.ids)):
                return False
            if list(set(values_ids_attributes) & set(exclusions_ids_attributes)):
                return False
            return True
        return True
    
    def _check_exist(self, cr, uid, ids, context=None):
        inclusion_obj = self.pool.get("product.value.domain")
        for value_domain in self.browse(cr, uid, ids, context=context):
            args=[('product_tmpl_id','=',value_domain.product_tmpl_id.id),('id','not in',ids)]
            template_inclusion=inclusion_obj.search(cr, uid, args, context=context)
            for inclusion in template_inclusion:
                values=[v.id for v in inclusion_obj.browse(cr, uid, inclusion, context=context).values_ids]
                if set(values)==set(value_domain.values_ids.ids):
                    return False
        return True


     
    _columns = {
        'product_tmpl_id': fields.many2one('product.template', 'Product Template', required=True, ondelete='cascade'),
        'values_ids': fields.many2many('product.attribute.value', 'product_value_domain_values_rel',d1='value_domain_id', id2='value_id', string='Values Domains', required=True),
        'exclusions_ids': fields.many2many('product.attribute.value', 'product_value_domain_exclusions_rel', d1='value_domain_id', id2='exclusion_id', string='Combinations' , required=True)
    }  
    _constraints = [
        (_check_values, 'Please do not set domain as an inclusion or duplicate attribute value on domain!!', ['values_ids','exclusions_ids']),
        (_check_exist, 'Please do not duplicate any  inclusion!!', ['product_tmpl_id','values_ids','exclusions_ids']),
    ]
         
    def _get_value_domain(self,cr, uid, product_tmpl_id,context=None):
        values_ids_domain=[]
        for line in self.pool.get('product.template').browse(cr, uid,product_tmpl_id,context=context).attribute_line_ids.ids:
            attribute_ids=self.pool.get('product.attribute').search(cr, uid, [('type','not in',['input_length','input_width','sides'])])
            value_ids=self.pool.get('product.attribute.line').browse(cr, uid,line,context=context).value_ids.ids
            for value in self.pool.get('product.attribute.value').search(cr, uid,[('attribute_id','in',attribute_ids),('id','in',value_ids)], context=context):
                values_ids_domain.append(value)
        return {'values_ids': [('id', 'in', values_ids_domain)],'exclusions_ids': [('id', 'in', values_ids_domain)]}
     
    def onchange_product_tmpl_id(self, cr, uid, ids, product_tmpl_id,context=None):
        if product_tmpl_id:
            return {'domain':self._get_value_domain(cr, uid, product_tmpl_id,context=context)}
        return {}
    
class product_public_category(osv.osv):
    _name = "product.public.category"
    _inherit = "product.public.category"
    _description = "Public Category"

    _columns = {
        'family_id': fields.many2one('hvc.product.family', 'Product family'),
        'active': fields.boolean('Active', help="By unchecking the active field you can disable a Public Category without deleting it."),
    }
    _defaults = {
        'active': True
                 
    }

class hvc_product_supplierinfo(osv.osv):
    _name = "hvc.product.supplierinfo"
    _description = "Information about a product supplier"
    def _calc_qty(self, cr, uid, ids, fields, arg, context=None):
        result = {}
        for supplier_info in self.browse(cr, uid, ids, context=context):
            for field in fields:
                result[supplier_info.id] = {field:False}
            qty = supplier_info.min_qty
            result[supplier_info.id]['qty'] = qty
        return result
 
    _columns = {
        'name' : fields.many2one('res.partner', 'Supplier', required=True,domain = [('supplier','=',True)], ondelete='cascade', help="Supplier of this product"),
        'product_name': fields.char('Supplier Product Name', help="This supplier's product name will be used when printing a request for quotation. Keep empty to use the internal one."),
        'product_code': fields.char('Supplier Product Code', help="This supplier's product code will be used when printing a request for quotation. Keep empty to use the internal one."),
        'sequence' : fields.integer('Sequence', help="Assigns the priority to the list of product supplier."),
        'product_uom': fields.related('product_id', 'uom_po_id', type='many2one', relation='product.uom', string="Supplier Unit of Measure", readonly="1", help="This comes from the product form."),
        'min_qty': fields.float('Minimal Quantity', required=True, help="The minimal quantity to purchase to this supplier, expressed in the supplier Product Unit of Measure if not empty, in the default unit of measure of the product otherwise."),
        'qty': fields.function(_calc_qty, store=True, type='float', string='Quantity', multi="qty", help="This is a quantity which is converted into Default Unit of Measure."),
        'product_id' : fields.many2one('product.product', 'Product Template', required=False, ondelete='cascade', select=True, oldname='product_id'),
        'delay' : fields.integer('Delivery Lead Time', required=True, help="Lead time in days between the confirmation of the purchase order and the receipt of the products in your warehouse. Used by the scheduler for automatic computation of the purchase order planning."),
        'pricelist_ids': fields.one2many('hvc.pricelist.partnerinfo', 'suppinfo_id', 'Supplier Pricelist', copy=True),
        'company_id':fields.many2one('res.company','Company',select=1),
    }
    _defaults = {
        'min_qty': 0.0,
        'sequence': 1,
        'delay': 1,
        'company_id': lambda self,cr,uid,c: self.pool.get('res.company')._company_default_get(cr, uid, 'hvc.product.supplierinfo', context=c),
    }
 
    _order = 'sequence'

class hvc_pricelist_partnerinfo(osv.osv):
    _name = 'hvc.pricelist.partnerinfo'
    _columns = {
        'name': fields.char('Description'),
        'suppinfo_id': fields.many2one('hvc.product.supplierinfo', 'Partner Information', required=True, ondelete='cascade'),
        'min_quantity': fields.float('Quantity', required=True, help="The minimal quantity to trigger this rule, expressed in the supplier Unit of Measure if any or in the default Unit of Measure of the product otherrwise."),
        'price': fields.float('Unit Price', required=True, digits_compute=dp.get_precision('Product Price'), help="This price will be considered as a price for the supplier Unit of Measure if any or the default Unit of Measure of the product otherwise"),
    }
    _order = 'min_quantity asc'
    
    
#sihem 15-06-2016    
    
class hvc_type(osv.osv):  
    _name="hvc.type"  
    _columns={
              'name':fields.char('Type')
              }
class hvc_finition(osv.osv):  
    _name="hvc.finition"  
    _columns={
              'name':fields.char('Fini')
              }    
class hvc_materiau(osv.osv):  
    _name="hvc.materiau"  
    _columns={
              'name':fields.char('materiau')
              }
#sihem 15-06-2016        