# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants 
#    (Hafedh RAHMANI/Wided BEN OTHMEN)  (http://hautevoltige.net/).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
logger= logging.getLogger('__ 4cotes module: __')

import random
import sys
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from datetime import datetime, timedelta
import time
from openerp import SUPERUSER_ID
from openerp.osv import osv, orm, fields
from openerp.addons.web.http import request
from openerp.tools.translate import _
class sale_order(orm.Model):
    _inherit = "sale.order"
    
    def _get_date_planned(self, cr, uid, order, line, start_date, context=None):
        date_planned = datetime.strptime(start_date, DEFAULT_SERVER_DATETIME_FORMAT) + timedelta(days=line.delay or 0.0)
        logger.warning('date_planned %s',date_planned)
        return date_planned


    def _get_side_name(self,cr,uid,code):
        ws_obj=self.pool.get('hvc.wood.sides')
        ws_ids= ws_obj.search(cr,uid,[('code','=',code)],limit=1)
        if ws_ids:
            return ws_obj.browse(cr,uid, ws_ids[0]).name
        else:
            return ""
 
    def _get_wood_sides_description_sale(self, cr, uid,final_sides,context=None):
        pp_description=""
        for i in range(0, len(final_sides)):
            if final_sides[i]!='0':
                if i==0:
                    pp_description += " [N-" +self._get_side_name(cr, uid, final_sides[i])+"]"
                elif i==1:
                    pp_description += " [S-" +self._get_side_name(cr, uid, final_sides[i])+"]"
                elif i==2:
                    pp_description += " [O-" +self._get_side_name(cr, uid, final_sides[i])+"]"
                elif i==3:
                    pp_description += " [E-" +self._get_side_name(cr, uid, final_sides[i])+"]"
                 
        return pp_description

    def _create_product(self,cr,uid,template_id,current_variant_value_ids,surface,final_cost,lst_price,purchase_ok,sale_ok):
        list_values=current_variant_value_ids.split(',')
        values=[int(value) for value in list_values]
        all_variants=[]
        all_variants.append(values)
        template_obj = self.pool.get('product.template')
        product_obj = self.pool.get('product.product')
        product_id= template_obj.create_product_id(cr, uid,template_id,all_variants,surface,final_cost,lst_price,purchase_ok,sale_ok)
        if template_obj.browse(cr, uid,template_id,context=None).category_measurement not in ['normal','quincaillerie']:
            product_obj.set_sequence_code_attribute_value_id(cr, uid, [product_id], context=None)
        return product_id
    

        
    def _cart_update(self, cr, uid, ids,
                         final_sides='',length=0, width=0, final_price=0,final_surface=0,lst_price=0,
                         template_id=0,current_variant_value_ids=None,
                         product_id=None, line_id=None, add_qty=0, set_qty=0,purchase_ok=False, sale_ok=False, context=None,**kwargs):

        """ Add or set product quantity, add_qty can be negative """
        sol = self.pool.get('sale.order.line')
        if product_id==0:
            product_id=self._create_product(cr,uid,int(template_id),current_variant_value_ids, final_surface,final_price,lst_price,purchase_ok,sale_ok)

        quantity = 0
        for so in self.browse(cr, uid, ids, context=context):
            if line_id != False:
                line_ids = so._cart_find_product_line(product_id, line_id, context=context, **kwargs)
                if line_ids:
                    line_id = line_ids[0]
                        
            # Create line if no line with product_id can be located
            if not line_id:
                values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=1, context=context)
                if lst_price>0:
                    values["price_unit"]=lst_price
                line_id = sol.create(cr, SUPERUSER_ID, values, context=context)
                if add_qty:
                    add_qty -= 1

            # compute new quantity
            if set_qty:
                quantity = set_qty
            elif add_qty != None:
                quantity = sol.browse(cr, SUPERUSER_ID, line_id, context=context).product_uom_qty + (add_qty or 0)

            # Remove zero of negative lines
            if quantity <= 0:
                sol.unlink(cr, SUPERUSER_ID, [line_id], context=context)
            else:
                # update line
                values = self._website_product_id_change(cr, uid, ids, so.id, product_id, qty=quantity, line_id=line_id, context=context)
                values['product_uom_qty'] = quantity
                if lst_price>0:
                    values['price_unit'] = lst_price

                values['name'] = self.pool.get('product.product').name_get(cr, uid, [product_id], context=context)[0][1]
                sol.write(cr, SUPERUSER_ID, [line_id], values, context=context)

        return {'line_id': line_id, 'quantity': quantity}

class website(orm.Model):
    _inherit = 'website'
    def sale_get_order(self, cr, uid, ids, force_create=False, code=None, update_pricelist=None, context=None):
        sale_order_obj = self.pool['sale.order']
        sale_order_id = request.session.get('sale_order_id')
        sale_order = None
        # create so if needed
        if not sale_order_id and (force_create or code):  
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id
            
            for w in self.browse(cr, uid, ids):
                values = {
                    'user_id': w.user_id.id,
                    'partner_id': partner.id,
                    'pricelist_id': partner.property_product_pricelist.id,
                    'section_id': self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website', 'salesteam_website_sales')[1],
                }
                sale_order_id = sale_order_obj.create(cr, SUPERUSER_ID, values, context=context)
                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [], partner.id, context=context)['value']
                sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)
                request.session['sale_order_id'] = sale_order_id
        if sale_order_id:
            # TODO cache partner_id session
            partner = self.pool['res.users'].browse(cr, SUPERUSER_ID, uid, context=context).partner_id

            sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order_id, context=context)
            if not sale_order.exists():
                request.session['sale_order_id'] = None
                return None

            # check for change of pricelist with a coupon
            if code and code != sale_order.pricelist_id.code:
                pricelist_ids = self.pool['product.pricelist'].search(cr, SUPERUSER_ID, [('code', '=', code)], context=context)
                if pricelist_ids:
                    pricelist_id = pricelist_ids[0]
                    request.session['sale_order_code_pricelist_id'] = pricelist_id
                    update_pricelist = True

            pricelist_id = request.session.get('sale_order_code_pricelist_id') or partner.property_product_pricelist.id

            # check for change of partner_id ie after signup
            if sale_order.partner_id.id != partner.id and request.website.partner_id.id != partner.id:
                flag_pricelist = False
                if pricelist_id != sale_order.pricelist_id.id:
                    flag_pricelist = True
                fiscal_position = sale_order.fiscal_position and sale_order.fiscal_position.id or False

                values = sale_order_obj.onchange_partner_id(cr, SUPERUSER_ID, [sale_order_id], partner.id, context=context)['value']
                if values.get('fiscal_position'):
                    order_lines = map(int,sale_order.order_line)
                    values.update(sale_order_obj.onchange_fiscal_position(cr, SUPERUSER_ID, [],
                        values['fiscal_position'], [[6, 0, order_lines]], context=context)['value'])

                values['partner_id'] = partner.id
                if partner.id != False:
                    sale_order_obj.write(cr, SUPERUSER_ID, [sale_order_id], values, context=context)

                if flag_pricelist or values.get('fiscal_position') != fiscal_position:
                    update_pricelist = True

            # update the pricelist
            if partner.id != False:
                if update_pricelist:
                    values = {'pricelist_id': pricelist_id}
                    values.update(sale_order.onchange_pricelist_id(pricelist_id, None)['value'])
                    sale_order.write(values)
                    for line in sale_order.order_line:
                        sale_order._cart_update(product_id=line.product_id.id, line_id=line.id, add_qty=0)
    
                # update browse record
                if (code and code != sale_order.pricelist_id.code) or sale_order.partner_id.id !=  partner.id:
                    sale_order = sale_order_obj.browse(cr, SUPERUSER_ID, sale_order.id, context=context)

        return sale_order

