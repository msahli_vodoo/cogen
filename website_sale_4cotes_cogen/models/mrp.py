# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-2015 Haute Voltige consultants (Wided BEN OTHMEN) 
#                        (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from lxml import etree
import random
from openerp import models, fields, api, _
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.osv import fields, osv
import logging
logger= logging.getLogger('__ 4cotes module: __')
from openerp.tools.float_utils import float_compare, float_round
class mrp_activity_line(osv.osv):
    _name = 'mrp.activity.line'
    _columns = {
        'name': fields.char('Name', required=True),
        'activity_id': fields.many2one('mrp.activity', 'Activity'),
    }

class mrp_activity(osv.osv):
    _name = 'mrp.activity'
    _columns = {
        'name': fields.char('Name', required=True),
        'activity_lines': fields.one2many('mrp.activity.line', 'activity_id', 'Activity lines'),
    }

class mrp_bom(osv.osv):
    _inherit = 'mrp.bom'
    _columns = {
        'state': fields.selection([('activated', 'Activated'),('deactivated','Deactivated')],'State'),
    }
    
    def set_bom_active(self,cr,uid,bom_id,context=None): 
        product_tmpl_id=self.browse(cr, uid, bom_id, context=context).product_tmpl_id.id
        
        if product_tmpl_id:
            active_bom=self.search(cr, uid, [('product_tmpl_id','=',product_tmpl_id),('state','=','activated')], context=context)
            if len(active_bom)>0:
                raise osv.except_osv(_('Error!'), _('There is already an active bom for this template!'))
            else:
                self.write(cr, uid, bom_id, {'state':'activated'}, context=context)
        return True
        
    def set_bom_desactive(self,cr,uid,bom_id,context=None): 
        return self.write(cr, uid, bom_id, {'state':'deactivated'}, context=context)
    
    _defaults = {
        'state': 'deactivated',
    }

class mrp_routing_workcenter(osv.osv):
    _inherit = 'mrp.routing.workcenter'
    
    def _get_cost(self, cr, uid, ids, field_names=None, arg=False, context=None):
        context = context or {}
        res = dict.fromkeys(ids, 0)
        for routing_workcenter in self.browse(cr, uid, ids, context=context):
            cost=0.0
            cost=routing_workcenter.hour_nbr*routing_workcenter.cycle_nbr*routing_workcenter.workcenter_id.costs_hour
            res[routing_workcenter.id]= {'cost':cost}
        return res
    
    _columns = {
        'type': fields.selection([('variable_time', 'Variable Time'), ('invariable_time', 'Invariable Time'),('variable_time_pi', 'Variable Time pi'),('variable_time_pi2', 'Variable Time pi(2)')], 'Timing type', required=True),
        'uom_id': fields.many2one('product.uom', 'Unit of Measure'),
        'cost': fields.function(_get_cost, type='float',multi='cost', digits_compute=dp.get_precision('Product Price'), string='Cost'),
        'activity_id': fields.many2one('mrp.activity', 'Activity'),
        'activity_line_id': fields.many2one('mrp.activity.line', 'Activity Line', domain="[('activity_id', '=', activity_id)]"),
    }  
    _defaults = {
        'cost': 0.0
    }
    
    def onchange_type(self, cr, uid, ids, type):
        if type:
            return {'value': {'uom_id': self._get_uom_id(cr, uid,type)}}
        return {}
    
    def _get_uom_id(self, cr, uid,type):
        try:
            proxy = self.pool.get('ir.model.data')
            if type=='variable_time_pi2':
                result = proxy.get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'product_uom_pi')
            else:
                result = proxy.get_object_reference(cr, uid, 'product', 'product_uom_unit')
            return result[1]
        except Exception, ex:
            return False
    
class mrp_workcenter(osv.osv):
    _inherit = 'mrp.workcenter'
    _columns = {
        'costs_hour': fields.float('Cost per hour', required=True,help="Specify Cost of Work Center per hour.")
    }

class mrp_routing(osv.osv):
    _inherit = 'mrp.routing'
     
    def _get_routing_costs(self, cr, uid, ids, field_names=None, arg=False, context=None):
        context = context or {}
        res = dict.fromkeys(ids, 0)
        mrp_rounting_workcenter_obj=self.pool.get('mrp.routing.workcenter') #OP
        for routing in self.browse(cr, uid, ids, context=context):
            routing_costs=0.0
            workcenter_lines=self.browse(cr, uid, routing.id, context=context).workcenter_lines.ids
            for op in workcenter_lines:
                routing_costs=routing_costs + mrp_rounting_workcenter_obj.browse(cr, uid,op, context=context).cost
            res[routing.id]= {'routing_costs':routing_costs}
        return res
    
    _columns = {
        'routing_costs': fields.function(_get_routing_costs, type='float',multi='routing_costs', digits_compute=dp.get_precision('Product Price'), string='Routing costs'),
    }
    _defaults = {
        'routing_costs': 0.0
    }
    
class mrp_production_workcenter_line(osv.Model):

    _inherit = "mrp.production.workcenter.line"
    _columns = {

        'routing_id': fields.related('production_id', 'routing_id', type='many2one', relation='mrp.routing', string='Routing', store=True),
    }

    def _read_group_workcenter_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None):

        routing_obj = self.pool.get('mrp.routing')
        workcenter_obj = self.pool.get('mrp.workcenter')

        workcenter_ids = []
        if context.get('active_id', False):
            routing_brw = routing_obj.browse(cr, uid,
                                             context.get('active_id', False), context=context)

            for work_line in routing_brw.workcenter_lines:
                workcenter_ids.append(work_line.workcenter_id.id)
            workcenter_ids = map(lambda x: x, set(workcenter_ids))
            work_orders_ids = self.search(cr, uid, [("workcenter_id", "in", workcenter_ids), (
                "routing_id", "=", context.get('active_id', False))], context=context)
        else:
            workcenter_ids = workcenter_obj.search(
                cr, uid, [], context=context)
            work_orders_ids = self.search(cr, uid, [(
                "workcenter_id", "in", workcenter_ids)], context=context)

        lista_workcenter = workcenter_obj.browse(
            cr, uid, workcenter_ids, context=context)
       # lista_workcenter.sort(key=lambda x: x.sequence)
       # lista_workcenter.reverse()

        # Lista de tuplas (id, name)
        result = []
        for i in lista_workcenter:
            result.append((i.id, i.name))

        # Si se despliega o no
        visible = {}
        for i in workcenter_ids:
            visible[i] = False

        return result, visible

    _group_by_full = {
        'workcenter_id': _read_group_workcenter_ids,
    }    

class hvc_mrp_state(osv.osv):
    _name = "hvc.mrp.state"
    _description = "Stage of case"
    _rec_name = 'name'
    _order = "sequence"
    
    def _get_next_sequence(self, cr, uid, ids):
        cr.execute("SELECT max(sequence) FROM hvc_mrp_state ")
        try:
            db_result=cr.fetchall()
            return db_result[0][0]+1
        except :
            logger.error('An exception occured when trying to access to the  database')                                   
            raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))

    _columns = {
        'name': fields.char('Stage Name', required=True, translate=True),
        'sequence': fields.integer('Sequence', readonly=True,help="Used to order stages. Lower is better."),
        'case_default': fields.boolean('Default stage', #to updtae
                                       help="If you check this field, this stage will be proposed by default on each sales team. It will not assign this stage to existing teams."),
        'fold': fields.boolean('Folded in Kanban View', help='This stage is folded in the kanban view when there are no records in that stage to display.'),
    }

    _defaults = {
        'sequence':_get_next_sequence,
        'fold': False,
        'case_default': True,
    } 

class stock_move_kanban(osv.osv): 
    _name = "stock.move.kanban"
    _rec_name='product_id'
    _columns = {
        'product_id': fields.many2one('product.product', 'Product'),
        'product_uom_qty': fields.float('Quantity', digits_compute=dp.get_precision('Product Unit of Measure')),
        'product_uom': fields.many2one('product.uom', 'Unit of Measure'),
        'move_id': fields.many2one('stock.move', 'move', ondelete='cascade'),
    } 
    def name_get(self, cr, uid, ids, context=None):
        res = []
        for line in self.browse(cr, uid, ids, context=context):
            name = line.product_id.name + ' : ' + str(line.product_uom_qty) + ' : ' +line.product_uom.name
            res.append((line.id, name))
        return res       

class mrp_production(osv.osv):
    _inherit = "mrp.production"
    _name = "mrp.production"

    def create(self, cr, uid, vals, context=None):
        if not vals.get('color',False):
            vals['color']=random.randrange(10)
        return super(mrp_production, self).create(cr, uid, vals, context=context)
    
    def _read_group_stage_ids(self, cr, uid, ids, domain, read_group_order=None, access_rights_uid=None, context=None):
        access_rights_uid = access_rights_uid or uid
        stage_obj = self.pool.get('hvc.mrp.state')
        order = stage_obj._order
        # lame hack to allow reverting search, should just work in the trivial case
        if read_group_order == 'stage_id desc':
            order = "%s desc" % order
        # retrieve section_id from the context and write the domain
        # - ('id', 'in', 'ids'): add columns that should be present
        # - OR ('case_default', '=', True), ('fold', '=', False): add default columns that are not folded
        search_domain = []
        search_domain += ['|', ('id', 'in', ids), ('case_default', '=', True)]
        # perform search
        stage_ids = stage_obj._search(cr, uid, search_domain, order=order, access_rights_uid=access_rights_uid, context=context)
        result = stage_obj.name_get(cr, access_rights_uid, stage_ids, context=context)
        # restore order of the search
        result.sort(lambda x, y: cmp(stage_ids.index(x[0]), stage_ids.index(y[0])))

        fold = {}
        for stage in stage_obj.browse(cr, access_rights_uid, stage_ids, context=context):
            fold[stage.id] = stage.fold or False
        return result, fold

    def _get_side_id(self,cr,uid,code):
        ws_obj=self.pool.get('hvc.wood.sides')
        ws_ids= ws_obj.search(cr,uid,[('code','=',code)],limit=1)
        if ws_ids:
            return ws_obj.browse(cr,uid, ws_ids[0]).id
        else:
            return False
        
    def _get_move_lines(self, cr, uid, ids, name, arg, context=None):
        result = {}
        if context is None:
            context = {}
        stock_move_kanban = self.pool.get('stock.move.kanban')
        for mrp in self.browse(cr, uid, ids, context=context):
            move_line_kan_ids=[]
            for move in mrp.move_lines:
                stock_move_kanban.unlink(cr, uid,self.pool.get('stock.move.kanban').search(cr, uid, [('move_id','=', move.id)], context=context),context=context)
                move_line_kan_ids.append(stock_move_kanban.create(cr, uid, {
                        'product_id': move.product_id.id,
                        'product_uom_qty': move.product_uom_qty,
                        'product_uom': move.product_uom.id,
                        'move_id': move.id,
                    }, context=context))
            result[mrp.id] = {
                'move_lines_ids':[(6,0,move_line_kan_ids)],
                }
        return result
    
    def _get_base_caracts(self,cr,uid,product,context=None):
        base_caracts=[]
        if product.category_measurement=='planches':
            base_caracts=product.material_type_id.planches_attribute_base_ids.ids
        if product.category_measurement=='portes':
            base_caracts=product.material_type_id.portes_attribute_base_ids.ids
        if product.category_measurement=='tablettes':
            base_caracts=product.material_type_id.tablettes_attribute_base_ids.ids
        if product.category_measurement=='ensembles_monter':
            base_caracts=product.material_type_id.ensembles_monter_attribute_base_ids.ids
        return base_caracts
                   
    def _get_details(self, cr, uid, ids, field_names=None, arg=False, context=None):
        result = {}
        if context is None:
            context = {}
        attribut_obj=self.pool.get('product.attribute')
        attribut_value_obj=self.pool.get('product.attribute.value')
        length_att_id=attribut_obj.search(cr, uid, [('type', '=', "input_length")], context=context)
        width_att_id=attribut_obj.search(cr, uid, [('type', '=', "input_width")], context=context)
        side_att_id=attribut_obj.search(cr, uid, [('type', '=', "sides")], context=context)
        thickness_att_id=attribut_obj.search(cr, uid, [('type', '=', "thickness")], context=context)
        length=""
        width=""
        thickness=""
        sides=""
        for mrp in self.browse(cr, uid, ids, context=context):
            finitions=[]
            for value in mrp.product_id.attribute_value_ids:
                if value.attribute_id.id in length_att_id:
                    length=value.name
                elif value.attribute_id.id in width_att_id:
                    width=value.name
                elif value.attribute_id.id in thickness_att_id:
                    thickness=(attribut_value_obj.browse(cr, uid, value.id, context=context).name).split("/")
                    if len(thickness)>1:
                        pg_thickness=thickness[0].split(" ")
                        if len(pg_thickness)>1:
                            thickness=float(pg_thickness[0])+float(pg_thickness[1])/float((thickness[1].split("\""))[0])
                        else:
                            thickness=float(thickness[0])/float((thickness[1].split("\""))[0])
                    else:
                        thickness=float((thickness[0].split("\""))[0])
                    
                elif value.attribute_id.id in side_att_id:
                    sides=value.name
                else:
                    base_caracts=self._get_base_caracts(cr, uid, mrp.product_id, context=None)
                    if value.attribute_id.id not in base_caracts:
#                         logger.warning('value.name %s',value.name)
#                         logger.warning('value.id %s',value.id)
#                         if value.name like 
                        finitions.append(value.id)
            if sides:
                north_side_id=self._get_side_id(cr, uid,sides[0])
                south_side_id=self._get_side_id(cr, uid,sides[1])
                west_side_id=self._get_side_id(cr, uid,sides[2])
                east_side_id=self._get_side_id(cr, uid,sides[3])
            result[mrp.id] = {
                'length': length,
                'width': width,
                'sequence': mrp.sequence,
                'sides':sides,
                'north_side_id':north_side_id,
                'south_side_id':south_side_id,
                'west_side_id':west_side_id,
                'east_side_id':east_side_id,
                'thickness':thickness,
                'finitions_ids':[(6,0,finitions)],
                'attribute_values_ids': map(lambda y: y.id, [x for x in mrp.product_id.attribute_value_ids])
            }
        return result
    
    def _get_default_stage_id(self, cr, uid, context=None):
        search_domain = [('fold', '=', False),('case_default', '=', True)]
        stage_ids = self.pool.get('hvc.mrp.state').search(cr, uid, search_domain, order='sequence', limit=1, context=context)
        if stage_ids:
            return stage_ids[0]
        return False
  
    
#     def _get_stage_sequence (self, cr, uid, ids, name, arg, context=None):
#         res = {}
#         for mrp in self.browse(cr, uid, ids, context=context):
#             stage_id=self._get_default_stage_id(cr, uid, context=None)
#             if stage_id :
#                 res[mrp.id]={'sequence': self.browse(cr, uid, mrp.id, context=None).stage_id.sequence}
#         logger.warning(' stage_id %s',stage_id)
#         logger.warning(' res %s',res)
#         return res
    
    def _get_stage_sequence (self, cr, uid, ids, name, arg, context=None):
        res = {}
        for mrp in self.browse(cr, uid, ids, context=context):
            if mrp.stage_id :
                res[mrp.id]={'sequence': mrp.stage_id.sequence}
        return res
    
    _columns = {
        'child_ids': fields.one2many('res.partner', 'parent_id', 'Contacts', domain=[('active','=',True)]), # force "active_test" domain to bypass _search() override
        
        'thickness': fields.function(_get_details,type='char', multi='val', string='Thickness (inch)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The thickness in  (inch)."),
        'length': fields.function(_get_details,type='char',multi='val', string='Length (inch)',digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'width': fields.function(_get_details,type='char', multi='val', string='Width (inch)',digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'sides': fields.function(_get_details,type='char', multi='val', string='Sides'),
        'north_side_id': fields.function(_get_details,relation='hvc.wood.sides',type='many2one', multi='val', string='North side'),
        'south_side_id': fields.function(_get_details,relation='hvc.wood.sides',type='many2one', multi='val', string='South side'),
        'west_side_id': fields.function(_get_details,relation='hvc.wood.sides',type='many2one', multi='val', string='West side'),
        'east_side_id': fields.function(_get_details,relation='hvc.wood.sides',type='many2one', multi='val', string='East side'),
        
        'finitions_ids': fields.function(_get_details, multi='val', relation='product.attribute.value', string="finitions_ids", type='many2many'),
        'attribute_values_ids': fields.function(_get_details, multi='val', relation='product.attribute.value', string="Features", type='many2many'),
        
        'move_lines_ids': fields.function(_get_move_lines, multi='val1', relation='stock.move.kanban', string="Features", type='many2many'),
        'location_src_id': fields.many2one('stock.location', 'Raw Materials Location', required=True,
            readonly=True, states={'draft': [('readonly', False)],'confirmed': [('readonly', False)]},
            help="Location where the system will look for components."),
        'location_dest_id': fields.many2one('stock.location', 'Finished Products Location', required=True,
            readonly=True, states={'draft': [('readonly', False)],'confirmed': [('readonly', False)]},
            help="Location where the system will stock the finished products."),
        'move_lines': fields.one2many('stock.move', 'raw_material_production_id', 'Products to Consume',
            domain=[('state', 'not in', ('done', 'cancel'))], readonly=True, states={'draft': [('readonly', False)],'confirmed': [('readonly', False)]}),
        'product_lines': fields.one2many('mrp.production.product.line', 'production_id', 'Scheduled goods',
            readonly=True,states={'draft': [('readonly', False)],'confirmed': [('readonly', False)]}),
        'color': fields.integer('Color Index'),
        'stage_id': fields.many2one('hvc.mrp.state', 'Stage', select=True, required=True),
        'stage_domain_ids': fields.many2many('hvc.mrp.state','stage_mrp_domain_rel', id1='mrp',  id2='stage', string='Values Domain'),
        'sequence': fields.function(_get_stage_sequence,string='Sequence',type='integer', multi='val22',
            store={'mrp.production':  (lambda self, cr, uid, ids, c={}: ids, ['stage_id'], 10),}),
        'scraps':fields.boolean('Scraps'),
        'kanban_state': fields.selection([('blocked', 'In progress'),('done', 'Ready for delivery')], 'Kanban State',
                                 track_visibility='onchange',
                                 help="A task's kanban state indicates special situations affecting it:\n"
                                      " * Blocked indicates something is preventing the progress of this task\n"
                                      " * Ready for next stage indicates the task is ready to be pulled to the next stage",
                                 required=False,readonly=True, copy=False),
        'order_id': fields.many2one('sale.order', 'Sale Order'),
        'production_lot_id': fields.many2one('stock.production.lot', 'Lot'),
    }  
    
    _defaults = {
        'color': 0,
        'stage_id': lambda s, cr, uid, c: s._get_default_stage_id(cr, uid, c),
        'kanban_state': 'blocked',
    } 
    _group_by_full = {
        'stage_id': _read_group_stage_ids,
        'scraps':False
    }
    def _get_done_stage_id(self, cr, uid, context=None):
        stage_ids = self.pool.get('hvc.mrp.state').search(cr, uid,  [('sequence', '=', 6)], limit=1, context=context)
        if stage_ids:
            return stage_ids[0]
        return False

    def get_labels_report(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        context = dict(context or {}, active_ids=[self.browse(cr, uid, ids, context=context).production_lot_id.id], active_model=self._name)
        return {
             'type': 'ir.actions.report.xml',
             'report_name': 'stock.report_lot_barcode',
             'report_type':'qweb-pdf',
             'context': context,
        }
#      'report_name': 'website_sale_4cotes_cogen.report_lot_barcode_custom',
    def _get_related_production_details(self, cr, uid,ids,order_id, context=None):  
        update=True
        if order_id:
            mrp_ids = self.search(cr, uid, [('order_id','=',order_id),('id','!=',ids[0])], context=context) 
            for mrp in mrp_ids:
                if self.browse(cr, uid, mrp, context=context).kanban_state!='done':
                    update=False
                    break
        return  update
    
    def _update_production_cron(self, cr, uid,current_mrp, context=None):
        if context is None:
            context={}
        #For mrp without sale order
        if not current_mrp.order_id:
            cr.execute("Update  mrp_production SET  kanban_state='done' WHERE id=%s",(current_mrp.id,))
        #for mrp from sale order
        update=True
        mrp_ids = self.search(cr, uid, [('order_id','=',current_mrp.order_id.id),('id','!=',current_mrp.id)], context=context) 
        for mrp in mrp_ids:
            if self.browse(cr, uid, mrp, context=context).sequence not in [5,6] or self.browse(cr, uid, mrp, context=context).kanban_state=='done':
                update=False
                break
        if update:
            mrp_ids.append(current_mrp.id)
            for mrp in mrp_ids:
                cr.execute("Update  mrp_production SET  kanban_state='done' WHERE id=%s",(mrp,))
        return True
    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(mrp_production, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        doc = etree.XML(res['arch'])
        for node in doc.xpath("//field[@name='product_id']"):
                node.set('domain', "[('purchase_ok', '=', False)]")
        res['arch'] = etree.tostring(doc)
        return res  
    
    def onchange_product_id(self, cr, uid, ids, product_id, product_qty=0, context=None):
        res=self.product_id_change(cr, uid, ids, product_id,product_qty, context)
        stage_ids=self.pool.get('product.product').browse(cr, uid, product_id, context=None).material_type_id.stages_ids.ids
        res['value'].update({'stage_domain_ids':[(6,0,stage_ids)]})   
        return res
    
    def action_assign(self, cr, uid, ids, context=None):
        """
        Checks the availability on the consume lines of the production order
        """
#         logger.error('_____ INaction_assign ____')
        from openerp import workflow
        move_obj = self.pool.get("stock.move")
        for production in self.browse(cr, uid, ids, context=context):
            move_obj.action_assign(cr, uid, [x.id for x in production.move_lines], context=context)
            if self.pool.get('mrp.production').test_ready(cr, uid, [production.id]):
                workflow.trg_validate(uid, 'mrp.production', production.id, 'moves_ready', cr)

    def force_production(self, cr, uid, ids, *args):
        """ Assigns products.
        @param *args: Arguments
        @return: True
        """
        from openerp import workflow
        move_obj = self.pool.get('stock.move')
        for order in self.browse(cr, uid, ids):
            move_obj.force_assign(cr, uid, [x.id for x in order.move_lines])
            if self.pool.get('mrp.production').test_ready(cr, uid, [order.id]):
                workflow.trg_validate(uid, 'mrp.production', order.id, 'moves_ready', cr)
        return True
    
    def _set_production_details(self,cr, uid, mrp, context):
        prod_line_obj = self.pool.get('mrp.production.product.line')
        cr.execute('DELETE FROM mrp_production_product_line WHERE production_id =%s',(mrp.id,))
        for move_line in mrp.move_lines:
            product=self.pool.get('product.product').browse(cr, uid,move_line.product_id.id,context=context)
            line = {
                    'name': mrp.name,
                    'product_id': move_line.product_id.id,
                    'product_qty':move_line.product_uom_qty,
                    'product_uom': move_line.product_uom.id,
                    'product_uos_qty': product.uos_id and product.uos_qty or False,
                    'product_uos': product.uos_id or False,
                    'production_id': mrp.id,
            }
            prod_line_obj.create(cr, uid, line, context=context)
        return True
    
   
    def _get_new_stage_by_material_type(self,cr, uid, mrp, context):
        stage_domain_ids=mrp.stage_domain_ids.ids
        for i in range(len(stage_domain_ids)):
            if mrp.stage_id.id==stage_domain_ids[i]:
                return stage_domain_ids[i+1]
        return False
    def write(self, cr, uid, ids, vals, context=None, update=True, mini=True):
        mrp=self.browse(cr, uid, ids, context=None)
#         logger.warning('_____ IN write ____')

        if vals.get('stage_id', False):
            stage_obj=self.pool.get('hvc.mrp.state')
            new_stage=stage_obj.browse(cr, uid,vals.get('stage_id', False),context=context).name
            new_seq=self.pool.get('hvc.mrp.state').browse(cr, uid,vals.get('stage_id', False),context=context).sequence
#             logger.warning('stage de %s >>> %s',mrp.stage_id.name,new_stage)
#             logger.warning('sequence de %s >>> %s',mrp.sequence,new_seq)

            if vals.get('stage_id', False) not in  mrp.stage_domain_ids.ids:
                new_stage_id=self._get_new_stage_by_material_type(cr, uid, mrp, context )
                right_new_stage=stage_obj.browse(cr, uid,new_stage_id,context=context).name
                raise osv.except_osv(_('Error!'), _('You must respect order of production steps: from %s to %s' %(mrp.stage_id.name,right_new_stage)))
            
            
            if ((mrp.sequence==1 or mrp.sequence==2) and not mrp.move_lines):
                raise osv.except_osv(_('Error!'), _('Veuillez introduire la matière première à consommer en cliquant sur Proposition'))
            
            if (mrp.sequence==1 and new_seq not in [2,7]) :
                new_stage_id=self._get_new_stage_by_material_type(cr, uid, mrp, context )
                right_new_stage=stage_obj.browse(cr, uid,new_stage_id,context=context).name
                raise osv.except_osv(_('Error!'), _('You must respect order of production steps: from %s to %s' %(mrp.stage_id.name,right_new_stage)))
            
            if (mrp.sequence==2 and new_seq in [3,4,5] and  mrp.move_lines):
                if mrp.state=='draft':
                    self.signal_workflow(cr, uid, ids, 'button_confirm')
                self.force_production(cr, uid, ids, context)
                self.signal_workflow(cr, uid, ids, 'button_produce')
            
            if ((mrp.sequence in [3,4,5,6])and new_seq in [1,2]):  
                raise osv.except_osv(_('Error!'), _('production has been started you can\'t go back to the previous step'))
            
            if (mrp.sequence in [2,3,4] and new_seq==5):  
                self._update_production_cron(cr, uid,mrp, context)
                res=self.get_mrp_product_produce(cr, uid, ids, context)
                if res:
                    product_produce_id=res['product_produce_id']
                    if product_produce_id:
                        data = self.pool.get('mrp.product.produce').browse(cr, uid, product_produce_id, context=context)
                        self.do_produce(cr, uid, ids[0], data.product_qty, data, context)
                    vals['production_lot_id'] = res['lot_id']
            
            if ((mrp.sequence not in [1,2]) and new_seq==7):  
                raise osv.except_osv(_('Error!'), _('You can\'t cancel started production' ))
            
            elif ((mrp.sequence==1 or mrp.sequence==2 ) and new_seq==7): 
                self.action_cancel(cr, uid, ids, context=context)
                
            if (mrp.sequence==6):  
                raise osv.except_osv(_('Error!'), _('Production has been done you can\'t go to another stage'))
            
            if (mrp.sequence==7):  
                raise osv.except_osv(_('Error!'), _('Production has been cancled you can\'t go back'))
            
            if (mrp.sequence==5 and new_seq in [1,2,3,4]):  
                raise osv.except_osv(_('Error!'), _('You can\'t go back to the previous step: production is packaging'))
            
            if  (mrp.sequence!=5 and new_seq==6): 
                stage_id = stage_obj.search(cr, uid, [('sequence', '=', mrp.sequence+1)], order='sequence', limit=1, context=context)
                right_new_stage=stage_obj.browse(cr, uid,stage_id,context=context).name
                raise osv.except_osv(_('Error!'), _('You must respect order of production steps: from %s to %s' %(mrp.stage_id.name,right_new_stage)))
            
#             if  (mrp.sequence==5 and new_seq==6):
#                 if not self._get_related_production_details(cr, uid, ids, mrp.order_id.id, context):
#                     raise osv.except_osv(_('Error!'), _('You have others production orders in progress for the sale order %s' %(mrp.order_id.name)))
            
#             if (mrp.sequence>1 and new_seq!=7 and new_seq<mrp.sequence):  
#                 raise osv.except_osv(_('Error!'), _('You can\'t go back to the previous step: production has been started'))
        result = super(mrp_production, self).write(cr, uid, ids, vals, context=context)
        if vals.get('move_lines', False):
            self._set_production_details(cr, uid, mrp,context)
        return result
#    

    def do_produce(self, cr, uid, production_id,product_qty,product_produce_id, context=None):
        self.pool.get('mrp.production').action_produce(cr, uid, production_id,product_qty, 'consume_produce', product_produce_id, context=context)
        return {}
    
    def create_lot(self, cr, uid, ids,product_id, context=None):
        if not ids:
            return False
        vals={}
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'stock.lot.serial') or '/'
        order_name=""
        order_id=self.pool.get('mrp.production').browse(cr, uid,ids, context=context).order_id
        if order_id:
            order_name= '/' + str(self.pool.get('mrp.production').browse(cr, uid,ids, context=context).order_id.name)
            vals['order_id']=self.pool.get('mrp.production').browse(cr, uid,ids, context=context).order_id.id
        vals['product_id']=product_id
        vals['ref']='[' + str(self.pool.get('mrp.production').browse(cr, uid,ids, context=context).name) + order_name + ']'
        vals['production_id']=ids[0]
        return self.pool.get('stock.production.lot').create(cr, uid, vals, context=context) 
    
    def get_mrp_product_produce(self, cr, uid, ids, context=None): 
        vals={}
        product_qty=self._get_product_qty(cr, uid,ids, context)
        product_id=self._get_product_id(cr, uid, ids, context)
        lot_id=self.create_lot(cr, uid, ids,product_id, context)
        vals['product_qty']=product_qty
        vals['mode']='consume_produce'
        vals['product_id']=product_id
        vals['track_production']=self._get_track(cr, uid, ids, context)
        vals['consume_lines']=self.to_packing(cr, uid, ids, product_qty, context)
        vals['lot_id']=lot_id
#         logger.warning('vals  :%s',vals)
        res={'product_produce_id':self.pool.get('mrp.product.produce').create(cr, uid, vals, context=context),
             'lot_id':lot_id}
        return res
    
    def to_packing(self, cr, uid, production_id,product_qty, context=None):
        prod_obj = self.pool.get("mrp.production")
        uom_obj = self.pool.get("product.uom")
        production = prod_obj.browse(cr, uid, production_id, context=context)
        consume_lines = []
        new_consume_lines = []
        if product_qty > 0.0:
            product_uom_qty = uom_obj._compute_qty(cr, uid, production.product_uom.id, product_qty, production.product_id.uom_id.id)
            consume_lines = prod_obj._calculate_qty(cr, uid, production, product_qty=product_uom_qty, context=context)
           
        for consume in consume_lines:
            new_consume_lines.append([0, False, consume])
        return new_consume_lines
    
    def _get_track(self, cr, uid,ids, context=None):
        prod = self._get_product_id(cr, uid,ids, context=context)
        prod_obj = self.pool.get("product.product")
        return prod and prod_obj.browse(cr, uid, prod, context=context).track_production or False

    def _get_product_id(self, cr, uid,ids, context=None):
        """ To obtain product id
        @return: id
        """
        prod=False
        prod = self.pool.get('mrp.production').browse(cr, uid,ids, context=context)
        return prod and prod.product_id.id or False
    
    def _get_product_qty(self, cr, uid,ids, context=None):
        if context is None:
            context = {}
        prod = self.pool.get('mrp.production').browse(cr, uid,ids, context=context)
        done = 0.0
        for move in prod.move_created_ids2:
            if move.product_id == prod.product_id:
                if not move.scrapped:
                    done += move.product_uom_qty # As uom of produced products and production order should correspond
        return prod.product_qty - done

class stock_location(osv.osv):
    _inherit = "stock.location"
    _columns = {
        'raw_material': fields.boolean('Raw Material'),
        'sfp_material': fields.boolean('Semi Finished Products'),
    }   
    
class stock_production_lot(osv.osv):
    _inherit = 'stock.production.lot'
    
    def _get_lot(self, cr, uid, ids, context=None):
        result = {}
        for quant in self.pool.get('stock.quant').browse(cr, uid, ids, context=context):
            result[quant.lot_id.id] = True
        return result.keys()
    
    def _get_all_qty(self, cr, uid, ids, field_name, arg, context=None):
        """ Wrapper because of direct method passing as parameter for function fields """
        return self._qty_all(cr, uid, ids, field_name, arg, context=context)

    def _qty_all(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for lot in self.browse(cr, uid, ids, context=context):
            res[lot.id] = {
                'quantity': 0.0,
            }
            val = val1 = 0.0
            for line in lot.quant_ids:
                val1 += line.qty
            res[lot.id]['quantity'] = val1
        return res
    _columns = {
        'order_id': fields.many2one('sale.order', 'Sale Order'),
        'production_id': fields.many2one('mrp.production', 'Production order'),
        'quantity': fields.function(_get_all_qty, digits_compute=dp.get_precision('Account'), string='Quantity Total',
            store={
                'stock.production.lot': (lambda self, cr, uid, ids, c={}: ids, ['quant_ids'], 10),
                'stock.quant': (_get_lot, ['qty'], 10),
            },
            multi='sums', help="The total qty."),
    } 

# class mrp_production_product_line(osv.osv):
#     _inherit = 'mrp.production.product.line'
#     _description = 'Production Scheduled Product'
#     _columns = {
#         'move_id': fields.many2one('stock.move', 'move'),
#     }

class stock_picking(osv.osv):
    _inherit = 'stock.picking'
 
    def _get_all_splited_move(self,cr, uid, move, context):
        stock_move_obj = self.pool.get('stock.move')
        all_splited_moves=[]
        has_split=False
        if  move.split_from.id:
            all_splited_moves.append(move.split_from.id)
            has_split=True
        
        while has_split:
                origin_split_move=stock_move_obj.browse(cr, uid,all_splited_moves[len(all_splited_moves)-1], context)
                if  origin_split_move.split_from.id:
                    all_splited_moves.append(origin_split_move.split_from.id)
                else:
                    has_split=False
        
#         logger.warn('all_splited_moves %s',all_splited_moves)
        return all_splited_moves
    
    @api.cr_uid_ids_context
    def do_transfer(self, cr, uid, picking_ids, context=None):
        """
            If no pack operation, we do simple action_done of the picking
            Otherwise, do the pack operations
        """
        if not context:
            context = {}
        stock_move_obj = self.pool.get('stock.move')
        for picking in self.browse(cr, uid, picking_ids, context=context):
            if not picking.pack_operation_ids:
                self.action_done(cr, uid, [picking.id], context=context)
                continue
            else:
                need_rereserve, all_op_processed = self.picking_recompute_remaining_quantities(cr, uid, picking, context=context)
                #create extra moves in the picking (unexpected product moves coming from pack operations)
                todo_move_ids = []
                if not all_op_processed:
                    todo_move_ids += self._create_extra_moves(cr, uid, picking, context=context)
 
                #split move lines if needed
                toassign_move_ids = []
                for move in picking.move_lines:
                    remaining_qty = move.remaining_qty
                    if move.state in ('done', 'cancel'):
                        #ignore stock moves cancelled or already done
                        continue
                    elif move.state == 'draft':
                        toassign_move_ids.append(move.id)
                    if float_compare(remaining_qty, 0,  precision_rounding = move.product_id.uom_id.rounding) == 0:
                        if move.state in ('draft', 'assigned', 'confirmed'):
                            todo_move_ids.append(move.id)
                    elif float_compare(remaining_qty,0, precision_rounding = move.product_id.uom_id.rounding) > 0 and \
                                float_compare(remaining_qty, move.product_qty, precision_rounding = move.product_id.uom_id.rounding) < 0:
                        new_move = stock_move_obj.split(cr, uid, move, remaining_qty, context=context)
                        todo_move_ids.append(move.id)
                        #Assign move as it was assigned before
                        toassign_move_ids.append(new_move)
                if need_rereserve or not all_op_processed: 
                    if not picking.location_id.usage in ("supplier", "production", "inventory"):
                        self.rereserve_quants(cr, uid, picking, move_ids=todo_move_ids, context=context)
                    self.do_recompute_remaining_quantities(cr, uid, [picking.id], context=context)
                if todo_move_ids and not context.get('do_only_split'):
                    self.pool.get('stock.move').action_done(cr, uid, todo_move_ids, context=context)
                elif context.get('do_only_split'):
                    context = dict(context, split=todo_move_ids)
            
            
            backorder=self._create_backorder(cr, uid, picking, context=context)
            if toassign_move_ids:
                stock_move_obj.action_assign(cr, uid, toassign_move_ids, context=context)
            
            if todo_move_ids:
                picking_obj=self.pool.get('stock.picking')
                mrp_obj=self.pool.get('mrp.production')
                if not backorder:
                    for todo_move_id in todo_move_ids:
                        move=stock_move_obj.browse(cr, uid,todo_move_id, context)
                        all_splited_moves=self._get_all_splited_move(cr, uid, move, context)
                        mrp_move_prod_id=mrp_obj.search(cr, uid, [('move_prod_id','=',move.id)], limit=1, context=context)
                        mrp_split_from_move_prod_id=mrp_obj.search(cr, uid, [('move_prod_id','in',all_splited_moves)], limit=1, context=context)
                        if mrp_move_prod_id:
                            mrp_obj.write(cr, uid, mrp_move_prod_id,{'stage_id':mrp_obj._get_done_stage_id(cr, uid,context )},context)
                        elif mrp_split_from_move_prod_id:
                            mrp_obj.write(cr, uid, mrp_split_from_move_prod_id,{'stage_id':mrp_obj._get_done_stage_id(cr, uid,context )},context)
                            
                else:
                    for todo_move_id in todo_move_ids:
                        update=True
                        move=stock_move_obj.browse(cr, uid,todo_move_id, context)
                        all_splited_moves=self._get_all_splited_move(cr, uid, move, context)
                        mrp_move_prod_id=mrp_obj.search(cr, uid, [('move_prod_id','=',move.id)], limit=1, context=context)
                        mrp_split_from_move_prod_id=mrp_obj.search(cr, uid, [('move_prod_id','in',all_splited_moves)], limit=1, context=context)
                        if mrp_move_prod_id:
                            for mv in picking_obj.browse(cr, uid,backorder,context).move_lines:
                                if mv.split_from.id == move.id:
                                    update=False
                                    break
                            if update:
                                mrp_obj.write(cr, uid, mrp_move_prod_id,{'stage_id':mrp_obj._get_done_stage_id(cr, uid,context )},context)
                        elif mrp_split_from_move_prod_id:
                            for mv in picking_obj.browse(cr, uid,backorder,context).move_lines:
                                if mv.split_from.id ==  move.id:
                                    update=False
                                    break
                            if update:
                                mrp_obj.write(cr, uid, mrp_split_from_move_prod_id,{'stage_id':mrp_obj._get_done_stage_id(cr, uid,context )},context)
        return True
