# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants (Wided BEN OTHMEN | Hafedh RAHMANI) 
#    (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import math

import logging
logger= logging.getLogger('4cotes module')

class variant_details(osv.osv_memory):
    _name ='variant.details'
    _rec_name='attribute_id'

    
    _columns = {
        'attribute_id': fields.many2one('product.attribute', 'Attribute',required=True),
        'attribute_value_id': fields.many2one('product.attribute.value', 'Value',required=True),
        'generate_variant_id': fields.many2one('generate.variant', 'Generator'),
        'attribute_value_ids': fields.many2many('product.attribute.value', id1='details_id', id2='att_id', string='Values Domain'),
    }
    
 
class generate_variant(osv.osv_memory):
    _name ='generate.variant'
    _rec_name='product_tmpl_id'
    def _get_product_tmpl_id(self, cr, uid, context=None):
        if context.get('from_sale_order',False):
            return False
        return context.get('active_id', False)  
    
    def _get_min_max_measures(self,cr,uid,product_tmpl_id,measure,context=None):
        if product_tmpl_id:
            template=self.pool.get('product.template').browse(cr, uid,product_tmpl_id,context=context)
            if measure=='metric':
                res= {'max_width':1+template.max_width*25.4,
                      'min_width':template.min_width*25.4,
                      'max_length':1+template.max_length*25.4,
                      'min_length':template.min_length*25.4,
                    }
            else:
                res= {'max_width':template.max_width+1,
                      'min_width':template.min_width,
                      'max_length':template.max_length+1,
                      'min_length':template.min_length
                    }
            return res
        return  {}  
        
    def _check_width_length(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.product_tmpl_id.category_measurement in ['planches','portes','tablettes','ensembles_monter']:
                if wizard.measure:
                    res=self._get_min_max_measures(cr,uid,wizard.product_tmpl_id.id,wizard.measure,context=context)
                    min_length=res['min_length']
                    max_length=res['max_length']
                    min_width=res['min_width']
                    max_width=res['max_width']
                    if wizard.input_length<min_length or wizard.input_length>max_length:
                        return False
                    if wizard.input_width<min_width or wizard.input_width>max_width:
                        return False
        return True
    
    def _check_attribute_line(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.attribute_line_ids:
                g_attribute_ids=[a.attribute_id.id for a in wizard.attribute_line_ids]
                t_attribute_ids=[a.attribute_id.id for a in wizard.product_tmpl_id.attribute_line_ids  if a.attribute_id.type not in ['input_length','input_width','sides'] ]
                res=cmp(g_attribute_ids, t_attribute_ids)
                if res!=0:
                    return False
                else:
                    return True
            else:
                return False
        return True
    
    _columns = {   
        'attribute_line_ids': fields.one2many('variant.details', 'generate_variant_id',  'Details',required=True),
        'product_tmpl_id':fields.many2one('product.template', 'Template',required=True),
        'category_measurement': fields.char('Category Measurement'),
        'template_excepts':fields.boolean('Exceptions'),
        'input_length': fields.float('Length' , digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'input_width': fields.float('Width' , digits_compute=dp.get_precision('Product Unit of Measure pi2')),
        'max_width': fields.float('Maximum width(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),readonly=True, help="The maximum width in inches."),
        'min_width': fields.float('Minimum width(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),readonly=True, help="The minimum width in inches."),
        'max_length': fields.float('Maximum length(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),readonly=True, help="The maximum length in inches."),
        'min_length': fields.float('Minimum length(inches)', digits_compute=dp.get_precision('Product Unit of Measure pi2'),readonly=True, help="The minimum length in inches."),
        'side_n':fields.many2one('hvc.wood.sides', 'Side N'),
        'side_s':fields.many2one('hvc.wood.sides', 'Side S'),
        'side_w':fields.many2one('hvc.wood.sides', 'Side W'),
        'side_e':fields.many2one('hvc.wood.sides', 'Side E'),
        'measure': fields.selection([('imperial', 'Imperial (inch)'),('metric','Metric (mm) ')],'Measure',required=True),
        'state': fields.selection([('draft', 'Draft'),('confirmed','Confirmed')],'State'),
        'product_uom_qty': fields.float('Quantity', digits_compute= dp.get_precision('Product UoS')),
        'sale_ok': fields.boolean('Can be Sold', help="Specify if the product can be selected in a sales order line."),
        'purchase_ok': fields.boolean('Can be Purchased', help="Specify if the product can be selected in a purchase order line."),
    }
    
    _defaults = {
        'product_tmpl_id':_get_product_tmpl_id,
        'measure':'imperial',
        'input_length':1,
        'input_width':1,
        'state':'confirmed',
        'product_uom_qty':1.0,
        'purchase_ok': 1,
        'sale_ok': 0,
        
    }
    _constraints = [
        (_check_width_length, 'Please enter a valid length or width number !!', ['input_width','input_length']),
        (_check_attribute_line, 'Please do not remove any attribute line !!', ['attribute_line_ids']),
    ]
    
    def get_attribute_lines(self, cr, uid,product_tmpl_id,context=None):
        attribute_line_ids=[]
        template=self.pool.get('product.template').browse(cr,uid,product_tmpl_id,context=None)
        if template.attribute_line_ids:
            for line in template.attribute_line_ids:
                if line.attribute_id.type not in ['input_length','input_width','sides']:
                    attribute_line_ids.append((0, 0,  {'attribute_id':line.attribute_id.id,
                                                       'attribute_value_ids':[(6, 0, [value.id for value in line.value_ids])],
                                                       'attribute_value_id':line.value_ids.ids[0] if line.value_ids else False }))
        return attribute_line_ids
    
    def onchange_measure(self, cr, uid,ids,product_tmpl_id=False,measure=False,context=None):
        if product_tmpl_id and measure:
            return {'value':self._get_min_max_measures(cr,uid,product_tmpl_id,measure,context=context)}
        return {}

    def onchange_product_tmpl_id(self, cr, uid,ids,product_tmpl_id=False,context=None):
        if product_tmpl_id:
            state='confirmed'
            template_excepts=False
            template=self.pool.get('product.template').browse(cr, uid,product_tmpl_id,context=context)
            sides=[side.id for side in template.material_type_id.sides_ids]
            if template.template_exceptions_ids:
                template_excepts=True
                state='draft'
            return {'value':
                           {'attribute_line_ids':self.get_attribute_lines(cr, uid, product_tmpl_id, context),
                            'category_measurement':template.category_measurement,
                            'max_width':template.max_width+1,
                            'min_width':template.min_width,
                            'max_length':template.max_length+1,
                            'min_length':template.min_length,
                            'template_excepts':template_excepts,
                            'state':state,
                            'side_n':sides[0] if len(sides) else False,
                            'side_s':sides[0] if len(sides) else False,
                            'side_w':sides[0] if len(sides) else False,
                            'side_e':sides[0] if len(sides) else False,
                            },
                    'domain':
                           {'side_n':[('id', 'in', sides)],
                            'side_s':[('id', 'in', sides)],
                            'side_w':[('id', 'in', sides)],
                            'side_e':[('id', 'in', sides)]
                           }
                   }
        return {}
    
    def _get_attributes_lines_values_ids(self,cr,uid,template_id,length,width,sides='0',context=None):
        attribute_line_obj = self.pool.get('product.attribute.line')
        types_attributes=['input_length','input_width','sides']
        result=[]
        for type_att in types_attributes:
            lines_ids=attribute_line_obj.get_attribute_line_type(cr, uid,template_id,type_att,context=None)
            if lines_ids:
                if type_att=='input_length' and length !='0':
                    result.append(attribute_line_obj.get_attribute_line_values_id(cr, uid,lines_ids[0],length,context=None))
                if type_att=='input_width'  and width !='0':
                    result.append(attribute_line_obj.get_attribute_line_values_id(cr, uid,lines_ids[0],width,context=None))
                if type_att=='sides' and sides !='0':
                    result.append(attribute_line_obj.get_attribute_line_values_id(cr, uid,lines_ids[0],sides,context=None))

        return result

    def get_type(self, cr, uid, val):
        if  val - math.floor(val)==0:
            res=int(val) 
        else:
            res=float(val) 
        return res

    def _get_input_details(self,cr, uid,length,width,measure):
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure pi2')
        if measure=='imperial':
            res={'input_length':self.get_type(cr, uid,length),
                 'input_width':self.get_type(cr, uid, width),
                  'surface':round((length * width)/144,precision)
                }
        else:
            res={'input_length':round(self.get_type(cr, uid,length)/25.4,precision),
                 'input_width':round(self.get_type(cr, uid, width)/25.4,precision),
                  'surface':round((length * width)/(144*645.16),precision)
                }
            
        return res
    
    def get_measure(self,cr,uid,ids,context=None):
        vals={ 'state':'confirmed' }
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'generate_variant_form')
        wizard=self.browse(cr,uid,ids[0],context=None)
        if wizard.product_tmpl_id and wizard.attribute_line_ids:
            min_length=wizard.product_tmpl_id.min_length
            max_length=wizard.product_tmpl_id.max_length+1
            min_width=wizard.product_tmpl_id.min_width
            max_width=wizard.product_tmpl_id.max_width+1
            template_exceptions_ids = [ [ [v.id for v in e.attribute_value_ids ],e.min_length,e.max_length,e.min_width,e.max_width] for e in wizard.product_tmpl_id.template_exceptions_ids]
            attribute_value_ids=[v.attribute_value_id.id for v in wizard.attribute_line_ids]
            for ex in template_exceptions_ids:
                if reduce(lambda v1,v2: v1 or v2, map(lambda v: v in ex[0], attribute_value_ids)): 
                    if wizard.measure=='imperial':
                        min_length=ex[1]
                        max_length=ex[2]
                        min_width=ex[3]
                        max_width=ex[4]
                    else:
                        min_length=ex[1]*25.4
                        max_length=ex[2]*25.4
                        min_width=ex[3]*25.4
                        max_width=ex[4]*25.4

                    break
            vals['max_length']=max_length
            vals['min_length']=min_length
            vals['max_width']=max_width
            vals['min_width']=min_width
            self.write(cr, uid,ids[0],vals, context=context )
        return {   
            'name':_("Generate Variant"),
            'view_mode': 'form',
            'res_id': wizard.id,
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'generate.variant',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': context
            }

    def _get_edge_length(self,cr,uid,input_length,input_width,sides,context=None):
        edge_length=0.0
        if sides[0]=='1':
            edge_length+=input_length
        if sides[1]=='1':
            edge_length+=input_length
        if sides[2]=='1':
            edge_length+=input_width
        if sides[3]=='1':
            edge_length+=input_width
        
        return edge_length/12
        
    def create_product(self,cr,uid,ids,context=None):
        wizard=self.browse(cr,uid,ids[0],context=None)
        value_ids=[]
        input_values=[]
        values=[]
        edge_length=0.0
        if wizard.product_tmpl_id:
            res=self._get_input_details(cr, uid,wizard.input_length,wizard.input_width,wizard.measure)
            input_length= res['input_length']
            input_width= res['input_width']
            surface= res['surface']
            if wizard.product_tmpl_id.category_measurement == "planches":
                if wizard.side_n or wizard.side_s or wizard.side_w or wizard.side_e:
                    sides=(wizard.side_n.code if wizard.side_n else '0') \
                        +(wizard.side_s.code if wizard.side_s else '0') \
                        +(wizard.side_w.code if wizard.side_w else '0') \
                        +(wizard.side_e.code if wizard.side_e else '0')
                else:
                    sides='0000'
                edge_length=self._get_edge_length(cr,uid,input_length,input_width,sides)
                
                input_values=self._get_attributes_lines_values_ids(cr, uid, wizard.product_tmpl_id.id,str(input_length),str(input_width),sides)
            if wizard.product_tmpl_id.category_measurement in ['portes','tablettes','ensembles_monter']:
                input_values=self._get_attributes_lines_values_ids(cr, uid, wizard.product_tmpl_id.id,str(input_length),str(input_width))
            value_ids.extend(input_values)
            
            if wizard.attribute_line_ids:
                values=[line.attribute_value_id.id for line in  wizard.attribute_line_ids]
                value_ids.extend(values)

            
            current_variant_value_ids=','.join([str(line) for line in value_ids])
            
            result=self.pool.get('product.product').get_result(cr,uid,wizard.product_tmpl_id.id,values,surface,edge_length,context=context)
            final_cost=result.get('cost',False)
            lst_price=result.get('price',False)
            product_id=self._exist_product(cr, uid,wizard.product_tmpl_id.id,value_ids,context=context)
            if product_id==0:
                product_id= self.pool.get('sale.order')._create_product(cr,uid,wizard.product_tmpl_id.id,current_variant_value_ids,surface,final_cost,lst_price,wizard.purchase_ok, wizard.sale_ok)
            if context.get('from_sale_order',False) and product_id :
                so_obj=self.pool.get('sale.order')
                so_values=so_obj._cart_update(cr,uid,[context.get('active_id')],final_sides='',length=0, width=0, final_price=0,final_surface=0,lst_price=0,
                         template_id=0,current_variant_value_ids=None,
                                       product_id=product_id,line_id=None,add_qty=wizard.product_uom_qty,set_qty=0,purchase_ok=wizard.purchase_ok, sale_ok=wizard.sale_ok,context=context)
            return True
        return False
    
     
    def _exist_product(self, cr, uid, product_tmpl_id, value_ids, context=None):
        product_id=0
        product_obj = self.pool.get("product.product")
        template_products=product_obj.search(cr, uid,[('product_tmpl_id','=',product_tmpl_id)] ,  context=context)
        for product in template_products:
            values=[v.id for v in product_obj.browse(cr, uid, product, context=context).attribute_value_ids]
            if set(values)==set(value_ids):
                product_id=product
                break
        return product_id
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
