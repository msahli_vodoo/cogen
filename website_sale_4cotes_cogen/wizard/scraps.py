# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-2015 Haute Voltige consultants (Wided BEN OTHMEN) 
#                         (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.osv import fields, osv
import datetime
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import math
import logging
logger= logging.getLogger('4cotes module')
class scraps_details(osv.osv_memory):
    _name ='scraps.details'
    _rec_name='product_id'
    
    def _get_template_domain(self, cr, uid, context=None):
        if not  context.get('production_id', False):
            return False
        production=self.pool.get('mrp.production').browse(cr, uid, context.get('production_id', False),context)
        return [(6, 0, [move.product_id.product_tmpl_id.id for move in production.move_lines])]
     
    _columns = {
        'quantity':fields.float('Quantity',required=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'product_tmpl_id': fields.many2one('product.template', 'Product Template'),
        'template_ids': fields.many2many('product.template', id1='details_id', id2='template_id', string='Template Domain'),
        'length_input':fields.float('Length',required=True),
        'width_input':fields.float('Width',required=True),
        'uom_id': fields.many2one('product.uom', 'Product Uom',required=True),
        'location_src_id': fields.many2one('stock.location', 'Source location',required=True),
        'location_dest_id': fields.many2one('stock.location', 'Destination location'), 
        'scraps_id': fields.many2one('scraps.scraps', 'Scraps'),
        'measure': fields.selection([('imperial', 'Imperial (inch)'),('metric','Metric (mm) ')],'Measure',required=True),
        
    }
    _defaults = {
        'quantity':1.0,
        'length_input':1,
        'width_input':1,
        'measure':'imperial',
        'template_ids':_get_template_domain,
    }
    
    def onchange_product_tmpl_id(self, cr, uid,ids, product_tmpl_id,context=None):
        if product_tmpl_id and context.get('production_id', False):
            production=self.pool.get('mrp.production').browse(cr, uid, context.get('production_id', False),context)
            for move in production.move_lines:
                logger.warn(' onchange_product_tmpl_id')
                if move.product_id.product_tmpl_id.id==product_tmpl_id:
                    return {'value':{'uom_id':self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'product_uom_borad')[1],
                                     'location_src_id':move.location_dest_id.id,
                                     'location_dest_id':move.location_id.id,
                                     'product_id':move.product_id.id
                                    }
                            }
        return {'value':{'uom_id':self.pool.get('ir.model.data').get_object_reference(cr, uid, 'website_sale_4cotes_cogen', 'product_uom_borad')[1]}}
    
    def _check_quantity(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.quantity<=0.0 :
                    return False
        return True

    def _check_length_input(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.measure=='metric':
                if wizard.length_input< wizard.product_tmpl_id.min_length*25.4 or (wizard.length_input> wizard.product_tmpl_id.max_length +1) *25.4 :
                    return False
            else:
                if wizard.length_input< wizard.product_tmpl_id.min_length or wizard.length_input> wizard.product_tmpl_id.max_length+1:
                    return False
        return True
    
    def _check_width_input(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.measure=='metric':
                if wizard.width_input< wizard.product_tmpl_id.min_width*25.4 or wizard.width_input> wizard.product_tmpl_id.max_width*25.4 :
                    return False
            else:
                if wizard.width_input< wizard.product_tmpl_id.min_width or wizard.width_input> wizard.product_tmpl_id.max_width :
                    return False
        return True
    _constraints = [
        (_check_quantity, 'Please enter a valid quantity !!', ['quantity']),
        (_check_length_input, 'Please enter a valid length !!', ['length_input']),
        (_check_width_input, 'Please enter a valid width !!', ['width_input']),
    ]
    
class scraps_scraps(osv.osv_memory):
    _name ='scraps.scraps'
    _rec_name='production_id'
    
    def _get_production_id(self, cr, uid, context=None):
        return context.get('active_id', False)  
    
    def _get_details_ids(self, cr, uid,context=None):
        details_ids=[]
        if context.get('active_id', False) :
            production=self.pool.get('mrp.production').browse(cr, uid,context.get('active_id', False),context=context)
            for move in production.move_lines:
                details_ids.append((0, 0,  {
                                            'quantity':move.product_uom_qty,
                                            'product_tmpl_id':move.product_id.product_tmpl_id.id,
                                            'product_id':move.product_id.id,
                                            'template_ids':[(6, 0, [m.product_id.product_tmpl_id.id for m in production.move_lines])],
                                            'uom_id':move.product_uom.id,
                                            'location_src_id':move.location_dest_id.id,
                                            'location_dest_id':move.location_id.id,
                                            'measure':'imperial',
                                            'length_input':1,
                                            'width_input':1
                                            }
                                   ))
        return details_ids
    _columns = {   
        'production_id':fields.many2one('mrp.production', 'production',required=True,ondelete='cascade'),
        'details_ids': fields.one2many('scraps.details', 'scraps_id',  'details'),
    }
    _defaults = {
        'details_ids':_get_details_ids,
        'production_id':_get_production_id,
    }
        
    def get_type(self, cr, uid, val):
        if  val - math.floor(val)==0:
            res=int(val) 
        else:
            res=float(val) 
        return res        
        
    def _get_input_details(self,cr, uid,length,width,measure):
        precision = self.pool.get('decimal.precision').precision_get(cr, uid, 'Product Unit of Measure pi2')
        if measure=='imperial':
            res={'input_length':self.get_type(cr, uid,length),
                 'input_width':self.get_type(cr, uid, width),
                  'surface':round((length * width)/144,precision)
                }
        else:
            res={'input_length':round(self.get_type(cr, uid,length)/25.4,precision),
                 'input_width':round(self.get_type(cr, uid, width)/25.4,precision),
                  'surface':round((length * width)/(144*645.16),precision)
                }
            
        return res    
    
    def _get_edge_length(self,cr,uid,input_length,input_width,sides,context=None):
        edge_length=0.0
        if sides:
            if sides[0]=='1':
                edge_length+=input_length
            if sides[1]=='1':
                edge_length+=input_length
            if sides[2]=='1':
                edge_length+=input_width
            if sides[3]=='1':
                edge_length+=input_width
        
        return edge_length/12    
    
    def return_stock(self,cr,uid,ids,context=None):
        move_obj = self.pool.get('stock.move')
        product_obj = self.pool.get('product.product')
        production_obj=self.pool.get('mrp.production')
        
        wizard=self.browse(cr,uid,ids[0],context=None)
        if wizard.production_id:
            for detail in wizard.details_ids:
                res=self._get_input_details(cr, uid,detail.length_input,detail.width_input,detail.measure)
                input_length= res['input_length']
                input_width= res['input_width']
                surface= res['surface']
                value_obj=self.pool.get('product.attribute.value')
                value_ids=[value.id for value in detail.product_id.attribute_value_ids if  value.attribute_id.type not in ['input_length','input_width']]
                value_ids.extend(self.get_attributes_lines_values_ids(cr,uid,detail.product_id.product_tmpl_id.id,str(input_length),str(input_width)))
               
                values=[value.id for value in detail.product_id.attribute_value_ids if  value.attribute_id.type not in ['input_length','input_width','sides']]
                side_id=[value.id for value in detail.product_id.attribute_value_ids if  value.attribute_id.type =='sides']
               
                edge_length=self._get_edge_length(cr,uid,input_length,input_width,value_obj.browse(cr, uid,side_id, context=context).name)                                      
                context['from_scraps']=True
                result=self.pool.get('product.product').get_result(cr,uid,detail.product_id.product_tmpl_id.id,values,surface,edge_length,context=context)
                final_cost=result.get('cost',False)
                lst_price=result.get('price',False)
                
#                 Scraps with sale_ok= False and purchase_ok=False
                product_id=self._exist_product(cr, uid,detail.product_id.product_tmpl_id.id,value_ids,context=context)
#                 logger.info('--- product_id ----%s',product_id)
                current_variant_value_ids=','.join([str(line) for line in value_ids])
                if product_id==0:
                    product_id= self.pool.get('sale.order')._create_product(cr,uid,detail.product_id.product_tmpl_id.id,current_variant_value_ids,surface,final_cost,lst_price,False, False)
                
                  
                
                production=production_obj.browse(cr, uid, wizard.production_id.id, context=context)
                source_location_id = production.product_id.property_stock_production.id
                destination_location_id = production.location_src_id.id
                
                prev_move= False
                if production.bom_id.routing_id and production.bom_id.routing_id.location_id and production.bom_id.routing_id.location_id.id != source_location_id:
                    source_location_id = production.bom_id.routing_id.location_id.id
                    prev_move = True
#                 logger.info('--- source_location_id ----%s',source_location_id)
#                 logger.info('--- destination_location_id ----%s',destination_location_id)
                product=self.pool.get('product.product').browse(cr, uid,product_id,context=context)
                move_id=self.pool.get('stock.move').create(cr, uid, {
                    'name': product.name,
                    'date': datetime.datetime.now(),
                    'product_id': product_id,
                    'product_uom_qty': detail['quantity'],
                    'product_uom': detail['uom_id'].id,
                    'product_uos_qty': product.uos_id and product.uos_qty or False,
                    'product_uos': product.uos_id or False,
                    'location_id': source_location_id,
                    'location_dest_id': destination_location_id,
                    'company_id': production.company_id.id,
                    'procure_method': prev_move and 'make_to_stock' or production_obj._get_raw_material_procure_method(cr, uid, product_obj.browse(cr, uid,product_id,context=context ), context=context), #Make_to_stock avoids creating procurement
#                     'raw_material_production_id': production.id,
                    'production_id': production.id,
                    #this saves us a browse in create()
                    'price_unit': product.standard_price,
                    'origin': production.name,
                    'warehouse_id': self.pool.get('stock.location').get_warehouse(cr, uid, production.location_src_id, context=context),
                    'group_id': production.move_prod_id.group_id.id,
                }, context=context)
                
                move_obj.action_done(cr, uid, move_id)
        return True
    
    
    def get_attributes_lines_values_ids(self,cr,uid,template_id,length,width):
        attr_line_obj = self.pool.get('product.attribute.line')
        types_attributes=['input_length','input_width']
        result=[]
        for type_att in types_attributes:
            lines_ids=attr_line_obj.get_attribute_line_type(cr, uid,template_id,type_att,context=None)
            if lines_ids:
                if type_att=='input_length':
                    result.append(attr_line_obj.get_attribute_line_values_id(cr, uid,lines_ids[0],length,context=None))
                if type_att=='input_width':
                    result.append(attr_line_obj.get_attribute_line_values_id(cr, uid,lines_ids[0],width,context=None))
        return result
    
    def _exist_product(self, cr, uid, product_tmpl_id, value_ids, context=None):
        product_id=0
        product_obj = self.pool.get("product.product")
        template_products=product_obj.search(cr, uid,[('product_tmpl_id','=',product_tmpl_id)] ,  context=context)
        for product in template_products:
            values=[v.id for v in product_obj.browse(cr, uid, product, context=context).attribute_value_ids]
            if set(values)==set(value_ids):
                product_id=product
                break
        return product_id
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: