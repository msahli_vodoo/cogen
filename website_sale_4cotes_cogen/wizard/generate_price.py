# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants (Wided BEN OTHMEN) 
#    (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import logging
logger= logging.getLogger('4cotes module')

class attribute_price(osv.osv_memory):
    _name ='attribute.price'
    _rec_name = 'attribute_value_id'
    
#     def _product_public_price(self, cr, uid, ids, name, arg, context=None):
#         res = dict.fromkeys(ids, 0.0)
#         for attribute_price in self.browse(cr, uid, ids, context=context):
#             logger.warning('edge_id %s ,',attribute_price.edge_id)
#             if attribute_price.edge_id:
#                 res[attribute_price.id] = attribute_price.edge_id.list_price
#             else:
#                 res[attribute_price.id] = 0.0
#                 
#         return res
    
#     def _set_edge_new_public_price(self, cr, uid, id, name, value, args, context=None):
#         for attribute_price in self.browse(cr, uid, id, context=context):
#             if attribute_price.edge_id:
#                 self.pool.get('product.template').write(cr, uid,attribute_price.edge_id.id,{'list_price':attribute_price.price_extra_pi} )
#         return True
    
    _columns = {
        'attribute_value_id': fields.many2one('product.attribute.value', 'Value',required=True),
        'price_extra': fields.float('Price Extra / pi(2)', digits_compute=dp.get_precision('Product Price')),
        'price_extra_pi': fields.float('Price Extra / pi', digits_compute=dp.get_precision('Product Price')),
#         'price_extra_pi': fields.function(_product_public_price,   fnct_inv=_set_edge_new_public_price,type='float', string='Price Extra / pi', digits_compute=dp.get_precision('Product Price')),
        'generate_price_id': fields.many2one('generate.price', 'Price'),
        'cost_extra': fields.float('Cost Extra / pi(2)', digits_compute=dp.get_precision('Product Price')),
#         'edge_id': fields.many2one('product.product', 'Value',domain="[('family_id.category_measurement', '=', 'edge')]",required=False),
        'edge_id': fields.many2one('product.template', 'Edge',required=False),
    }
    _defaults = {
        'price_extra': 0.0,
        'price_extra_pi':0.0,
        'cost_extra': 0.0,
    }
    def onchange_edge_id(self, cr, uid, ids,edge_id,context=None):
        if edge_id:
                return {'value': {'price_extra_pi': self.pool.get('product.template').browse(cr, uid, edge_id, context).list_price}}
        return {}
      
class generate_price(osv.osv_memory):
    _name ='generate.price'
    _rec_name = 'product_tmpl_id'
    
    def _get_product_tmpl_id(self, cr, uid, context=None):
        return context.get('active_id', False)  
    
    def _get_attribut_price_ids(self, cr, uid, context=None):
        tmpl_id=context.get('active_ids', [])
        prd_attribut_price_obj = self.pool.get("product.attribute.price")
        att_price_ids=[]
        if tmpl_id:
            attribute_line_obj=self.pool.get('product.attribute.line')
            attribute_lines = attribute_line_obj.search(cr, uid,[('product_tmpl_id','in',tmpl_id),('attribute_id.type','not in',['input_width','input_length','hidden','sides','only_one','thickness','model_mop','color_texture'])], context=context)
            for line in attribute_lines:
                for v in  attribute_line_obj.browse(cr, uid, line, context=context).value_ids:
                    if prd_attribut_price_obj.search_count(cr,uid,[('product_tmpl_id','in',tmpl_id),('value_id','=',v.id)])>0:
                        attribut_price=prd_attribut_price_obj.browse(cr,uid,prd_attribut_price_obj.search(cr,uid,[('product_tmpl_id','in',tmpl_id),('value_id','=',v.id)],limit=1)[0])
                        price_extra=attribut_price.price_extra
#                         price_extra_pi=attribut_price.price_extra_pi
                        price_extra_pi=attribut_price.edge_id.list_price
                        cost_extra=attribut_price.cost_extra
                        edge_id=attribut_price.edge_id
                    else:
                        cost_extra=price_extra=0.0
                        edge_id=v.edge_id
                        price_extra_pi=v.edge_id.list_price
                        
                    att_price_ids.append((0,0,{'edge_id':edge_id,'attribute_value_id':v.id,'price_extra':price_extra,'price_extra_pi':price_extra_pi,'cost_extra':cost_extra}))
        return att_price_ids or False
    
    _columns = {
        'attribut_price_ids': fields.one2many('attribute.price', 'generate_price_id',  'Attribut Prices',required=True, ondelete='restrict'),
        'product_tmpl_id':fields.many2one('product.template', 'Template',readonly=True,required=True,ondelete="cascade"),
        'material_id':fields.many2one('hvc.product.material.type', 'Material type'),
#         'material_id': fields.related('product_tmpl_id', 'material_type_id', type='many2one', relation='hvc.product.material.type', string='Material type')
    }
    
    _defaults = {
        'product_tmpl_id':_get_product_tmpl_id,
        'attribut_price_ids': _get_attribut_price_ids,
    }
    def onchange_product_tmpl_id(self, cr, uid, ids,product_tmpl_id,context=None):
        if product_tmpl_id:
                return {'value': {'material_id': self.pool.get('product.template').browse(cr, uid, product_tmpl_id, context).material_type_id.id}}
        return {}
    
    def generate(self, cr, uid, ids, context=None):
        prd_attribut_price_obj = self.pool.get("product.attribute.price")
        attribute_price_obj=self.pool.get('attribute.price')
        self_obj=self.browse(cr, uid, ids, context=context)
        if self_obj: 
            for att_price in self_obj.attribut_price_ids:
                vals={
                     'product_tmpl_id':self_obj.product_tmpl_id.id,
                     'value_id':attribute_price_obj.browse(cr, uid, att_price.id,context=context).attribute_value_id.id,
                     'price_extra':attribute_price_obj.browse(cr, uid, att_price.id,context=context).price_extra,
                     'cost_extra':attribute_price_obj.browse(cr, uid, att_price.id,context=context).cost_extra,
                     'price_extra_pi':attribute_price_obj.browse(cr, uid, att_price.id,context=context).price_extra_pi,
                     'edge_id':attribute_price_obj.browse(cr, uid, att_price.id,context=context).edge_id.id,
                     }
                existe_id=prd_attribut_price_obj.search(cr,uid,[('product_tmpl_id','=',vals['product_tmpl_id']),('value_id','=',vals['value_id'])])
                if not len(existe_id):
                    prd_attribut_price_obj.create(cr,uid, vals,context=context)
                else:
                    prd_attribut_price_obj.write(cr,uid,existe_id[0],vals,context=context)
             
        return True
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: