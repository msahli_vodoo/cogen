# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-2015 Haute Voltige consultants (Wided BEN OTHMEN) 
#                         (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import math
import logging
logger= logging.getLogger('4cotes module')
class consume_prod(osv.osv_memory):
    _name ='consume.prod'
    
    def _get_production_id(self, cr, uid, context=None):
        return context.get('active_id', False)  

    def _get_details_ids(self, cr, uid,context=None):
        details_ids=[]
        procurement_obj=self.pool.get('procurement.order')
        if context.get('active_id', False) :
            production=self.pool.get('mrp.production').browse(cr, uid,context.get('active_id', False),context=context)
            location_src_id=procurement_obj._get_location_src_id(cr, uid,context=context)
            details_ids=procurement_obj._get_move_line_propositions(cr, uid,production.product_id.id,location_src_id,production.product_qty,context=context)
        return details_ids

    _columns = {   
        'production_id':fields.many2one('mrp.production', 'production',ondelete='cascade'),
        'details_ids': fields.one2many('consume.prod.details', 'consume_prod_id',  'details'),
    }
    _defaults = {
        'details_ids':_get_details_ids,
        'production_id':_get_production_id,
    }
        
    def consume(self,cr,uid,ids,context=None):
        stock_move = self.pool.get('stock.move')
#         stock_move_kanban = self.pool.get('stock.move.kanban')
        product_obj = self.pool.get('product.product')
        production_obj=self.pool.get('mrp.production')
        prod_line_obj = self.pool.get('mrp.production.product.line')
        
        wizard=self.browse(cr,uid,ids[0],context=None)
        if wizard.production_id:
            production=production_obj.browse(cr, uid, wizard.production_id.id, context=context)
            source_location_id = production.product_id.property_stock_production.id
            destination_location_id = production.location_src_id.id
            for detail in wizard.details_ids:
                if detail.use_it:
                    prev_move= False
                    if production.bom_id.routing_id and production.bom_id.routing_id.location_id and production.bom_id.routing_id.location_id.id != source_location_id:
                        source_location_id = production.bom_id.routing_id.location_id.id
                        prev_move = True
                    stock_move.create(cr, uid, {
                        'name': detail.product_id.name,
                        'date': production.date_planned,
                        'product_id': detail.product_id.id,
                        'product_uom_qty': detail.quantity,
                        'product_uom': detail.uom_id.id,
                        'product_uos_qty': detail.product_id.uos_id and detail.product_id.uos_qty or False,
                        'product_uos': detail.product_id.uos_id or False,
                        'location_id': destination_location_id,
                        'location_dest_id': source_location_id,
                        'company_id': production.company_id.id,
                        'procure_method': prev_move and 'make_to_stock' or production_obj._get_raw_material_procure_method(cr, uid, product_obj.browse(cr, uid,detail.product_id.id,context=context ), context=context), #Make_to_stock avoids creating procurement
                        'raw_material_production_id': production.id,
                        #this saves us a browse in create()
                        'price_unit': detail.product_id.standard_price,
                        'origin': production.name,
                        'warehouse_id': self.pool.get('stock.location').get_warehouse(cr, uid, production.location_src_id, context=context),
                        'group_id': production.move_prod_id.group_id.id,
                    }, context=context)
                    line = {
                        'name': production.name,
                        'product_id': detail.product_id.id,
                        'product_qty':detail.quantity,
                        'product_uom': detail.uom_id.id,
                        'product_uos_qty': detail.product_id.uos_id and detail.product_id.uos_qty or False,
                        'product_uos': detail.product_id.uos_id or False,
                        'production_id': production.id,
                    }
                    prod_line_obj.create(cr, uid, line)
        return True

class consume_prod_details(osv.osv_memory):
    _name ='consume.prod.details'
    
    def _get_product_domain(self, cr, uid, context=None):
        if not  context.get('production_id', False):
            return False
        procurement_obj=self.pool.get('procurement.order')
        production=self.pool.get('mrp.production').browse(cr, uid, context.get('production_id', False),context)
        location_src_id=procurement_obj._get_location_src_id(cr, uid,context=context)
        details_ids=procurement_obj._get_move_line_propositions(cr, uid,production.product_id.id,location_src_id,production.product_qty,context=context)
        product_domain_ids=[]
        for detail in details_ids:
            product_domain_ids.append(detail[2]['product_id'])
        return [(6, 0, product_domain_ids)]
    
    _columns = {
        'use_it':fields.boolean('Use It'),
        'consume_prod_id': fields.many2one('consume.prod', 'consume product'),
        'quantity':fields.float('Quantity',required=True),
        'product_id': fields.many2one('product.product', 'Product'),
        'product_ids': fields.many2many('product.product', id1='details_id', id2='product_id', string='Product Domain'),
        'code_product':fields.char('code_product',readonly=True),
        'length_input':fields.float('Length', readonly=True),
        'width_input':fields.float('Width',readonly=True),
        'qty_available_display':fields.float('Qty Available',readonly=True),
        'qty_available':fields.float('Qty Available'),
        'uom_id_display': fields.many2one('product.uom', 'Product Uom',readonly=True),
        'uom_id': fields.many2one('product.uom', 'Product Uom'),
        'location_src_id': fields.many2one('stock.location', 'Raw Materials Location'),
    }
    _defaults = {
        'quantity':1.0,
        'product_ids':_get_product_domain,
    }
    
    def onchange_product_id(self, cr, uid,ids, product_id,context=None):
        if product_id and context.get('production_id', False):
            production=self.pool.get('mrp.production').browse(cr, uid, context.get('production_id', False),context)
            procurement_obj=self.pool.get('procurement.order')
            location_src_id=procurement_obj._get_location_src_id(cr, uid,context=context)
            details_ids=procurement_obj._get_move_line_propositions(cr, uid,production.product_id.id,location_src_id,production.product_qty,context=context)
            for detail in details_ids:
                if detail[2]['product_id']==product_id:
                    allowed_qty=0.0
                    if detail[2]['quantity']<detail[2]['qty_available']:
                        allowed_qty=detail[2]['quantity']
                    else:
                        allowed_qty=detail[2]['qty_available']
                    return {'value':{#'quantity':detail[2]['quantity'],
                                     'quantity':allowed_qty,
                                     'length_input':detail[2]['length_input'],
                                     'width_input':detail[2]['width_input'],
                                     'uom_id':detail[2]['uom_id'],
                                     'uom_id_display':detail[2]['uom_id'],
                                     'qty_available':detail[2]['qty_available'],
                                     'qty_available_display':detail[2]['qty_available'],
                                     'location_src_id':detail[2]['location_src_id'],
                                     'code_product':detail[2]['code_product'],
                                    }
                            }
        return {}
 
    def _check_quantity(self, cr, uid, ids, context=None):
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.quantity<=0.0 or wizard.quantity > wizard.qty_available:
                    return False
        return True
     
    _constraints = [
        (_check_quantity, 'Please enter a valid quantity !!', ['quantity']),
    ]    
    
    



# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: