# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants (Wided BEN OTHMEN) 
#    (http://hautevoltige.net/).
#                            
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time
from openerp.tools.translate import _
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp
import math
import logging
logger= logging.getLogger('4cotes module')
class value_details(osv.osv_memory):
    _name ='value.details'

    def _check_measure(self, cr, uid, ids, context=None):
        for value_details in self.browse(cr, uid, ids, context=context):
            if value_details.max_width <=0 or value_details.min_width<=0  or value_details.max_length <=0 or value_details.min_length <=0 :
                return False
            if value_details.max_width < value_details.min_width  or value_details.max_length < value_details.min_length:
                return False
            return True
        return True
    
    def _check_attribute_value_ids(self, cr, uid, ids, context=None):
        attribut_ids=[]
        for value_details in self.browse(cr, uid, ids, context=context):
            if value_details.attribute_value_ids:
                for v in  value_details.attribute_value_ids:
                    if v.attribute_id.id in attribut_ids:
                        return False
                    else:
                        attribut_ids.append(v.attribute_id.id)
                return True
            return True
        return True
    
    def _check_priority(self, cr, uid, ids, context=None):
        for value_details in self.browse(cr, uid, ids, context=context):
            if value_details.priority <=0 :
                return False
            product_tmpl_id=value_details.generate_exception_id.product_tmpl_id.id
#             logger.warning('product_tmpl_id%s:',product_tmpl_id)
            if product_tmpl_id:
                if value_details.priority < self.get_last_priority(cr, uid, ids, product_tmpl_id, context):
                    return False
        return True
    
    def get_last_priority(self, cr, uid,ids,product_tmpl_id=False,context=None):
        priority=1
        if product_tmpl_id:
            cr.execute("SELECT max(priority) FROM template_measure_exception WHERE product_tmpl_id=%s",(product_tmpl_id,))
            try: 
                db_result = cr.fetchall()
                if db_result[0][0]!=None:
                    priority=db_result[0][0] + 1
            except:
                raise osv.except_osv(_('Error !!'), _('Your request was not processed because an unexpected exception has been produced.Please try later !!'))
     
        return priority
      
    def on_change_attribute_value_ids(self, cr, uid,ids,product_tmpl_id=False,context=None):
        return {'value':{'priority':self.get_last_priority(cr, uid, ids,product_tmpl_id, context=context) }}
                 
    _columns = {
        'generate_exception_id': fields.many2one('generate.exception', 'Generator'),
        'attribute_value_ids': fields.many2many('product.attribute.value', id1='detail_id', id2='value_id', string='Values'),
        'max_width': fields.float('Max width(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum width in inches."),
        'min_width': fields.float('Min width(inches)',readonly=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The minimum width in inches."),
        'max_length': fields.float('Max length(inches)',required=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'), help="The maximum length in inches."),
        'min_length': fields.float('Min length(inches)',readonly=True, digits_compute=dp.get_precision('Product Unit of Measure pi2'),help="The minimum length in inches."),
        'priority': fields.integer('Priority',required=True,help="Gives the exception priority."),
    }
    _defaults={
        'max_width':1,
        'min_width':1,
        'max_length':1,
        'min_length':1,
    }
  
    _constraints = [
        (_check_measure, 'Please enter a valid Min/Max length/width number !!', ['max_width','min_width','max_length','min_length']),
        (_check_attribute_value_ids, 'Please do not duplicate any value attributs !!', ['attribute_value_ids']),
        (_check_priority, 'Please enter a valid priority !!', ['priority']),
    ]
    
class generate_exception(osv.osv_memory):
    _name ='generate.exception'
    _rec_name='product_tmpl_id'
    
    def _get_product_tmpl_id(self, cr, uid, context=None):
        return context.get('active_id', False)  
    
    def _check_values_ids(self, cr, uid, ids, context=None):
        value_details=[]
        priorities=[]
        for wizard in self.browse(cr, uid, ids, context=context):
            if wizard.values_ids:
                for v in  wizard.values_ids:
                    if v.attribute_value_ids.ids in value_details:
                        return False
                    else:
                        value_details.append(v.attribute_value_ids.ids)
                    
                    if v.priority in priorities:
                        return False
                    else:
                        priorities.append(v.priority)
                return True
            return True
        return True
    
    _columns = {   
        'values_ids': fields.one2many('value.details', 'generate_exception_id',  'Values',required=True),
        'product_tmpl_id':fields.many2one('product.template', 'Template',required=True),
        'category_measurement': fields.char('Category Measurement'),
        'value_ids': fields.many2many('product.attribute.value', id1='exception_id', id2='v_id', string='values Domain'),
        }
    _defaults = {
        'product_tmpl_id':_get_product_tmpl_id,
    }
    _constraints = [
        (_check_values_ids, 'Please do not duplicate any values line!! or any priority', ['values_ids']),
    ]
    def get_value_ids(self, cr, uid,product_tmpl_id,context=None):
        values=[]
        template=self.pool.get('product.template').browse(cr,uid,product_tmpl_id,context=None)
        if template.attribute_line_ids:
            for line in template.attribute_line_ids:
                if line.attribute_id.type not in ['input_length','input_width','sides']:
                    for i in line.value_ids.ids:
                        values.append(i)
        return values
    
    def onchange_product_tmpl_id(self, cr, uid,ids,product_tmpl_id=False,context=None):
        if product_tmpl_id:
            template=self.pool.get('product.template').browse(cr, uid,product_tmpl_id,context=context)
            sides=[side.id for side in template.material_type_id.sides_ids]
            return {'value':
                           {'value_ids':self.get_value_ids(cr, uid, product_tmpl_id, context),
                            'category_measurement':template.category_measurement,
                           },
                   }
        return {}
        
    def create_exception(self,cr,uid,ids,context=None):
        wizard=self.browse(cr,uid,ids[0],context=None)
        exception_vals={}
        if wizard.product_tmpl_id and  wizard.values_ids :
            for exception in wizard.values_ids:
                if exception.attribute_value_ids.ids:
                    exception_vals={'product_tmpl_id' : wizard.product_tmpl_id.id,
                          'attribute_value_ids':[(6, 0, exception.attribute_value_ids.ids)],
                          'max_width':exception.max_width,
                          'min_width':exception.min_width,
                          'max_length':exception.max_length,
                          'min_length':exception.min_length,
                          'priority':exception.priority,
                          }
                    exception_id=self.pool.get('product.template')._exist_exception(cr, uid,exception_vals,context=context)
                    if exception_id==0:
                        exception_id= self.pool.get('template.measure.exception').create(cr,uid,exception_vals,context=context)
        return True
    
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: