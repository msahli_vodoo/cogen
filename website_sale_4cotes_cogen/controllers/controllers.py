# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 Haute Voltige consultants (Hafedh RAHMANI) (http://hautevoltige.net/).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import logging
logger= logging.getLogger('4cotes module:')

import werkzeug
from openerp.osv import osv, orm
from openerp import SUPERUSER_ID
from openerp import http
from openerp.http import request
from openerp.tools.translate import _
from openerp.addons.website.models.website import slug
import openerp.addons.website_sale.controllers.main
from openerp.addons.website_sale.controllers.main import *
import openerp.addons.web.controllers.main
from openerp.addons.web.controllers.main import *
import itertools
import openerp.addons.website_crm.controllers.main
# from recaptcha.client import captcha

PPG = 16 # Products Per Page
PPR = 4  # Products Per Row


class website_projets(http.Controller):
    
#     abir : clear line product


    @http.route(['/shop/payment_pickup'], type='http', auth="public", website=True)
    def payment(self, **post):
        print "payment_pickup"
        """ Payment step. This page proposes several payment means based on available
        payment.acquirer. State at this point :

         - a draft sale order with lines; otherwise, clean context / session and
           back to the shop
         - no transaction in context / session, or only a draft one, if the customer
           did go to a payment.acquirer website but closed the tab without
           paying / canceling
        """
        cr, uid, context = request.cr, request.uid, request.context
        payment_obj = request.registry.get('payment.acquirer')
        sale_order_obj = request.registry.get('sale.order')

        order = request.website.sale_get_order(context=context)
        
      
        shipping_partner_id = False
        if order:
            if order.partner_shipping_id.id:
                shipping_partner_id = order.partner_shipping_id.id
            else:
                shipping_partner_id = order.partner_invoice_id.id

        values = {
            'order': request.registry['sale.order'].browse(cr, SUPERUSER_ID, order.id, context=context)
        }
        values['errors'] = sale_order_obj._get_errors(cr, uid, order, context=context)
        values.update(sale_order_obj._get_website_data(cr, uid, order, context))

        # fetch all registered payment means
        # if tx:
        #     acquirer_ids = [tx.acquirer_id.id]
        # else:
        if not values['errors']:
            acquirer_ids = payment_obj.search(cr, SUPERUSER_ID, [('website_published', '=', True), ('company_id', '=', order.company_id.id)], context=context)
            values['acquirers'] = list(payment_obj.browse(cr, uid, acquirer_ids, context=context))
#             render_ctx = dict(context, submit_class='btn btn-primary', submit_txt=_('Pay Now'))
            for acquirer in values['acquirers']:
                render_ctx = dict(context, submit_class='btn secondary small', submit_txt=acquirer.name)
                acquirer.button = payment_obj.render(
                    cr, SUPERUSER_ID, acquirer.id,
                    order.name,
                    order.amount_total,
                    order.pricelist_id.currency_id.id,
                    partner_id=shipping_partner_id,
                    tx_values={
                        'return_url': '/shop/payment/validate',
                    },
                    context=render_ctx)

        return request.website.render("website_sale_4cotes_cogen.payment_pickup", values)


    @http.route(['/shop/clear_cart'], type='json', auth="public", website=True)
    def clear_cart(self, line_id):
      
        order = request.website.sale_get_order()
        if order:
            exist_product=False
            for line in order.website_order_line:
               
                if int(line.id)==int(line_id):
                    line.unlink()
                elif line.product_id.code_product!='LV':
                    exist_product=True
                    
            
            '''if remove all product'''
            if not exist_product: 
                for line in order.website_order_line:
               
                    if line.product_id.code_product=='LV':
                        line.unlink()
                       
                    
                    
            
    
#     abir      
    
    
    @http.route(['/shop/cart/update'], type='http', auth="public", methods=['POST'], website=True)
    def cart_update(self, product_id, add_qty=1, set_qty=0, **kw):
        cr, uid, context = request.cr, request.uid, request.context
        request.website.sale_get_order(force_create=1)._cart_update(product_id=int(product_id), add_qty=float(add_qty), set_qty=float(set_qty))
        return request.redirect("/shop/cart")
    
    
    
    
    @http.route(['/projets'], type='http', auth="public", website=True)
    def projets(self,**post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        sale_order_obj = pool['sale.order']
        res_user_obj = pool['res.users']
        user=res_user_obj.browse(cr, uid, uid, context=context)
        if user.login=="public":
            return request.redirect('/web/login')
        sale_order_ids = sale_order_obj.search(cr, uid,[('partner_id','=',res_user_obj.browse(cr,uid,uid).partner_id.id)], context=context)
        list_sale_order=sale_order_obj.browse(cr, uid,sale_order_ids, context=context)   
        return request.website.render("website_sale_4cotes_cogen.hvc_projets", {'list_sale_order':list_sale_order})  
    
    
#     @http.route(['/page/cantact2'], type='http', auth="public", website=True)
#     def contact2(self,**post):
#         cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
#         sale_order_obj = pool['sale.order']
#         res_user_obj = pool['res.users']
#         user=res_user_obj.browse(cr, uid, uid, context=context)
#         if user.login=="public":
#             return request.redirect('/web/login')
#         sale_order_ids = sale_order_obj.search(cr, uid,[('partner_id','=',res_user_obj.browse(cr,uid,uid).partner_id.id)], context=context)
#         list_sale_order=sale_order_obj.browse(cr, uid,sale_order_ids, context=context)   
#         return request.website.render("website_sale_4cotes_cogen.hvc_contact2",{'list_sale_order':list_sale_order}) 
#     
    @http.route(['/how_it_work'], type='http', auth="public", website=True)
    def content(self,**post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        values={}   
        return request.website.render("website_sale_4cotes_cogen.hvc_how_it_work", values) 
    
    
    def preRenderThanks(self, values, kwargs):
        """ Allow to be overrided """
        company = request.website.company_id
        return {
            'google_map_url': self.generate_google_map_url(company.street, company.city, company.zip, company.country_id and company.country_id.name_get()[0][1] or ''),
            '_values': values,
            '_kwargs': kwargs,
        }

    def get_contactus_response(self, values, kwargs):
        values = self.preRenderThanks(values, kwargs)
        return request.website.render(kwargs.get("view_callback", "website_crm.contactus_thanks"), values)
    
#     
#     @http.route(['/crm/contactus2'], type='http', auth="public", website=True)
#     def contactus(self, **kwargs):
#         print "moez_________________"
#         def dict_to_str(title, dictvar):
#             ret = "\n\n%s" % title
#             for field in dictvar:
#                 ret += "\n%s" % field
#             return ret
# 
#         _TECHNICAL = ['show_info', 'view_from', 'view_callback']  # Only use for behavior, don't stock it
#         _BLACKLIST = ['id', 'create_uid', 'create_date', 'write_uid', 'write_date', 'user_id', 'active']  # Allow in description
#         _REQUIRED = ['name', 'contact_name', 'email_from', 'description']  # Could be improved including required from model
# 
#         post_file = []  # List of file to add to ir_attachment once we have the ID
#         post_description = []  # Info to add after the message
#         values = {}
#      
#         for field_name, field_value in kwargs.items():
#             if hasattr(field_value, 'filename'):
#                 post_file.append(field_value)
#             elif field_name in request.registry['crm.lead']._fields and field_name not in _BLACKLIST:
#                 values[field_name] = field_value
#             elif field_name not in _TECHNICAL:  # allow to add some free fields or blacklisted field like ID
#                 post_description.append("%s: %s" % (field_name, field_value))
# 
#         if "name" not in kwargs and values.get("contact_name"):  # if kwarg.name is empty, it's an error, we cannot copy the contact_name
#             values["name"] = values.get("contact_name")
#         # fields validation : Check that required field from model crm_lead exists
#         error = set(field for field in _REQUIRED if not values.get(field))
# 
#         if error:
#             values = dict(values, error=error, kwargs=kwargs.items())
#             return request.website.render(kwargs.get("view_from", "website.contactus"), values)
# 
#         values['medium_id']  = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website')
#         values['section_id'] = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'website.salesteam_website_sales')
# 
#         # description is required, so it is always already initialized
#         if post_description:
#             values['description'] += dict_to_str(_("Custom Fields: "), post_description)
# 
#         if kwargs.get("show_info"):
#             post_description = []
#             environ = request.httprequest.headers.environ
#             post_description.append("%s: %s" % ("IP", environ.get("REMOTE_ADDR")))
#             post_description.append("%s: %s" % ("USER_AGENT", environ.get("HTTP_USER_AGENT")))
#             post_description.append("%s: %s" % ("ACCEPT_LANGUAGE", environ.get("HTTP_ACCEPT_LANGUAGE")))
#             post_description.append("%s: %s" % ("REFERER", environ.get("HTTP_REFERER")))
#             values['description'] += dict_to_str(_("Environ Fields: "), post_description)
# 
#         lead_id = self.create_lead(request, dict(values, user_id=False), kwargs)
#         values.update(lead_id=lead_id)
#         if lead_id:
#             for field_value in post_file:
#                 attachment_value = {
#                     'name': field_value.filename,
#                     'res_name': field_value.filename,
#                     'res_model': 'crm.lead',
#                     'res_id': lead_id,
#                     'datas': base64.encodestring(field_value.read()),
#                     'datas_fname': field_value.filename,
#                 }
#                 request.registry['ir.attachment'].create(request.cr, SUPERUSER_ID, attachment_value, context=request.context)
# 
#         return self.get_contactus_response(values, kwargs)

class Extension_website_crm(openerp.addons.website_crm.controllers.main.contactus):
    def generate_google_map_url(self, street, city, city_zip, country_name):
        url = "http://maps.googleapis.com/maps/api/staticmap?center=%s&sensor=false&zoom=8&size=298x298" % werkzeug.url_quote_plus(
            '%s, %s %s, %s' % (street, city, city_zip, country_name)
        )
        return url
     
    def create_lead(self, request, values, kwargs):
        """ Allow to be overrided """
        cr, context = request.cr, request.context
        return request.registry['crm.lead'].create(cr, SUPERUSER_ID, values, context=dict(context, mail_create_nosubscribe=True))
     
    def preRenderThanks(self, values, kwargs):
        """ Allow to be overrided """
        company = request.website.company_id
        return {
            'google_map_url': self.generate_google_map_url(company.street, company.city, company.zip, company.country_id and company.country_id.name_get()[0][1] or ''),
            '_values': values,
            '_kwargs': kwargs,
        }
 
    def get_contactus_response(self, values, kwargs):
        values = self.preRenderThanks(values, kwargs)
        return request.website.render(kwargs.get("view_callback", "website_sale_4cotes_cogen.hvc_contactus_thanks"), values)
    
    @http.route(['/crm/contactus'], type='http', auth="public", website=True)
    def contactus(self, **kwargs):
        print "moez_________________"
        def dict_to_str(title, dictvar):
            ret = "\n\n%s" % title
            for field in dictvar:
                ret += "\n%s" % field
            return ret
 
        _TECHNICAL = ['show_info', 'view_from', 'view_callback']  # Only use for behavior, don't stock it
        _BLACKLIST = ['id', 'create_uid', 'create_date', 'write_uid', 'write_date', 'user_id', 'active']  # Allow in description
        _REQUIRED = ['name', 'contact_name', 'email_from', 'description']  # Could be improved including required from model
 
        post_file = []  # List of file to add to ir_attachment once we have the ID
        post_description = []  # Info to add after the message
        values = {}
      
        for field_name, field_value in kwargs.items():
            if hasattr(field_value, 'filename'):
                post_file.append(field_value)
            elif field_name in request.registry['crm.lead']._fields and field_name not in _BLACKLIST:
                values[field_name] = field_value
            elif field_name not in _TECHNICAL:  # allow to add some free fields or blacklisted field like ID
                post_description.append("%s: %s" % (field_name, field_value))
 
        if "name" not in kwargs and values.get("contact_name"):  # if kwarg.name is empty, it's an error, we cannot copy the contact_name
            values["name"] = values.get("contact_name")
        # fields validation : Check that required field from model crm_lead exists
        error = set(field for field in _REQUIRED if not values.get(field))
 
        if error:
            values = dict(values, error=error, kwargs=kwargs.items())
            return request.website.render(kwargs.get("view_from", "website.contactus"), values)
 
        values['medium_id']  = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website')
        values['section_id'] = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'website.salesteam_website_sales')
 
        # description is required, so it is always already initialized
        if post_description:
            values['description'] += dict_to_str(_("Custom Fields: "), post_description)
 
        if kwargs.get("show_info"):
            post_description = []
            environ = request.httprequest.headers.environ
            post_description.append("%s: %s" % ("IP", environ.get("REMOTE_ADDR")))
            post_description.append("%s: %s" % ("USER_AGENT", environ.get("HTTP_USER_AGENT")))
            post_description.append("%s: %s" % ("ACCEPT_LANGUAGE", environ.get("HTTP_ACCEPT_LANGUAGE")))
            post_description.append("%s: %s" % ("REFERER", environ.get("HTTP_REFERER")))
            values['description'] += dict_to_str(_("Environ Fields: "), post_description)
 
        lead_id = self.create_lead(request, dict(values, user_id=False), kwargs)
        values.update(lead_id=lead_id)
        if lead_id:
            for field_value in post_file:
                attachment_value = {
                    'name': field_value.filename,
                    'res_name': field_value.filename,
                    'res_model': 'crm.lead',
                    'res_id': lead_id,
                    'datas': base64.encodestring(field_value.read()),
                    'datas_fname': field_value.filename,
                }
                request.registry['ir.attachment'].create(request.cr, SUPERUSER_ID, attachment_value, context=request.context)
 
#         if request.httprequest.method == 'POST':
#             response = captcha.submit(
#                 kwargs.get("recaptcha_challenge_field"),
#                 kwargs.get("recaptcha_response_field"),
#                 '6LfVtCATAAAAAHBZ4gR6OKxXuPSwbvdzTzufPMds',
#                 request.httprequest.remote_addr,
#             )
#             if not response.is_valid:
#                 return request.redirect('/page/contactus')
        return self.get_contactus_response(values, kwargs)
     
#     @http.route(['/crm/contactus'], type='http', auth="public", website=True)
#     def contactus(self, **kwargs):
#         print "moez_________________"
#         def dict_to_str(title, dictvar):
#             ret = "\n\n%s" % title
#             for field in dictvar:
#                 ret += "\n%s" % field
#             return ret
#  
#         _TECHNICAL = ['show_info', 'view_from', 'view_callback']  # Only use for behavior, don't stock it
#         _BLACKLIST = ['id', 'create_uid', 'create_date', 'write_uid', 'write_date', 'user_id', 'active']  # Allow in description
#         _REQUIRED = ['name', 'contact_name', 'email_from', 'description']  # Could be improved including required from model
#  
#         post_file = []  # List of file to add to ir_attachment once we have the ID
#         post_description = []  # Info to add after the message
#         values = {}
#       
#         for field_name, field_value in kwargs.items():
#             if hasattr(field_value, 'filename'):
#                 post_file.append(field_value)
#             elif field_name in request.registry['crm.lead']._fields and field_name not in _BLACKLIST:
#                 values[field_name] = field_value
#             elif field_name not in _TECHNICAL:  # allow to add some free fields or blacklisted field like ID
#                 post_description.append("%s: %s" % (field_name, field_value))
#  
#         if "name" not in kwargs and values.get("contact_name"):  # if kwarg.name is empty, it's an error, we cannot copy the contact_name
#             values["name"] = values.get("contact_name")
#         # fields validation : Check that required field from model crm_lead exists
#         error = set(field for field in _REQUIRED if not values.get(field))
#  
#         if error:
#             values = dict(values, error=error, kwargs=kwargs.items())
#             return request.website.render(kwargs.get("view_from", "website.contactus"), values)
#  
#         values['medium_id']  = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'crm.crm_medium_website')
#         values['section_id'] = request.registry['ir.model.data'].xmlid_to_res_id(request.cr, SUPERUSER_ID, 'website.salesteam_website_sales')
#  
#         # description is required, so it is always already initialized
#         if post_description:
#             values['description'] += dict_to_str(_("Custom Fields: "), post_description)
#  
#         if kwargs.get("show_info"):
#             post_description = []
#             environ = request.httprequest.headers.environ
#             post_description.append("%s: %s" % ("IP", environ.get("REMOTE_ADDR")))
#             post_description.append("%s: %s" % ("USER_AGENT", environ.get("HTTP_USER_AGENT")))
#             post_description.append("%s: %s" % ("ACCEPT_LANGUAGE", environ.get("HTTP_ACCEPT_LANGUAGE")))
#             post_description.append("%s: %s" % ("REFERER", environ.get("HTTP_REFERER")))
#             values['description'] += dict_to_str(_("Environ Fields: "), post_description)
#  
#         lead_id = self.create_lead(request, dict(values, user_id=False), kwargs)
#         values.update(lead_id=lead_id)
#         if lead_id:
#             for field_value in post_file:
#                 attachment_value = {
#                     'name': field_value.filename,
#                     'res_name': field_value.filename,
#                     'res_model': 'crm.lead',
#                     'res_id': lead_id,
#                     'datas': base64.encodestring(field_value.read()),
#                     'datas_fname': field_value.filename,
#                 }
#                 request.registry['ir.attachment'].create(request.cr, SUPERUSER_ID, attachment_value, context=request.context)
#  
#         return self.get_contactus_response(values, kwargs)

class redirectExtension(openerp.addons.web.controllers.main.Home):
    
#     @http.route('/', type='http', auth="public", website=True)
#     def index(self, **kw):
#         print "moez_____________"
#         page = 'homepage'
#         try:
#             main_menu = request.registry['ir.model.data'].get_object(request.cr, request.uid, 'website', 'main_menu')
#         except Exception:
#             pass
#         else:
#             first_menu = main_menu.child_id and main_menu.child_id[0]
#             if first_menu:
#                 if not (first_menu.url.startswith(('/page/', '/?', '/#')) or (first_menu.url=='/')):
#                     return request.redirect(first_menu.url)
#                 if first_menu.url.startswith('/page/'):
#                     return request.registry['ir.http'].reroute(first_menu.url)
# #         return self.page(page)
#         print "moez_____________"
#         values={'page':self.page(page)}   
#         return request.website.render("website.homepage", values) 
    
    @http.route('/web/login', type='http', auth="none")
    def web_login(self, redirect=None, **kw):
        redirect = '/?' + request.httprequest.query_string
        if request.httprequest.method == 'GET' and redirect and request.session.uid:
            return http.redirect_with_hash(redirect)

        if not request.uid:
            request.uid = openerp.SUPERUSER_ID

        values = request.params.copy()
        if not redirect:
            redirect = '/?' + request.httprequest.query_string
        values['redirect'] = redirect

        try:
            values['databases'] = http.db_list()
        except openerp.exceptions.AccessDenied:
            values['databases'] = None

        if request.httprequest.method == 'POST':
            old_uid = request.uid
            uid = request.session.authenticate(request.session.db, request.params['login'], request.params['password'])
            if uid is not False:
                return http.redirect_with_hash(redirect)
            request.uid = old_uid
            values['error'] = "Wrong login/password"
        return request.render('web.login', values)
    
def ensure_db(redirect='/web/database/selector'):
    # This helper should be used in web client auth="none" routes
    # if those routes needs a db to work with.
    # If the heuristics does not find any database, then the users will be
    # redirected to db selector or any url specified by `redirect` argument.
    # If the db is taken out of a query parameter, it will be checked against
    # `http.db_filter()` in order to ensure it's legit and thus avoid db
    # forgering that could lead to xss attacks.
    db = request.params.get('db')

    # Ensure db is legit
    if db and db not in http.db_filter([db]):
        db = None

    if db and not request.session.db:
        # User asked a specific database on a new session.
        # That mean the nodb router has been used to find the route
        # Depending on installed module in the database, the rendering of the page
        # may depend on data injected by the database route dispatcher.
        # Thus, we redirect the user to the same page but with the session cookie set.
        # This will force using the database route dispatcher...
        r = request.httprequest
        url_redirect = r.base_url
        if r.query_string:
            # Can't use werkzeug.wrappers.BaseRequest.url with encoded hashes:
            # https://github.com/amigrave/werkzeug/commit/b4a62433f2f7678c234cdcac6247a869f90a7eb7
            url_redirect += '?' + r.query_string
        response = werkzeug.utils.redirect(url_redirect, 302)
        request.session.db = db
        abort_and_redirect(url_redirect)

    # if db not provided, use the session one
    if not db and request.session.db and http.db_filter([request.session.db]):
        db = request.session.db

    # if no database provided and no database in session, use monodb
    if not db:
        db = db_monodb(request.httprequest)

    # if no db can be found til here, send to the database selector
    # the database selector will redirect to database manager if needed
    if not db:
        werkzeug.exceptions.abort(werkzeug.utils.redirect(redirect, 303))

    # always switch the session to the computed db
    if db != request.session.db:
        request.session.logout()
        abort_and_redirect(request.httprequest.url)

    request.session.db = db    

class Extension(openerp.addons.website_sale.controllers.main.website_sale):    
    
#     mandatory_billing_fields = ["name", "phone", "email", "street2", "city", "country_id","mobile"]
#     mandatory_shipping_fields = ["name", "phone", "street", "city", "country_id","mobile"]
    
    @http.route(['/my_profile'], type='http', auth="public", website=True)
    def my_profile(self,**post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        user=pool['res.users'].browse(cr, uid, uid, context=context)
        if user.login=="public":
            return request.redirect('/web/login')
        values = self.checkout_values()
        return request.website.render("website_sale_4cotes_cogen.hvc_my_profile", values) 
    
    @http.route(['/my_profil/confirm'], type='http', auth="public", website=True)
    def confirm_profil(self, **post):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        user=registry['res.users'].browse(cr, uid, uid, context=context)
        if user.login=="public":
            return request.redirect('/web/login')
        values = self.checkout_values_profil(post)
        values["error"] = self.checkout_form_validate(values["checkout"])
        if values["error"]:
            return request.website.render("website_sale_4cotes_cogen.hvc_my_profile", values)
        self.checkout_form_save(values["checkout"])
        return request.redirect("/")
    
    def checkout_values_profil(self, data=None):
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry
        orm_country = registry.get('res.country')
        state_orm = registry.get('res.country.state')
        country_ids = orm_country.search(cr, SUPERUSER_ID, [], context=context)
        countries = orm_country.browse(cr, SUPERUSER_ID, country_ids, context)
        states_ids = state_orm.search(cr, SUPERUSER_ID, [], context=context)
        states = state_orm.browse(cr, SUPERUSER_ID, states_ids, context)
        checkout = {}
        # Default search by user country
        checkout = self.checkout_parse('billing', data)
        if not checkout.get('country_id'):
            country_code = request.session['geoip'].get('country_code')
            if country_code:
                country_ids = request.registry.get('res.country').search(cr, uid, [('code', '=', country_code)], context=context)
                if country_ids:
                    checkout['country_id'] = country_ids[0]

        values = {
            'countries': countries,
            'states': states,
            'checkout': checkout,
            'error': {},
        }
        return values
                
    @http.route(['/shop/confirm_order'], type='http', auth="public", website=True)
    def confirm_order(self, **post):
        print "confirm_order"
        cr, uid, context, registry = request.cr, request.uid, request.context, request.registry

        order = request.website.sale_get_order(context=context)
        print order
        if not order:
            return request.redirect("/shop")

        redirection = self.checkout_redirection(order)
        if redirection:
            print "if redirection"
            return redirection

        values = self.checkout_values(post)
        print values
        values["error"] = self.checkout_form_validate(values["checkout"])
        print values
        if values["error"]:
            print "if values"
            return request.website.render("website_sale.checkout", values)

        self.checkout_form_save(values["checkout"])
        print "confirm_order----------sale_last_order_id"
        print order.id
        request.session['sale_last_order_id'] = order.id
        print request.session['sale_last_order_id']
        request.website.sale_get_order(update_pricelist=True, context=context)
        print values['shipping_id']
        if values['shipping_id']==1:
            return request.redirect("/shop/payment_pickup")
        else:
            return request.redirect("/shop/payment")
        
    mandatory_billing_fields = ["name", "phone", "email", "street2", "city", "country_id","zip"]
    mandatory_shipping_fields = ["name", "phone", "street2", "city", "country_id","zip", "email"]#,"state_id"
        
    
    @http.route(['/shop/payment'], type='http', auth="public", website=True)
    def payment(self, **post):
        """ Payment step. This page proposes several payment means based on available
        payment.acquirer. State at this point :

         - a draft sale order with lines; otherwise, clean context / session and
           back to the shop
         - no transaction in context / session, or only a draft one, if the customer
           did go to a payment.acquirer website but closed the tab without
           paying / canceling
        """
        cr, uid, context = request.cr, request.uid, request.context
        payment_obj = request.registry.get('payment.acquirer')
        sale_order_obj = request.registry.get('sale.order')

        order = request.website.sale_get_order(context=context)

        redirection = self.checkout_redirection(order)
        if redirection:
            return redirection

        shipping_partner_id = False
        if order:
            if order.partner_shipping_id.id:
                shipping_partner_id = order.partner_shipping_id.id
            else:
                shipping_partner_id = order.partner_invoice_id.id

        values = {
            'order': request.registry['sale.order'].browse(cr, SUPERUSER_ID, order.id, context=context)
        }
        values['errors'] = sale_order_obj._get_errors(cr, uid, order, context=context)
        values.update(sale_order_obj._get_website_data(cr, uid, order, context))

        # fetch all registered payment means
        # if tx:
        #     acquirer_ids = [tx.acquirer_id.id]
        # else:
        if not values['errors']:
            acquirer_ids = payment_obj.search(cr, SUPERUSER_ID, [('website_published', '=', True), ('company_id', '=', order.company_id.id)], context=context)
            values['acquirers'] = list(payment_obj.browse(cr, uid, acquirer_ids, context=context))
#             render_ctx = dict(context, submit_class='btn btn-primary', submit_txt=_('Pay Now'))
            for acquirer in values['acquirers']:
                render_ctx = dict(context, submit_class='btn secondary small', submit_txt=acquirer.name)
                acquirer.button = payment_obj.render(
                    cr, SUPERUSER_ID, acquirer.id,
                    order.name,
                    order.amount_total,
                    order.pricelist_id.currency_id.id,
                    partner_id=shipping_partner_id,
                    tx_values={
                        'return_url': '/shop/payment/validate',
                    },
                    context=render_ctx)

        return request.website.render("website_sale.payment", values)



    @http.route(['/shop/cart'], type='http', auth="public", website=True)
    def cart(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        order = request.website.sale_get_order()

        
        if order:
            from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
            to_currency = order.pricelist_id.currency_id
            compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        else:
            compute_currency = lambda price: price

        values = {
            'order': order,
            'compute_currency': compute_currency,
            'suggested_products': [],
        }
        if order:
            _order = order
            if not context.get('pricelist'):
                _order = order.with_context(pricelist=order.pricelist_id.id)
            values['suggested_products'] = _order._cart_accessories()
            print '__order___',order.state

        return request.website.render("website_sale.cart", values)
    
    @http.route(['/shop/cart/new_projet'], type='http', auth="public", website=True)
    def cart_new(self, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        request.session['sale_order_id']=None
        return request.redirect("/shop")
    
    @http.route(['/projet/cart/<model("sale.order"):order_projet>'], type='http', auth="public", website=True)
    def cart_projet(self,order_projet, **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        if order_projet:
            request.session['sale_order_id']=order_projet.id 
        return request.redirect("/shop/cart")
    
    @http.route(['/shop',
        '/shop/page/<int:page>',
        '/shop/category/<model("product.public.category"):category>',
        '/shop/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop(self, page=0, category=None, search='',crx='',typ='', **post):
        logger.warning(' in search shop')
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        domain = request.website.sale_product_domain()
        selections_ids=[]
        if search:
            for srch in search.split(" "):
                domain += ['|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]
        if crx:
            crx =crx.split(",") ;
            product_obj = pool.get('product.template')
            product_ids = product_obj.search(cr, uid,[], context=context)
            for tab in product_obj.browse(cr, uid, product_ids, context=context):
                if tab.attribute_line_ids:
                    for line in tab.attribute_line_ids:                   
                        for many in  line.value_ids :
                            for ep in crx:
                                if many.id == int(ep):
                                    selections_ids+=[tab.id]
                                    
            domain +=[('id', 'in', selections_ids)]
        if typ:
            typ =typ.split(",") ;
            product_obj = pool.get('product.template')
            product_ids = product_obj.search(cr, uid,[], context=context)
            for tab in product_obj.browse(cr, uid, product_ids, context=context):
                for ep in typ:
                    if tab.material_type_id.id == int(ep):
                        selections_ids+=[tab.id]
            domain +=[('id', 'in', selections_ids)]
        
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])
        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]
 
        keep = QueryURL('/shop', category=category and int(category), search=search, attrib=attrib_list)
 
        if not context.get('pricelist'):
            pricelist = self.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)
 
        product_obj = pool.get('product.template')
 
        url = "/shop_items"
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if crx:
            post["crx"] = crx
        if typ:
            post["typ"] = typ
        if search:
            post["search"] = search
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/shop_items/category/%s" % slug(category)
        pager = request.website.pager_items(url=url, total=product_count, page=page, step=PPG, scope=200, url_args=post)
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)
        
        style_obj = pool['product.style']
        style_ids = style_obj.search(cr, uid, [], context=context)
        styles = style_obj.browse(cr, uid, style_ids, context=context)
 
        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)
 
        attributes_obj = request.registry['product.attribute']
        attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)
 
        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        
        matriaux_obj = pool['hvc.product.material.type']
        matriaux_ids = matriaux_obj.search(cr, uid, [], context=context)
        type_matriaux = matriaux_obj.browse(cr, uid, matriaux_ids, context=context)
        
        attr_value_obj = pool['product.attribute.value']
        attr_value_ids = attr_value_obj.search(cr, uid, [], context=context)
        attr_value = attr_value_obj.browse(cr, uid, attr_value_ids, context=context)

        values = {
            'attr_value':attr_value,
            'type_matriaux':type_matriaux,
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': table_compute().process(products),
            'rows': PPR,
            'styles': styles,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        }
    #START HR 13062016
#         return request.website.render("website_sale.products", values)
        return request.website.render("website_sale_4cotes_cogen.hvc_products", values)
    #END HR 13062016    
    
    @http.route(['/shop',
        '/shop_items/page/<int:page>',
        '/shop_items/category/<model("product.public.category"):category>',
        '/shop_items/category/<model("product.public.category"):category>/page/<int:page>'
    ], type='http', auth="public", website=True)
    def shop_items(self, page=0, category=None, search='',crx='',typ='', **post):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        
        domain = request.website.sale_product_domain()
       
        selections_ids=[]
        if search:
            for srch in search.split(" "):
                domain += ['|', '|', '|', ('name', 'ilike', srch), ('description', 'ilike', srch),
                    ('description_sale', 'ilike', srch), ('product_variant_ids.default_code', 'ilike', srch)]
        if crx:
            crx =crx.split(",") ;
            product_obj = pool.get('product.template')
            product_ids = product_obj.search(cr, uid,[], context=context)
            for tab in product_obj.browse(cr, uid, product_ids, context=context):
                if tab.attribute_line_ids:
                    for line in tab.attribute_line_ids:                   
                        for many in  line.value_ids :
                            for ep in crx:
                                if many.id == int(ep):
                                    selections_ids+=[tab.id]
                                    
     
            domain +=[('id', 'in', selections_ids)]
        if typ:
            typ =typ.split(",") ;
            product_obj = pool.get('product.template')
            product_ids = product_obj.search(cr, uid,[], context=context)
            for tab in product_obj.browse(cr, uid, product_ids, context=context):
                for ep in typ:
                    if tab.material_type_id.id == int(ep):
                        selections_ids+=[tab.id]
            domain +=[('id', 'in', selections_ids)]
        if category:
            domain += [('public_categ_ids', 'child_of', int(category))]

        attrib_list = request.httprequest.args.getlist('attrib')
       
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        if attrib_values:
            attrib = None
            ids = []
            for value in attrib_values:
                if not attrib:
                    attrib = value[0]
                    ids.append(value[1])
                elif value[0] == attrib:
                    ids.append(value[1])
                else:
                    domain += [('attribute_line_ids.value_ids', 'in', ids)]
                    attrib = value[0]
                    ids = [value[1]]
            if attrib:
                domain += [('attribute_line_ids.value_ids', 'in', ids)]

        keep = QueryURL('/shop_items', category=category and int(category), search=search, attrib=attrib_list)

        if not context.get('pricelist'):
            pricelist = self.get_pricelist()
            context['pricelist'] = int(pricelist)
        else:
            pricelist = pool.get('product.pricelist').browse(cr, uid, context['pricelist'], context)

        product_obj = pool.get('product.template')

        url = "/shop_items"
        product_count = product_obj.search_count(cr, uid, domain, context=context)
        if crx:
            post["crx"] = crx
        if typ:
            post["typ"] = typ
        if search:
            post["search"] = search
        if category:
            category = pool['product.public.category'].browse(cr, uid, int(category), context=context)
            url = "/shop_items/category/%s" % slug(category)
        pager = request.website.pager_items(url=url, total=product_count, page=page, step=PPG, scope=200, url_args=post)
        
        product_ids = product_obj.search(cr, uid, domain, limit=PPG, offset=pager['offset'], order='website_published desc, website_sequence desc', context=context)
        products = product_obj.browse(cr, uid, product_ids, context=context)

        style_obj = pool['product.style']
        style_ids = style_obj.search(cr, uid, [], context=context)
        styles = style_obj.browse(cr, uid, style_ids, context=context)

        category_obj = pool['product.public.category']
        category_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs = category_obj.browse(cr, uid, category_ids, context=context)

        attributes_obj = request.registry['product.attribute']
        attributes_ids = attributes_obj.search(cr, uid, [], context=context)
        attributes = attributes_obj.browse(cr, uid, attributes_ids, context=context)

        from_currency = pool.get('product.price.type')._get_field_currency(cr, uid, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, uid, from_currency, to_currency, price, context=context)
        
       
        values = {
            'search': search,
            'category': category,
            'attrib_values': attrib_values,
            'attrib_set': attrib_set,
            'pager': pager,
            'pricelist': pricelist,
            'products': products,
            'bins': table_compute().process(products),
            'rows': PPR,
            'styles': styles,
            'categories': categs,
            'attributes': attributes,
            'compute_currency': compute_currency,
            'keep': keep,
            'style_in_product': lambda style, product: style.id in [s.id for s in product.website_style_ids],
            'attrib_encode': lambda attribs: werkzeug.url_encode([('attrib',i) for i in attribs]),
        }
        return request.website.render("website_sale_4cotes_cogen.products_grid_items", values)

    

    
    
    ################################### 4 Cotes: Nouveau configurateur des produits sur mesure ##############################
    @http.route(['/shop/product/get_attributes_lines_price'], type='json', auth="public", methods=['POST'], website=True)
    def get_attributes_lines_price(self,template_id,values,final_surface,edge_length=0.0,final_lenght=0.0,final_width=0.0,final_sides='0000'):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res=pool.get('product.product').get_result(cr,uid,template_id,values,final_surface,edge_length,final_lenght,final_width,final_sides,context=context)
        return res
    
    def get_attribute_value_ids(self, product):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        currency_obj = pool['res.currency']
        attribute_value_ids = []
        if request.website.pricelist_id.id != context['pricelist']:
            website_currency_id = request.website.currency_id.id
            currency_id = self.get_pricelist().currency_id.id
            for p in product.product_variant_ids:
                price = currency_obj.compute(cr, SUPERUSER_ID, website_currency_id, currency_id, p.lst_price)
                attribute_value_ids.append([p.id, [v.id for v in p.attribute_value_ids if len(v.attribute_id.value_ids) > 1], p.price, price])
        else:
            attribute_value_ids = [[p.id, [v.id for v in p.attribute_value_ids if len(v.attribute_id.value_ids) > 1], p.price, p.lst_price]
                for p in product.product_variant_ids]
        return attribute_value_ids
    
    @http.route()
    def product(self, product, category='', search='',local_calling=False, **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        category_obj = pool['product.public.category']
        template_obj = pool['product.template']

        context.update(active_id=product.id)

        if category:
            category = category_obj.browse(cr, uid, int(category), context=context)
        
            
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/shop', category=category and category.id, search=search, attrib=attrib_list)
        categs_obj = pool['product.public.category']
        categs_ids = categs_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs_prod = categs_obj.browse(cr, uid, categs_ids, context=context)
        category_ids = category_obj.search(cr, uid, [], context=context)
        category_list = category_obj.name_get(cr, uid, category_ids, context=context)
        category_list = sorted(category_list, key=lambda category: category[1])

        pricelist = self.get_pricelist()

        from_currency = pool.get('product.price.type')._get_field_currency(cr, SUPERUSER_ID, 'list_price', context)
        to_currency = pricelist.currency_id
        compute_currency = lambda price: pool['res.currency']._compute(cr, SUPERUSER_ID, from_currency, to_currency, price, context=context)

        if not context.get('pricelist'):
            context['pricelist'] = int(self.get_pricelist())
            product = template_obj.browse(cr, SUPERUSER_ID, int(product), context=context)
        
        bc_ids=[]
        if product:
            bc_ids=self.get_basic_characteristics(product.material_type_id.id, product.category_measurement, local_calling=True)
           
        values = {
            'search': search,
            'category': category,
            'pricelist': pricelist,
            'attrib_values': attrib_values,
            'compute_currency': compute_currency,
            'attrib_set': attrib_set,
            'keep': keep,
            'category_list': category_list,
            'main_object': product,
            'product': product,
            'get_attribute_value_ids': self.get_attribute_value_ids,
            'template_id': int(product),
#             'get_template_exception_ids': self.get_template_exception_ids,
#             'get_template_inclusion_ids': self.get_template_inclusion_ids,
            'categories': categs_prod,
            'basic_characteristics_ids':bc_ids,
            'category_measurement':product.category_measurement
        }
        if local_calling:
            return values
        return request.website.render("website_sale.product", values)
    
    @http.route(['/shop/configurator/category/<model("product.public.category"):category>'], type='http', auth="public", website=True)
    def configurator(self, product=None, category=None, search='',local_calling=False, **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        category_obj = pool['product.public.category']
        template_obj = pool['product.template']
        category_measurement='normal'
        if product:
            context.update(active_id=product.id)
        if category:
            category = category_obj.browse(cr, uid, int(category), context=context)
            category_measurement=category.family_id.category_measurement
        
        attrib_list = request.httprequest.args.getlist('attrib')
        attrib_values = [map(int,v.split("-")) for v in attrib_list if v]
        attrib_set = set([v[1] for v in attrib_values])

        keep = QueryURL('/shop', category=category and category.id, search=search, attrib=attrib_list)
        categs_ids = category_obj.search(cr, uid, [('parent_id', '=', False)], context=context)
        categs_prod = category_obj.browse(cr, uid, categs_ids, context=context)
        
        t_ids=template_obj.search(cr,SUPERUSER_ID,[('active','=',True),('sale_ok','=',True),('category_measurement','=',category_measurement)])
        materials_type=sorted(list(set([t.material_type_id for t in template_obj.browse(cr, SUPERUSER_ID, t_ids, context=context)])),key=lambda x: x.sequence, reverse=False)

        
        values = {
            'category': category,
            'keep': keep,
            'categories': categs_prod,
            'materials_type': materials_type,
            'get_basic_characteristics':[],
            'category_measurement':category_measurement
        }
        
        
        if local_calling:
            return values
        return request.website.render("website_sale_4cotes_cogen.configurator", values)
    
    def get_values_friends_ids(self,criteria,material_type_id,attribute_id,category_measurement,bc):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        templates_ids=pool['product.template'].search(cr, SUPERUSER_ID,[('attribute_line_ids.value_ids','in',[criteria.id]),('category_measurement','=',category_measurement),('sale_ok','=',True),('material_type_id.id','=',material_type_id)] )
        friends_ids=[]
        bc1=[]
       
        attribute_line_obj = pool['product.attribute.line']        
        line_ids = attribute_line_obj.search(cr, SUPERUSER_ID, [('product_tmpl_id.id','in',templates_ids),('attribute_id.id','in', bc)], context=context)
        for line in attribute_line_obj.browse(cr, SUPERUSER_ID, line_ids, context=context):
            for i in line.value_ids.ids:
                friends_ids.append(i)          
        return friends_ids
    
    def get_basic_characteristics_value_ids(self,material_type_id,attribute_id,category_measurement,bc):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        res=[]
        value_ids=[]
        friends_ids=[]
        attribute_line_obj = pool['product.attribute.line']
        line_ids = attribute_line_obj.search(cr, SUPERUSER_ID, [('product_tmpl_id.category_measurement','=',category_measurement),('product_tmpl_id.sale_ok','=',True),('product_tmpl_id.material_type_id.id','=',material_type_id),('attribute_id.id','=',attribute_id)], context=context)
        for line in attribute_line_obj.browse(cr, SUPERUSER_ID, line_ids, context=context):
            value_ids.append(line.value_ids)
            
        value_ids=sorted(list(set(value_ids)),key=lambda x: x.sequence, reverse=False)
        for v in value_ids:
            friends_ids=self.get_values_friends_ids(v,material_type_id,attribute_id,category_measurement,bc)
            res.append([v,list(set(friends_ids))])
        return res
    


## get_basic_characteristics
    
    @http.route(['/shop/material/get_basic_characteristics'], type='json', auth="public", methods=['POST'], website=True)
    def get_basic_characteristics(self,material_type_id,category_measurement,local_calling=False):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        basic_characteristics=[]
        basic_characteristics_ids=[]
        result={}
        bc=[]
        bc_ids=[]
        material_type = pool['hvc.product.material.type'].browse(cr, SUPERUSER_ID, material_type_id, context=context)
        if category_measurement=='planches':
            bc=material_type.planches_attribute_base_ids
        elif category_measurement=='portes':
            bc=material_type.portes_attribute_base_ids
        elif category_measurement=='tablettes':
            bc=material_type.tablettes_attribute_base_ids
        elif category_measurement=='ensembles_monter':
            bc=material_type.ensembles_monter_attribute_base_ids
        for c in bc:
            bc_ids.append(c.id)
        for c in bc:
            value_ids=self.get_basic_characteristics_value_ids(material_type_id,c.id,category_measurement,bc_ids)
            basic_characteristics.append((c,value_ids))
            basic_characteristics_ids.append(c.id)
        
        values={'get_basic_characteristics':basic_characteristics}
        if local_calling:
            return basic_characteristics_ids
        result['website_sale_4cotes_cogen.area_basic_characteristics']=request.website._render("website_sale_4cotes_cogen.area_basic_characteristics", values)
        result['website_sale_4cotes_cogen.area_basic_characteristics_thickness']=request.website._render("website_sale_4cotes_cogen.area_basic_characteristics_thickness", values)#HR19052016
        return result
    
    @http.route(['/shop/material/get_product_model'], type='json', auth="public", methods=['POST'], website=True)
    def get_product_model(self,material_type_id,category_measurement,category_id,basic_characteristics=[]):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        result={}
        configurator_values=self.configurator(category=category_id,local_calling=True)
        template=None
        template_obj = pool['product.template']
        t_ids=template_obj.search(cr,SUPERUSER_ID,[('material_type_id','=',material_type_id),('active','=',True),('category_measurement','=',category_measurement),('sale_ok','=',True)])
        for t in template_obj.browse(cr,SUPERUSER_ID,t_ids):
            is_ok=False
            lines =[ (line.attribute_id.id,[x.id for x in line.value_ids][0]) for line in t.attribute_line_ids if len(line.value_ids)==1]
            for bc in basic_characteristics:
                if eval(bc) in lines:
                    is_ok=True
                else:
                    is_ok=False
                    break
            if is_ok:
                template=t
                break
        if template:
            result['template_name']=template.name
            result['template_id']=template.id
            template_values=self.product(template,local_calling=True)
            result['selling_options']=request.website._render("website_sale_4cotes_cogen.selling_options", template_values)
            result['options_variants']=request.website._render("website_sale_4cotes_cogen.options_variants", template_values)
            result['product_detail_ids']=request.website._render("website_sale_4cotes_cogen.product_detail_ids", template_values)

#             result['template_finition_des_chants']=request.website._render("website_sale_4cotes_cogen.template_finition_des_chants", template_values)

        else:
            result['selling_options']=request.website._render("website_sale_4cotes_cogen.selling_options", configurator_values)

#             result['template_finition_des_chants']=request.website._render("website_sale_4cotes_cogen.template_finition_des_chants", configurator_values)


            result['options_variants']=request.website._render("website_sale_4cotes_cogen.options_variants", configurator_values)
            result['product_detail_ids']=request.website._render("website_sale_4cotes_cogen.product_detail_ids", configurator_values)
        return result
    
    def refresh_variant_values(self, template_id, category='', search='', **kwargs):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        template_obj = pool['product.template']
        template=template_obj.browse(cr,SUPERUSER_ID,template_id)
        template_values=self.product(template,local_calling=True)
        result={}
        result['options_variants'] = request.website._render("website_sale_4cotes_cogen.options_variants",template_values )
        return result
    
    @http.route(['/shop/product/get_attributes_lines_values_ids'], type='json', auth="public", methods=['POST'], website=True)
    def get_attributes_lines_values_ids(self,template_id,length,width,sides):
        cr, uid, context, pool = request.cr, request.uid, request.context, request.registry
        attribute_line_obj = request.registry.get('product.attribute.line')
        types_attributes=['input_length','input_width','sides']
        result={}
        for type_att in types_attributes:
            lines_ids=attribute_line_obj.get_attribute_line_type(request.cr, SUPERUSER_ID,template_id,type_att,context=None)
            if lines_ids:
                if type_att=='input_length':
                    length_id = attribute_line_obj.get_attribute_line_values_id(request.cr, SUPERUSER_ID,lines_ids[0],length,context=None)
                    result['length_id']=length_id
                if type_att=='input_width':
                    width_id = attribute_line_obj.get_attribute_line_values_id(request.cr, SUPERUSER_ID,lines_ids[0],width,context=None)
                    result['width_id']=width_id
                if type_att=='sides':
                    side_id = attribute_line_obj.get_attribute_line_values_id(request.cr, SUPERUSER_ID,lines_ids[0],sides,context=None)
                    result['side_id']=side_id
        dic=self.refresh_variant_values(template_id)
        result['options_variants']=dic['options_variants']
        return result
    
    @http.route(['/shop/product/add_to_cart'], type='json', auth="public", methods=['POST'], website=True)
    def add_to_cart_update(self, final_sides='',final_length=0,final_width=0,final_price=0,final_surface=0,final_price_display=0,template_id=0,current_variant_value_ids=0,
                    product_id=None,add_qty=1, set_qty=0,purchase_ok=False, sale_ok=True,**kw):
        cr, uid, context = request.cr, request.uid, request.context
        template=None
        result={}
        logger.info("_____Product_id: %s",product_id)
        template_obj = request.registry.get('product.template')
        if template_id:
            template=template_obj.browse(cr,SUPERUSER_ID,int(template_id))
        order=request.website.sale_get_order(force_create=1)
        order._cart_update(final_sides=final_sides,length=float(final_length),width=float(final_width),final_price=float(final_price),
                                                                    final_surface=final_surface,lst_price=final_price_display,
                                                                    template_id=template_id,current_variant_value_ids=current_variant_value_ids,
                                                                    product_id=int(product_id), add_qty=float(add_qty), set_qty=float(set_qty),purchase_ok=False, sale_ok=True)
        result['cart_quantity'] = order.cart_quantity
        template_values=self.product(template,local_calling=True)
        
        result['selling_options']=request.website._render("website_sale_4cotes_cogen.selling_options", template_values)
        result['icon_panier']=request.website._render("website_4cotes_theme.icon_panier", {})
        
        return result
    
    @http.route(['/shop/project_set_ref'], type='json', auth="public", methods=['POST'], website=True)
    def project_set_ref(self,order_name, client_order_ref='',**kw):
        cr, uid, context = request.cr, request.uid, request.context
        sale_order_obj = request.registry.get('sale.order')
        sale_order_ids = sale_order_obj.search(cr, uid,[('name','=',order_name)], context=context)
        result={}
        if len(sale_order_ids):
            sale_order_obj.write(cr,uid,sale_order_ids[0],{'client_order_ref':client_order_ref})
        return result
    
# vim:expandtab:tabstop=4:softtabstop=4:shiftwidth=4:
